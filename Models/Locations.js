
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Config = require('../Config');

var Location = new Schema({    
    cityId: {type: Schema.Types.ObjectId,ref:'City'},
    location:{type: [Number],required:true,index: '2d'},
    isBlocked:{type: Boolean, default: false,required:true},
    isDeleted:{type:Boolean,default:false,required:true}
});



module.exports = mongoose.model('Location', Location);
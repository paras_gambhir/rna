
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Config = require('../Config');

var Account = new Schema({    
    bookingId:{type:Number,trim:true},
    invoiceNo:{type:String,trim:true},
    type:{type:String,trim:true,enum:[
            'DRIVER',
            'COMPANY',
            'VENDOR',
            'LOADER'
    ]},
    freightAmount:{type:Number},
    detentionCharges:{type:Number},
    amount:{type:Number,required:true},
    startDate:{type:String},
    endDate:{type:String},
    date:{type:Number,required:true},
    receiverId:{type:Schema.Types.ObjectId,required:true},
    receiverName:{type:String},
    isClear:{type:Boolean,default:false},
    clearDate:{type:Number},
    comment:{type:String,default:''},
    isBlocked:{type: Boolean, default: false,required:true},
    isDeleted:{type:Boolean,default:false,required:true}
});



module.exports = mongoose.model('Account', Account);
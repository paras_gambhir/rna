/**
 * Created by parasgambhir on 18/04/17.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Config = require('../Config');



var Company = new Schema({
    name: {type: String, trim: true, default: null, sparse: true},
    countryCode: {type: String, trim: true, default: null, sparse: true},
    phoneNumber: {type: String, trim: true, required: true},
    email: {type: String, trim: true, index: true, default: null},
    password: {type: String, trim: true, default: null},
    codeUpdatedAt: {type: Date, default: Date.now, required: true},
    registrationDate: {type: Date, default: Date.now, required: true},
    isBlocked: {type: Boolean, default: false},
    profilePicURL: {
        original: {type: String, default: null},
        thumbnail: {type: String, default: null}
    },
    verificationCode: {type: String, trim: true, default: null},
    verifiedCodeVerified: {type: Boolean, default: false},
    isApproved: {type: Boolean, default: false},
    OTP: {type: Number, default : 0},
    OTPVerified: {type: Boolean, default : false},
    profileCompleted: {type: Boolean, default: false},
    approvedByAdmin:{type: Boolean, default: false},
    address: {type: String, default : null},
    pincode:{type: Number},
    city:{type:String},
    state:{type:String},
    emailVerify:{type:Boolean,default:false},
    accessToken: {type: String, trim: true},
    emailNotifications:{type:Boolean, default:true},
    smsNotifications:{type:Boolean, default:true},
    pushNotifications:{type: Boolean, default: true},
    step1:{type:Boolean,default:false},
    step2:{type:Boolean,default:false},
    step3:{type:Boolean,default:false},
    isBlocked:{type:Boolean,default:false},
    isDeleted:{type:Boolean,default:false}

});




module.exports = mongoose.model('Company', Company);
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Config = require('../Config');


var documents = new Schema({
    documentType: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.DOCUMENT_TYPE.DRIVING_LICENSE,
            Config.APP_CONSTANTS.DATABASE.DOCUMENT_TYPE.REGISTRATION,
            Config.APP_CONSTANTS.DATABASE.DOCUMENT_TYPE.INSURANCE,
            Config.APP_CONSTANTS.DATABASE.DOCUMENT_TYPE.ADHAR,
            Config.APP_CONSTANTS.DATABASE.DOCUMENT_TYPE.PAN

        ]
    },
    documentImage: {type: String, trim: true, required: true},
    expiryDate: {type: Date, default: Date.now()}
});



var Driver = new Schema({
    name: {type: String, trim: true, default: null, sparse: true},
    dob: {type: String, trim: true, default: null, sparse: true},
    countryCode: {type: String, trim: true, default: null, sparse: true},
    address:{type: String,default:null,sparse: true},
    pincode:{type: Number,required:true},
    city:{type:String,required:true},
    state:{type:String,required:true},
    phoneNumber: {type: String, trim: true, required: true},
    email: {type: String, trim: true, index: true, default: null},
    password: {type: String, trim: true, default: null},
    codeUpdatedAt: {type: Date, default: Date.now, required: true},
    registrationDate: {type: Date, default: Date.now, required: true},
    appVersion: {type: String},
    accessToken: {type: String, trim: true,default: null},
    deviceToken: {type: String, trim: true, default: null},
    deviceType: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID
        ],
        default: 'IOS'
    },
    latitude: {type: Number, default: 0},
    longitude: {type: Number, default: 0},
    currentLocation: {type: [Number], index: '2d'},
    isBlocked: {type: Boolean, default: false},
    profilePicURL: {
        original: {type: String, default: null},
        thumbnail: {type: String, default: null}
    },
    verificationCode: {type: String, trim: true, default: null},
    verifiedCodeVerified: {type: Boolean, default: false},
    documents: [documents],
    vehicleNumber: {type: String,default:null},
    isApproved: {type: Boolean, default: false},
    truckId:{type: Schema.ObjectId, ref:'Truck',default: null},
    vendorId:{type: Schema.ObjectId, ref:'Vendor', default: null},
    VendorTruck:{type: Schema.ObjectId, ref:'VendorTruck', default: null},
    OTP:{type: Number, default:0},
    approvedByAdmin:{type: Boolean, default: false},
    isAvailable: {type: Boolean, default: true},
    currentBooking:{type: Boolean, default: false},
    truckAssign: {type: Boolean, default: false},
    notification: {type: String, default:'1'},
    isBlocked:{type:Boolean,default:false},
    isDeleted:{type:Boolean,default:false}
});



Driver.index({'currentLocation.coordinates': "2d"});



module.exports = mongoose.model('Driver', Driver);
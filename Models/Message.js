
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Config = require('../Config');

var Message = new Schema({    
    driverId: {type: Schema.Types.ObjectId,ref:'Driver'},
    vendorId: {type: Schema.Types.ObjectId,ref:'Vendor'},
    message:{type: String,required:true,trim:true},
    date:{type:Date,default:Date.now},
    isDeleted:{type:Boolean,default:false,required:true}
});



module.exports = mongoose.model('Message', Message);
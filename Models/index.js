
module.exports = {
    AppVersions : require('./AppVersions'),
    Admin: require('./Admin'),
    Driver: require('./Driver'),
    Vendor:require('./Vendor'),
    Truck: require('./Truck'),
    VendorTruck: require('./VendorTrucks'),
    Company:require('./Company'),
    Bookings:require('./Bookings'),
    Contracts: require('./Contracts'),
    Materials: require('./Materials'),
    City:require('./City'),
    Loader:require('./Loader'),
    Location:require('./Location'),
    Account:require('./Account'),
     Message:require('./Message'),
     Notification:require('./Notification')
};
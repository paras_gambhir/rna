
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Config = require('../Config');

var Location = new Schema({    
    pointName: {type: String, trim: true,required:true},
    location:{type: [Number],required:true,index: '2d'},
    authPerson: {type: String, trim: true,required:true},
    phoneNumber: {type: String, trim: true,required:true},
    address: {type: String, required:true},
    isBlocked:{type: Boolean, default: false,required:true},
    isDeleted:{type:Boolean,default:false,required:true},
    cityId:{type:Schema.Types.ObjectId,ref:'City',required:true},
    companyId:{type:Schema.Types.ObjectId,ref:'Company',required:true}
});



module.exports = mongoose.model('Location', Location);
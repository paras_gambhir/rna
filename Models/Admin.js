/**
 * Created by parasgambhir on 01/02/17.
 */




var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');


var Admin = new Schema({
    Email:{type: String, index:true, unique:true,trim:true, required: true},
    Password:{type: String, trim: true, required: true},
    accessToken:{type: String, trim: true, default:null},
    insertedAt:{type: Date, default: Date.now},
    lastLogin:{type: Date, default:Date.now,required: true},
    emailNotifications:{type:Boolean, default:false},
    smsNotifications:{type:Boolean, default:false},
    pushNotifications:{type: Boolean, default: false},
     isBlocked:{type:Boolean,default:false},
    isDeleted:{type:Boolean,default:false}

});


module.exports = mongoose.model('Admin', Admin);
/**
 * Created by parasgambhir on 18/04/17.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Config = require('../Config');




var Truck = new Schema({    
    truckName: {type: String, trim: true, default: null, sparse: true,unique:true},
    Capacity:{type: Number,min:1,max:50, default: null},
    length:{type: Number,required:true},
    breadth:{type:Number,required:true},
    height:{type:Number,required:true},
    isDeleted:{type:Boolean,default:false},
    isBlocked:{type:Boolean,default:false}
});



module.exports = mongoose.model('Truck', Truck);
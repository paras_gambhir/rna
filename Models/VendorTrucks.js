/**
 * Created by parasgambhir on 18/04/17.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Config = require('../Config');


var documents = new Schema({
    documentType: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.DOCUMENT_TYPE.POLLUTION,
            Config.APP_CONSTANTS.DATABASE.DOCUMENT_TYPE.REGISTRATION,
            Config.APP_CONSTANTS.DATABASE.DOCUMENT_TYPE.INSURANCE,
            Config.APP_CONSTANTS.DATABASE.DOCUMENT_TYPE.FITNESS,
        ]
    },
    documentImage: {type: String, trim: true, required: true},
    expiryDate: {type: Date, default: Date.now()}
});



var VendorTruck = new Schema({
    truckId:{type: Schema.ObjectId, ref:'Truck', required:true, default:null},
    vendorId:{type:Schema.ObjectId, ref:'Vendor',required: true, default:null},
    driverId:{type: Schema.ObjectId, ref:'Driver',default:null},
    truckNo: {type: String, trim: true,required:true},
    startLocation: {type: [Number],index:'2d'},
    startCityId:{type:Schema.Types.ObjectId,ref:'City',required:true},
    endLocation:{type:[Number], index:'2d'},
    endCityId:{type:Schema.Types.ObjectId,ref:'City',required:true},
    currentLocation:{type:Schema.Types.ObjectId,ref:'City'},
    startAddress:{type:String, default:null},
    endAddress:{type: String, default: null},
    createdDate:{type: Date, default: Date.now()},
    status: {type: String, 
        default: Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.VACANT,
        enum:[
        Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.BOOKED,
        Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.BROKE,
        Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.VACANT

    ]},
    documents:[documents]
});


VendorTruck.index({'startLocation.coordinates': "2d"},{'endLocation.coordinates': "2d"});


module.exports = mongoose.model('VendorTruck', VendorTruck);
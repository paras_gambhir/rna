/**
 * Created by parasgambhir on 18/04/17.
 */



var mongoose = require('mongoose');
var Schema = mongoose.Schema;



var Config = require('../Config');

var Vendor = new Schema({
    name:{type: String,required: true, trim: true},
    email:{type: String, required: true, unique: true},
    countryCode:{type: String, required:true, trim:true},
    contactNo:{type: String, required:true, trim:true, unique:true},
    lastLoginDate:{type:Date, default:Date.now()},
    registrationDate:{type: Date, default:Date.now()},
    password:{type:String, trim:true, required:true},
    profilePicURL: {
        original: {type: String, default: null},
        thumbnail: {type: String, default: null}
    },
    accessToken:{type:String, trim:true},
    aadharNumber: {type: String, default: null},
    panNumber: {type: String, default: null},
    aadharCardImage: {type: String, default: null},
    panCardImage:{type: String, default: null},
    OTP: {type: Number, default : 0},
    OTPVerified: {type: Boolean, default : false},
    profileCompleted: {type: Boolean, default: false},
    approvedByAdmin:{type: Boolean, default: false},
    address: {type: String, default:null},
    city:{type: String,default:null},
    state:{type: String,default:null},
    pincode:{type: Number},
    emailVerify:{type:Boolean,default:false},
    emailNotifications:{type:Boolean,default:true},
    smsNotifications:{type:Boolean,default:true},
    pushNotifications:{type:Boolean,default:true},
    step1:{type:Boolean,default:false},
    step2:{type:Boolean,default:false},
    step3:{type:Boolean,default:false},
    isBlocked:{type:Boolean,default:false},
    isDeleted:{type:Boolean,default:false}
});




module.exports = mongoose.model('Vendor',Vendor);
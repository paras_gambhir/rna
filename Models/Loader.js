var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Config = require('../Config');

var Loader = new Schema({
    name: {type: String, trim: true, default: null, sparse: true},
    dob: {type: String, trim: true, default: null, sparse: true},
    countryCode: {type: String, trim: true, default: null, sparse: true},
    address: {type: String, default:null},
    city:{type: String,default:null},
    state:{type: String,default:null},
    pincode:{type: Number},
    phoneNumber: {type: String, trim: true, required: true},
    email: {type: String, trim: true, index: true, default: null},
    password: {type: String, trim: true, default: null},
    codeUpdatedAt: {type: Date, default: Date.now, required: true},
    registrationDate: {type: Date, default: Date.now, required: true},
    appVersion: {type: String},
    accessToken: {type: String, trim: true,default:null},
    deviceToken: {type: String, trim: true, default: null},
    deviceType: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID
        ],
        default: 'IOS'
    },
    latitude: {type: Number, default: 0},
    longitude: {type: Number, default: 0},
    currentLocation: {type: [Number], index: '2d'},
    isBlocked: {type: Boolean, default: false},
    profilePicURL: {
        original: {type: String, default: null},
        thumbnail: {type: String, default: null}
    },
    isDeleted:{type:Boolean,default:false},
    OTP:{type: Number, default:0},
    assignedLocations:{type:[Schema.Types.ObjectId],ref:'Location',default:[]},
    isBlocked:{type:Boolean,default:false},
    isDeleted:{type:Boolean,default:false}
});



module.exports = mongoose.model('Loader', Loader);
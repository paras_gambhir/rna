/**
 * Created by parasgambhir on 20/05/17.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Config = require('../Config');


var Contract = new Schema({
    companyId:{type:Schema.Types.ObjectId,ref:"Company",trim:true},
    truckType:{type:String,required:true},
    freightAmount:{type:Number,required:true},
    detentionCharges:{type:Number,required:true},
    billingPeriod:{type:Number,required:true},
    startCityId:{type:Schema.Types.ObjectId,ref:'City',required:true},
    endCityId:{type:Schema.Types.ObjectId,ref:'City',required:true},
    truckId:{type:Schema.ObjectId,ref:'Truck'}, 
    isBlocked:{type:Boolean,default:false},
    isDeleted:{type:Boolean,default:false}
});



Contract.index({'startLocation.coordinates': "2d"},{'endLocation.coordinates':'2d'});


module.exports = mongoose.model('Contract', Contract);
/**
 * Created by parasgambhir on 20/05/17.
 */



var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Config = require('../Config');


var Materials = new Schema({
    CompanyId:{type: Schema.ObjectId, ref:'Company',required:true},
    materialName: {type: String, trim: true, default: null, sparse: true},
    length:{type: Number,required:true},
    breadth:{type:Number,required:true},
    height:{type:Number,required:true},
    capacity:{type:Number,required:true},
    accessionNo:{type:String,required:true},
    isDeleted:{type: Boolean, default:false}
});




module.exports = mongoose.model('Materials', Materials);
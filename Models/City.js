
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Config = require('../Config');

var City = new Schema({    
    cityName: {type: String, trim: true,required:true},
    location:{type: [Number],required:true,index: '2d'},
    isBlocked:{type: Boolean, default: false,required:true},
    isDeleted:{type:Boolean,default:false,required:true}
});



module.exports = mongoose.model('City', City);
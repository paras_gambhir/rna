/**
 * Created by parasgambhir on 18/04/17.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Config = require('../Config');


var Rejection = new Schema({
    driverId:{type: Schema.ObjectId,ref:'Driver', required: true},
    rejectionReason:{type: String, required: true},
    rejectionTime:{type: Number}
});


var Materials = new Schema({
    materialId:{type: Schema.ObjectId,ref:'Materials', required: true},
    materialQuantity:{type: String, default: null}

});



var loadingLocation =  new Schema({
    locationAddress:{type: String, default: null},
    location:{type:[Number], index:'2d'},
    status :{type: String, enum:[
        Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.LOADING_LOCATION,
        Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.PENDING,
        Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.FILLED_BY_LOADER,
        Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.OTP_CONFIRMED
    ],default:'PENDING'},
    locationArriveTime:{type: Date, default: Date.now()},
    locationLeaveTime:{type: Date, default: Date.now()},
    materials:[{
        materialId:{type: Schema.ObjectId,ref:'Materials'},
        materialQuantity:{type: String, default: null}
    }],
    OTP:{type: Number, default: null}
});


var unloadingLocation =  new Schema({
    locationAddress:{type: String, default: null},
    location:{type:[Number], index:'2d'},
    status :{type: String, enum:[
        Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.UNLOADING_LOCATION,
        Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.PENDING,
        Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.UNLOADING_FORM_FILLED
    ],default:'PENDING'},
    locationArriveTime:{type: Date, default: Date.now()},
    locationLeaveTime:{type: Date, default: Date.now()},
    materials:[{
        materialId:{type: Schema.ObjectId,ref:'Materials'},
        materialsQuantity:{type: String, default: null}
    }]

});



var loadingInfo=new Schema({
     materialId:{type: Schema.ObjectId,ref:'Materials'},
     materialQuantity:{type:String,required:true}
});


var unloadInfo=new Schema({
     materialId:{type: Schema.ObjectId,ref:'Materials'},
     totalQty:{type: Number, default: null},
     ok:{type:Number,required:true},
     missing:{type:Number,required:true},
     damaged:{type:Number,required:true},
     other:{type:Number,required:true}
});


var Bookings = new Schema({
    companyId: {type: Schema.ObjectId, ref: 'Company',default:null},
    driverId: {type: Schema.ObjectId, ref: 'Driver',default:null},
    bookingId:{type:Number,default:11111},
    truckId:{type: Schema.ObjectId, ref:'VendorTruck',default:null},
    bookDate:{type: Date, default:Date.now()},
    bookingDate:{type:Number,default:parseInt(+new Date()+19800000)},
    pickupAddress:{type: String, default:null},
    pickupLocationId:{type:Schema.ObjectId,ref:'Location',required:true},
    dropOffAddress: {type: String, default: null},
    truckNo: {type: String, default: null},
    truckType: {type: String, default: null},
    dropOffLocationId:{type:Schema.ObjectId,ref:'Location',default:null},
    lastLocation: {type: [Number], index: '2d'},
    locationLogs: [{type: [Number],index:'2d'}],
    loadingLocation:{type:[Number], index:'2d'},
    unloadingLocation: {type: [Number], index: '2d'},
    status :{  type: String, enum: [
        Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.PENDING,
        Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.ACCEPTED,
        Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.NO_DRIVER_FOUND,
        Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.TOWARDS_UNLOADING_LOCATION,
        Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.LOADING_LOCATION,
        Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.REJECTED,
        Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.ENDED,
        Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.UNLOADING_LOCATION,
        Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.TIMEOUT,
        Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.ON_LOADING_LOCATION,
        Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.ON_UNLOADING_LOCATION
    ]},
    requestTime: {type: Number, trim: true, default: null},
    acceptTime:{type: Number, trim: true, default: null},
    startTime:{type: Number, trim:true,default: null},
    endTime:{type: Number, trim:true,default: null},
    loadLocationReachTime:{type: Number, trim:true,default: null},
    loadLocationLeftTime:{type: Number, trim:true,default: null},
    unloadLocationReachTime:{type: Number, trim:true,default: null},
    unloadLocationLeftTime:{type: Number, trim:true,default: null},
    totalDistance: {type:Number,default:0},
    rejection:[Rejection],
    contractId:{type: Schema.ObjectId, ref:'Contract',required: true},
    materials:{type:[Schema.ObjectId]},
    unloadInfo:{type:[unloadInfo],default:[]},
    loadingInfo:{type:[loadingInfo],default:[]},
    loadingLocations:[loadingLocation],
    unloadingLocations:[unloadingLocation],
    driverIds:{type:[Schema.Types.ObjectId],ref:'Driver'},
    city:{type:String,trim:true},
    state:{type:String,trim:true},
    pincode:{type:Number},
    authPersonName:{type:String},
    countryCode:{type:String,default:'+91'},
    phoneNumber:{type:String},
    OTP:{type: String, default: null},
    otpVerified:{type:Boolean,default:false},
    driverAmount:{type:Number,default:0},
    driverDate:{type:Number,default:+new Date()},
    companyAmount:{type:Number,default:0},
    companyDate:{type:Number,default:+new Date()},

});


Bookings.index({'lastLocation.coordinates': "2d"});

unloadingLocation.index({'location.coordinates': "2d"});

loadingLocation.index({'location.coordinates': "2d"});


module.exports = mongoose.model('Bookings', Bookings);

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Config = require('../Config');

var Notification = new Schema({    
    to: {type: [Schema.Types.ObjectId],default:null},
    type:{type:String,enum:[
        Config.APP_CONSTANTS.DATABASE.USER_ROLES.COMPANY,
        Config.APP_CONSTANTS.DATABASE.USER_ROLES.VENDOR,
        Config.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER,
        Config.APP_CONSTANTS.DATABASE.USER_ROLES.LOADER
    ]},
    message:{type: String,required:true,trim:true},     
    date:{type:Date,default:parseInt(+new Date()+19800000)},
    isRead:{type:Boolean,default:false,required:true},
    isDeleted:{type:Boolean,default:false,required:true}
});



module.exports = mongoose.model('Notification', Notification);
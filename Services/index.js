
module.exports = {
    AdminService: require('./AdminService'),
    AppVersionService: require('./AppVersionService'),
    DriverService: require('./DriverService'),
    CompanyService: require('./CompanyService'),
    BookingService: require('./BookingService'),
    TruckService:require('./TruckService'),
    VendorService:require('./VendorService'),
    VendorTruckService:require('./VendorTruckService'),
    ContractsService : require('./ContractsService'),
    MaterialsService: require('./MaterialsService'),
    CityService:require('./CityService'),
    LoaderService:require('./LoaderService'),
    LocationService:require('./LocationService'),
    AccountService:require('./AccountService'),
    NotificationService:require('./NotificationService')
};
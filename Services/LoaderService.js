
'use strict';

var Models = require('../Models');

var getLoader= function(criteria,projection,option,callback){
    Models.Loader.find(criteria,projection,option,callback);
};

var createLoader = function (data,callback) {
    new Models.Loader(data).save(callback);
};

var deleteLoader = function (criteria,callback) {
    Models.Loader.remove(criteria,callback)
};
var updateLoader= function (criteria, dataToSet, options, callback) {
    Models.Loader.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var updateMultiLoader= function (criteria, dataToSet, options, callback) {
    Models.Loader.update(criteria, dataToSet, options, callback);
};

var getLoaderPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.Loader.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{



            callback(null, docs);
        }
    });
};


var getLoaderPopulateAdmin = function (criteria, project, options,populateModelArr, callback) {
    Models.Loader.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{


               var options=[
            {
                path:'assignedLocations.cityId',
                model:'City',
                select:{
                    isDeleted:0

                },
                match:{}
            },
             {
                path:'assignedLocations.companyId',
                model:'Company',
                select:{
                    countryCode:1,
                    contactNo:1,
                    email:1,
                    name:1,
                    state:1,
                    city:1,
                    address:1,
                    pincode:1,
                    profilePicURL:1
                },
                match:{}
            }
            
            ]
            Models.City.populate(docs,options,function (err,result) {
                if(err){
                    return callback(err);
                }else{
                    return callback(null,result);
                }

            });
        }
    });
};



















var getLoaderAggregated=function (aggregateArray,callback) {

    Models.Loader.aggregate(aggregateArray).exec(function (err,results) {
        callback(err,results);
    });
}


var getLoaderCount = function (criteria,callback) {
    Models.Loader.count(criteria, callback);
};


module.exports = {
   getLoader:getLoader,
   createLoader:createLoader,
   deleteLoader:deleteLoader,
   updateLoader:updateLoader,
   updateMultiLoader:updateMultiLoader,
   getLoaderPopulate:getLoaderPopulate,
   getLoaderAggregated:getLoaderAggregated,
   getLoaderCount:getLoaderCount,
   getLoaderPopulateAdmin:getLoaderPopulateAdmin
};


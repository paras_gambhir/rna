

/**
 * Created by parasgambhir on 18/04/17.
 */

'use strict';

var Models = require('../Models');

var getTruck= function(criteria,projection,option,callback){
    Models.Truck.find(criteria,projection,option,callback);
};

var createTruck = function (data,callback) {
    new Models.Truck(data).save(callback);
};

var deleteTruck = function (criteria,callback) {
    Models.Truck.remove(criteria,callback)
};
var updateTruck= function (criteria, dataToSet, options, callback) {
    Models.Truck.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var updateMultiTruck= function (criteria, dataToSet, options, callback) {
    Models.Truck.update(criteria, dataToSet, options, callback);
};

var getTruckPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.Truck.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

module.exports = {
    getTruck: getTruck,
    createTruck: createTruck,
    deleteTruck:deleteTruck,
    updateTruck:updateTruck,
    getTruckPopulate: getTruckPopulate,
    updateMultiTruck:updateMultiTruck
};


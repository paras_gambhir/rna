
/**
 * Created by parasgambhir on 20/05/17.
 */


'use strict';

var Models = require('../Models');

var getContract= function(criteria,projection,option,callback){
    Models.Contracts.find(criteria,projection,option,callback);
};

var createContract = function (data,callback) {
    new Models.Contracts(data).save(callback);
};

var deleteContract = function (criteria,callback) {
    Models.Contracts.remove(criteria,callback)
};
var updateContract = function (criteria, dataToSet, options, callback) {
    Models.Contracts.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var updateMultiContract= function (criteria, dataToSet, options, callback) {
    Models.Contracts.update(criteria, dataToSet, options, callback);
};

var getContractPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.Contracts.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};


var getContractsCount = function (criteria,callback) {
    Models.Contracts.count(criteria, callback);
};


module.exports = {
    getContract: getContract,
    createContract: createContract,
    deleteContract:deleteContract,
    updateContract:updateContract,
    getContractPopulate: getContractPopulate,
    updateMultiContract:updateMultiContract,
    getContractsCount:getContractsCount
};


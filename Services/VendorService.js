/**
 * Created by parasgambhir on 18/04/17.
 */


'use strict';

var Models = require('../Models');

var getVendor= function(criteria,projection,option,callback){
    Models.Vendor.find(criteria,projection,option,callback);
};

var createVendor = function (data,callback) {
    new Models.Vendor(data).save(callback);
};

var deleteVendor = function (criteria,callback) {
    Models.Vendor.remove(criteria,callback)
};
var updateVendor= function (criteria, dataToSet, options, callback) {
    Models.Vendor.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var updateMultiVendor= function (criteria, dataToSet, options, callback) {
    Models.Vendor.update(criteria, dataToSet, options, callback);
};

var getVendorPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.Vendor.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};


var getVendorCount = function (criteria,callback) {
    Models.Vendor.count(criteria, callback);
};



module.exports = {
    getVendor: getVendor,
    createVendor: createVendor,
    deleteVendor:deleteVendor,
    updateVendor:updateVendor,
    getVendorPopulate: getVendorPopulate,
    updateMultiVendor:updateMultiVendor,
    getVendorCount:getVendorCount
};


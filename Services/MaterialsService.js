
/**
 * Created by parasgambhir on 20/05/17.
 */


'use strict';

var Models = require('../Models');

var getMaterial= function(criteria,projection,option,callback){
    Models.Materials.find(criteria,projection,option,callback);
};

var createMaterial = function (data,callback) {
    new Models.Materials(data).save(callback);
};

var deleteMaterial = function (criteria,callback) {
    Models.Materials.remove(criteria,callback)
};
var updateMaterial = function (criteria, dataToSet, options, callback) {
    Models.Materials.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var updateMultiMaterial= function (criteria, dataToSet, options, callback) {
    Models.Materials.update(criteria, dataToSet, options, callback);
};

var getMaterialPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.Materials.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

module.exports = {
    getMaterial: getMaterial,
    createMaterial: createMaterial,
    deleteMaterial:deleteMaterial,
    updateMaterial:updateMaterial,
    getMaterialPopulate: getMaterialPopulate,
    updateMultiMaterial:updateMultiMaterial
};


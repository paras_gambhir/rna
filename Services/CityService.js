
'use strict';

var Models = require('../Models');

var getCity= function(criteria,projection,option,callback){
    Models.City.find(criteria,projection,option,callback);
};

var createCity = function (data,callback) {
    new Models.City(data).save(callback);
};

var deleteCity = function (criteria,callback) {
    Models.City.remove(criteria,callback)
};
var updateCity= function (criteria, dataToSet, options, callback) {
    Models.City.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var updateMultiCity= function (criteria, dataToSet, options, callback) {
    Models.City.update(criteria, dataToSet, options, callback);
};

var getCityPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.City.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};


var getCityCount = function (criteria,callback) {
    Models.City.count(criteria, callback);
};

module.exports = {
    getCity:getCity,
    createCity:createCity,
    deleteCity:deleteCity,
    updateCity:updateCity,
    updateMultiCity:updateMultiCity,
    getCityPopulate:getCityPopulate,
    getCityCount:getCityCount
};


/**
 * Created by parasgambhir on 18/04/17.
 */


'use strict';

var Models = require('../Models');

var getVendorTruck= function(criteria,projection,option,callback){
    Models.VendorTruck.find(criteria,projection,option,callback);
};

var createVendorTruck = function (data,callback) {
    new Models.VendorTruck(data).save(callback);
};

var deleteVendorTruck = function (criteria,callback) {
    Models.VendorTruck.remove(criteria,callback)
};
var updateVendorTruck= function (criteria, dataToSet, options, callback) {
    Models.VendorTruck.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var updateMultiVendorTruck= function (criteria, dataToSet, options, callback) {
    Models.VendorTruck.update(criteria, dataToSet, options, callback);
};

var getVendorTruckPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.VendorTruck.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

module.exports = {
    getVendorTruck: getVendorTruck,
    createVendorTruck: createVendorTruck,
    deleteVendorTruck:deleteVendorTruck,
    updateVendorTruck:updateVendorTruck,
    getVendorTruckPopulate: getVendorTruckPopulate,
    updateMultiVendorTruck:updateMultiVendorTruck
};


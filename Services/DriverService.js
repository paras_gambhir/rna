'use strict';

var Models = require('../Models');

var getDriver= function(criteria,projection,option,callback){
    Models.Driver.find(criteria,projection,option,callback);
};

var createDriver = function (data,callback) {
    new Models.Driver(data).save(callback);
};

var deleteDriver = function (criteria,callback) {
    Models.Driver.remove(criteria,callback)
};
var updateDriver= function (criteria, dataToSet, options, callback) {
    Models.Driver.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var updateMultiDriver= function (criteria, dataToSet, options, callback) {
    Models.Driver.update(criteria, dataToSet, options, callback);
};

var getDriverPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.Driver.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};


var getDriverCount = function (criteria,callback) {
    Models.Driver.count(criteria, callback);
};


var createMessage = function (data,callback) {
    new Models.Message(data).save(callback);
};

var getMessagePopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.Message.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

var getMessagesCount = function (criteria,callback) {
    Models.Message.count(criteria, callback);
};


module.exports = {
    getDriver: getDriver,
    createDriver: createDriver,
    deleteDriver:deleteDriver,
    updateDriver:updateDriver,
    getDriverPopulate: getDriverPopulate,
    updateMultiDriver:updateMultiDriver,
    getDriverCount:getDriverCount,
    createMessage:createMessage,
    getMessagePopulate:getMessagePopulate,
    getMessagesCount:getMessagesCount
};


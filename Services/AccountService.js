
'use strict';

var Models = require('../Models');

var getAccount= function(criteria,projection,option,callback){
    Models.Account.find(criteria,projection,option,callback);
};

var createAccount = function (data,callback) {
    new Models.Account(data).save(callback);
};

var deleteAccount = function (criteria,callback) {
    Models.Account.remove(criteria,callback)
};
var updateAccount= function (criteria, dataToSet, options, callback) {
    Models.Account.findOneAndUpdate(criteria, dataToSet, options, callback);
};


var getAccountPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.Account.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};


var getAccountCount = function (criteria,callback) {
    Models.Account.count(criteria, callback);
};

module.exports = {
    getAccount:getAccount,
    createAccount:createAccount,
    deleteAccount:deleteAccount,
    updateAccount:updateAccount,
    getAccountPopulate:getAccountPopulate,
    getAccountCount:getAccountCount
};


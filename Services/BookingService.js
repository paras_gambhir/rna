/**
 * Created by parasgambhir on 18/04/17.
 */


'use strict';

var Models = require('../Models');

var getBooking= function(criteria,projection,option,callback){
    Models.Bookings.find(criteria,projection,option,callback);
};

var createBookings = function (data,callback) {
    new Models.Bookings(data).save(callback);
};

var deleteBookings = function (criteria,callback) {
    Models.Bookings.remove(criteria,callback)
};
var updateBookings= function (criteria, dataToSet, options, callback) {
    Models.Bookings.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var updateMultiBookings= function (criteria, dataToSet, options, callback) {
    Models.Bookings.update(criteria, dataToSet, options, callback);
};

var getBookingsPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.Bookings.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
       
        if (err) {
            return callback(err, docs);
        }else{
            if(docs.length){
                
               var options=[
            {
                path:'driverId.VendorTruck',
                model:'VendorTruck',
                select:{
                    documents:0,
                    startCityId:0,
                    endCityId:0,

                },
                match:{}
            },
             {
                path:'driverId.vendorId',
                model:'Vendor',
                select:{
                    countryCode:1,
                    contactNo:1,
                    email:1,
                    name:1,
                    state:1,
                    city:1,
                    address:1,
                    pincode:1,
                    profilePicURL:1
                },
                match:{}
            },
            {
                path:'contractId.startCityId',
                model:'City',
                select:{
                     cityName:1,
                    location:1
                },
                match:{}
            },
            {
                path:'contractId.endCityId',
                model:'City',
                select:{
                     cityName:1,
                    location:1
                },
                match:{}
            },
             {
                path:'contractId.truckId',
                model:'Truck',
                select:{
                    truckName:1,
                    Capacity:1,
                    length:1,
                    breadth:1,
                    height:1
                },
                match:{}
            }
            
            ]
            Models.Truck.populate(docs,options,function (err,result) {
                if(err){
                    return callback(err);
                }else{
                    console.log('DDDDDDDDDDDDDDDDDDDdd',result)
                    return callback(null,result);
                }

            });
            }else{
                callback(null,[]);
            }

        }
    });
};

var getBookingsPopulateDriverUpdateRide = function (criteria, project, options,populateModelArr, callback) {
    Models.Bookings.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
       
        if (err) {
            return callback(err, docs);
        }else{
            

               var options=[
            {
                path:'contractId.startCityId',
                model:'City',
                select:{
                     cityName:1,
                    location:1
                },
                match:{}
            },
            {
                path:'contractId.endCityId',
                model:'City',
                select:{
                     cityName:1,
                    location:1
                },
                match:{}
            }
            
            ]
            Models.Truck.populate(docs,options,function (err,result) {
                if(err){
                    return callback(err);
                }else{
                    console.log('DDDDDDDDDDDDDDDDDDDdd',result)
                    return callback(null,result);
                }

            });
        }
    });
};




var getBookingsPopulateDriver = function (criteria, project, options,populateModelArr, callback) {
    Models.Bookings.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
       
        if (err) {
            return callback(err, docs);
        }else{
            

               var options=[
            {
                path:'contractId.startCityId',
                model:'City',
                select:{
                     cityName:1,
                    location:1
                },
                match:{}
            },
            {
                path:'contractId.endCityId',
                model:'City',
                select:{
                     cityName:1,
                    location:1
                },
                match:{}
            },
             {
                path:'contractId.truckId',
                model:'Truck',
                select:{
                    truckName:1,
                    Capacity:1,
                    length:1,
                    breadth:1,
                    height:1
                },
                match:{}
            },
            {
                path:'driverId.VendorTruck',
                model:'VendorTruck',
                select:{
                    truckNo:1,
                    startAddress:1,
                    endAddress:1,
                    documents:1
                },
                match:{}
            }
            
            ]
            Models.Truck.populate(docs,options,function (err,result) {
                if(err){
                    return callback(err);
                }else{
                    console.log('DDDDDDDDDDDDDDDDDDDdd',result)
                    return callback(null,result);
                }

            });
        }
    });
};


var getBookingAggregated=function (aggregateArray,callback) {

    Models.Bookings.aggregate(aggregateArray).exec(function (err,results) {
        callback(err,results);
    });
}

var getBookingCount = function (criteria,callback) {
    Models.Bookings.count(criteria, callback);
};


module.exports = {
    getBooking: getBooking,
    createBookings: createBookings,
    deleteBookings:deleteBookings,
    updateBookings:updateBookings,
    getBookingsPopulate: getBookingsPopulate,
    updateMultiBookings:updateMultiBookings,
    getBookingAggregated:getBookingAggregated,
    getBookingsPopulateDriver:getBookingsPopulateDriver,
    getBookingsPopulateDriverUpdateRide:getBookingsPopulateDriverUpdateRide,
    getBookingCount:getBookingCount
};


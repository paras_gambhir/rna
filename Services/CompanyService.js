/**
 * Created by parasgambhir on 18/04/17.
 */

'use strict';

var Models = require('../Models');

var getCompany= function(criteria,projection,option,callback){
    Models.Company.find(criteria,projection,option,callback);
};

var createCompany = function (data,callback) {
    new Models.Company(data).save(callback);
};

var deleteCompany = function (criteria,callback) {
    Models.Company.remove(criteria,callback)
};
var updateCompany= function (criteria, dataToSet, options, callback) {
    Models.Company.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var updateMultiCompany= function (criteria, dataToSet, options, callback) {
    Models.Company.update(criteria, dataToSet, options, callback);
};

var getCompanyPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.Company.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};


var getCompaniesCount = function (criteria,callback) {
    Models.Company.count(criteria, callback);
};

module.exports = {
    getCompany: getCompany,
    createCompany: createCompany,
    deleteCompany:deleteCompany,
    updateCompany:updateCompany,
    getCompanyPopulate: getCompanyPopulate,
    updateMultiCompany:updateMultiCompany,
    getCompaniesCount:getCompaniesCount
};


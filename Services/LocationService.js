
'use strict';

var Models = require('../Models');

var getLocation= function(criteria,projection,option,callback){
    Models.Location.find(criteria,projection,option,callback);
};

var createLocation = function (data,callback) {
    new Models.Location(data).save(callback);
};

var deleteLocation = function (criteria,callback) {
    Models.Location.remove(criteria,callback)
};
var updateLocation= function (criteria, dataToSet, options, callback) {
    Models.Location.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var updateMultiLocation= function (criteria, dataToSet, options, callback) {
    Models.Location.update(criteria, dataToSet, options, callback);
};

var getLocationPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.Location.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            return callback(null,docs)
        }
    });
};

var getLocationsCount = function (criteria,callback) {
    Models.Location.count(criteria, callback);
};



var getLocationAggregated=function (aggregateArray,callback) {

    Models.Location.aggregate(aggregateArray).exec(function (err,results) {
        callback(err,results);
    });
}

module.exports = {
   getLocation:getLocation,
   createLocation:createLocation,
   deleteLocation:deleteLocation,
   updateLocation:updateLocation,
   updateMultiLocation:updateMultiLocation,
   getLocationPopulate:getLocationPopulate,
   getLocationsCount:getLocationsCount,
   getLocationAggregated:getLocationAggregated
};


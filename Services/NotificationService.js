
'use strict';

var Models = require('../Models');

var getNotification= function(criteria,projection,option,callback){
    Models.Notification.find(criteria,projection,option,callback);
};

var createNotification = function (data,callback) {
    new Models.Notification(data).save(callback);
};

var deleteNotification= function (criteria,callback) {
    Models.Notification.remove(criteria,callback)
};
var updateNotification= function (criteria, dataToSet, options, callback) {
    Models.Notification.findOneAndUpdate(criteria, dataToSet, options, callback);
};



var getNotificationCount = function (criteria,callback) {
    Models.Notification.count(criteria, callback);
};

module.exports = {
    getNotification:getNotification,
    createNotification:createNotification,
    deleteNotification:deleteNotification,
    updateNotification:updateNotification,
    getNotificationCount:getNotificationCount
};


/**
 * Created by paras on 11/2/17
 */
'use strict';
var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var andr = require('../Utils/UniversalFunctions');
var ERROR = ('../Utils/UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR');
var Config = require('../Config');
var async = require('async');
var UploadManager = require('../Lib/UploadManager');
var TokenManager = require('../Lib/TokenManager');
var NotificationManager = require('../Lib/NotificationManager');
var SocketManager = require('../Lib/SocketManager');
var Jwt = require('jsonwebtoken');
var _ = require('lodash');
var schedule = require('node-schedule');
var moment = require('moment');
var request = require('request');
var googleMapsClient = require('@google/maps').createClient({
    key: 'AIzaSyCHtn-swl0C2kSzdlyWzqjr-z9X17FGxYE'
});

var geolib = require('geolib');


exports.requestDriver = function (payloadData, callback) {

    var customerData;
    var data;
    var rideId;
    var ToDeviceType;
    var ToDeviceToken;
    var pickUpLocation = payloadData.pickupLocation,
        dropOffLocation = payloadData.dropOffLocation;
    var priceData;
    var dataTosend = payloadData;
    var estimatedCost;
    var busyDrivers = [];
    async.auto({
            checkUser: function (cb) {
                var criteria = {
                    accessToken: payloadData.accessToken
                };
                var projection = {
                    _id: 1,
                    firstName: 1,
                    lastName: 1,
                    profilePicURL: 1,
                    countryCode: 1,
                    phoneNumber: 1
                };
                var options = {
                    lean: true
                };

                Service.CustomerService.getCustomer(criteria, projection, options, function (err, result) {
                    if (err) {
                        callback(err);
                    }
                    else {
                        customerData = result[0];
                        cb(null);
                    }
                })
            },
            getPrice: ['checkUser', function (cb) {
                var criteria = {
                    carCategory: payloadData.carType
                };
                var projection = {
                    minFare: 1,
                    zeroToFiveKm: 1,
                    nextFiveKm: 1
                };

                var options = {lean: true};
                Service.CarTypeService.getCarTyoe(criteria, projection, options, function (err, result) {
                    if (err) {
                        callback(err);
                    }
                    else {
                        priceData = result[0];
                        console.log("price data ", priceData);
                        cb(null)
                    }
                })
            }],
            createRide: ['getPrice', function (cb) {

                var obj;
                if(payloadData.shoferCode)
                {
                     obj = {
                        customerId: customerData._id,
                        pickUpLocation: payloadData.pickupLocation,
                        previousLocation: payloadData.pickupLocation,
                        dropOffLocation: payloadData.dropOffLocation,
                        carType: priceData._id,
                        requestTime: new Date().toISOString(),
                        status: Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.PENDING,
                        pickupAddress: payloadData.pickupAddress,
                        dropOffAddress: payloadData.dropOffAddress,
                        shoferCode: payloadData.shoferCode

                    };
                }
                else{
                     obj = {
                        customerId: customerData._id,
                        pickUpLocation: payloadData.pickupLocation,
                        previousLocation: payloadData.pickupLocation,
                        dropOffLocation: payloadData.dropOffLocation,
                        carType: priceData._id,
                        requestTime: new Date().toISOString(),
                        status: Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.PENDING,
                        pickupAddress: payloadData.pickupAddress,
                        dropOffAddress: payloadData.dropOffAddress

                    };
                }

                console.log("create ride obj ", obj);

                Service.RideService.createRide(obj, function (err, result) {
                    if (err) {
                        cb(err);
                    }
                    else {
                        rideId = result._id;
                        cb(null);
                    }
                })
            }],
            getBusyDrivers:['createRide',function(cb)
            {
                var criteria = {
                    isDeleted: false
                };
                var projection = {
                    driverId : 1,
                    rideId:1
                };

                var options = {lean: true};
                Service.BusyDriverService.getBusyDriver(criteria, projection, options, function (err, result) {
                    if (err) {
                        callback(err);
                    }
                    else {
                         if(result.length)
                         {
                            busyDrivers = result.unique();
                         }
                        cb(null)
                    }
                })

            }],
            searchNearbyDriver: ['checkUser', 'createRide', function (cb) {

                if(payloadData.shoferCode)
                {
                    var criteria = {
                        currentLocation: {
                            $nearSphere: payloadData.pickupLocation,
                            $maxDistance: Config.APP_CONSTANTS.SERVER.MAX_DISTANCE_RADIUS_TO_SEARCH
                        },
                        isAvailable: true,
                        carType: payloadData.carType,
                        isBlocked: false,
                        _id: {'$nin': busyDrivers},
                        shoferCode:payloadData.shoferCode
                    };
                }
                else{
                    var criteria = {
                        currentLocation: {
                            $nearSphere: payloadData.pickupLocation,
                            $maxDistance: Config.APP_CONSTANTS.SERVER.MAX_DISTANCE_RADIUS_TO_SEARCH
                        },
                        isAvailable: true,
                        carType: payloadData.carType,
                        isBlocked: false,
                        _id: {'$nin': busyDrivers}
                    };
                }


                var projection = {
                    _id: 1,
                    deviceType: 1,
                    deviceToken: 1
                };

                var option = {
                    limit: 1
                };

                Service.DriverService.getDriver(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    }
                    else if (result.length) {
                        data = result[0]._id;
                        ToDeviceType = result[0].deviceType;
                        ToDeviceToken = result[0].deviceToken;

                        var time = new Date();
                        tempRides.push({
                            "rideId": rideId,
                            "requestTime": time,
                            "driverId": data,
                            "time": time
                        });
                        cb(null);
                    }
                    else {
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DRIVER_NOT_FOUND)

                    }
                });
            }],
            insertBusyDriver:['searchNearbyDriver',function(cb)
            {
                var obj = {
                   rideId: rideId,
                   driverId: data

                };
                console.log("create busy ride obj ", obj);

                Service.BusyDriverService.createBusyDriver(obj, function (err, result) {
                    if (err) {
                        cb(err);
                    }
                    else {
                        cb(null);
                    }
                })

            }],
            sendPushNotification: ['insertBusyDriver', function (cb) {

                var temp = {};
                temp.name = customerData.firstName + " " + customerData.lastName;
                temp.profilePicURL = customerData.profilePicURL;
                temp.rideId = rideId;
                temp.customerId = customerData._id;
                temp.pushType = 0;
                temp.phoneNo = customerData.countryCode + "" + customerData.phoneNumber;
                temp.pickUpLocation = pickUpLocation;
                temp.dropOffLocation = dropOffLocation;
                temp.title = customerData.firstName + " " + customerData.lastName + " sends you new ride request";
                var title = "New ride request";
                sendPushNotification(ToDeviceType, ToDeviceToken, temp, title, function (err) {
                    if (err) {
                        cb(null)
                    } else {
                        cb(null)
                    }
                })
            }]
        },
        function (err, result) {
            if (err) {
                callback(err)
            } else {
                console.log("successfully requested");
                callback(null, {"rideId":rideId})
            }
        })
};



var sendPushNotification = function (deviceType, deviceToken, data, title, cb) {
    if (deviceType == "IOS" && deviceToken.length) {
        var temp = {};
        temp = data;

        NotificationManager.sendAndroidPushNotificationForDriver(deviceToken, temp, title, function (err, result) {
            if (err) {
                cb(err)
            } else {
                cb(null)
            }
        })
    } else if (deviceType == "ANDROID") {
        var temp = {};
        temp = data;

        NotificationManager.sendAndroidPushNotificationForDriver(deviceToken, temp, title, function (err, result) {
            if (err) {
                cb(err)
            } else {
                cb(null)
            }
        })
    }
};



exports.requestNextDriver = function () {

    var j = schedule.scheduleJob('*/10 * * * * *', function () {      //push and pop tempRides
        console.log("start schedule again");
        if (tempRides.length) {
            console.log("tempRides...........", tempRides);
            var date = new Date();
            var rides = [];
            var maxTimeReachedRides = [];

            for (var i = 0; i < tempRides.length; i++) {
                (function (i) {

                    if ((date - tempRides[i].time) / 1000 <= Config.APP_CONSTANTS.SERVER.MAX_TIME) {
                        if ((date - tempRides[i].requestTime) / 1000 >= Config.APP_CONSTANTS.SERVER.MAX_TIME_TO_SEND_REQUEST) {

                            rides.push({
                                "rideId": tempRides[i].rideId,
                                "requestTime": date,
                                "driverId": tempRides[i].driverId,
                                "time": tempRides[i].time
                            });
                        }


                        if (i == tempRides.length - 1) {

                                updateRidesInsideScheduler(rides,maxTimeReachedRides,function(err,result)
                                {


                                });
                        }
                    }
                    else{
                        maxTimeReachedRides.push(tempRides[i].rideId);
                        console.log("to be handled case");
                        if (i == tempRides.length - 1) {

                                updateRidesInsideScheduler(rides,maxTimeReachedRides,function(err,result)
                                {


                                });
                        }


                    }

                }(i))
            }
        } else {
            console.log("no temp jobs");
        }

    })
};



var updateRidesInsideScheduler = function(rides,maxTimeReachedRides,callback){

    async.parallel([
        function(cb){
          if(rides.length)
          {
              requestNextDrivers(rides,function(err,result)
              {
                  if(err){
                      cb(err)
                  }
                  else{
                      cb();
                  }

              });
          }
          else{
              console.log("no rides");
              cb();
          }

        },
        function(cb){
            if(maxTimeReachedRides.length)
            {
                updateMaxTimeReachedRides(maxTimeReachedRides,function(err,result)
                {
                    if(err){
                        cb(err)
                    }
                    else{
                        cb();
                    }

                });

            }
            else{
                console.log("max time rides this time");
                cb();
            }

        }
    ],function(err,result)
    {
        if(err){
            callback(err)
        }
        else{
            callback();
        }

    })


};



var requestNextDrivers = function (rides, callback) {


    var tasks = [];


    for(var i = 0 ; i < rides.length ; i++)
    {
        tasks.push(function(e)
        {
            return function(cb1){

                console.log("inside ride details");
                var busyDrivers=[];
                var previousDriver;
                var busyDriverIndex;
                var data;
                var driverId;
                var profilePicURL;
                var name;
                var phoneNo;
                var customerId;
                var pickUpLocation;
                var dropOffLocation;
                var ToDeviceType;
                var ToDeviceToken;
                var customerDeviceType;
                var customerDeviceToken;
                async.auto({
                    getRidesDetails: function (cb) {
                        var query = {
                            _id: rides[e].rideId
                        };
                        var projection = {
                            customerId: 1,
                            _id: 1,
                            dropOffLocation: 1,
                            carType: 1,
                            pickUpLocation: 1,
                            rejectDriverID: 1

                        };
                        var options = {
                            lean: true
                        };
                        var populate = [
                            {
                                path: 'customerId',
                                match: {},
                                select: "profilePicURL firstName lastName countryCode phoneNumber deviceType deviceToken",
                                options: {}
                            },
                            {
                                path: 'carType',
                                match: {},
                                select: "carCategory",
                                options: {}
                            }
                        ];
                        Service.RideService.getRidePopulate(query, projection, options, populate, function (err, result) {
                            if (err) {
                                console.log(err);
                                cb(err)
                            } else {
                                data = result[0];
                                profilePicURL = data.customerId.profilePicURL;
                                name = data.customerId.firstName + " " + data.customerId.lastName;
                                phoneNo = data.customerId.countryCode + "" + data.customerId.phoneNumber;
                                customerId = data.customerId._id;
                                pickUpLocation = data.pickUpLocation;
                                dropOffLocation = data.dropOffLocation;
                                customerDeviceToken = data.customerId.deviceToken;
                                customerDeviceType = data.customerId.deviceType;
                                console.log("pickup location is ", pickUpLocation);
                                cb(null);
                            }
                        })
                    },
                    getBusyDrivers:['getRidesDetails',function(cb)
                    {
                        var criteria = {
                            isDeleted: false
                        };
                        var projection = {
                            driverId : 1,
                            rideId:1
                        };

                        var options = {lean: true};
                        Service.BusyDriverService.getBusyDriver(criteria, projection, options, function (err, result) {
                            if (err) {
                                callback(err);
                            }
                            else {
                                if(result.length)
                                {
                                    busyDrivers = result.unique();
                                }
                                cb(null)
                            }
                        })

                    }],
                    findDriver: ['getBusyDrivers', function (cb) {
                        console.log("find driver in schedule");
                        console.log("busy drivers", busyDrivers);
                        var filter = busyDrivers;

                        console.log("not in drivers ", filter);

                        if(data.carType.carCategory == "SHOFER")
                        {
                            var query = {
                                currentLocation: {
                                    $nearSphere: pickUpLocation,
                                    $maxDistance: Config.APP_CONSTANTS.SERVER.MAX_DISTANCE_RADIUS_TO_SEARCH
                                },
                                isAvailable: true,
                                carType: data.carType.carCategory,
                                _id: {'$nin': filter}
                            };
                        }
                        else{
                            var query = {
                                currentLocation: {
                                    $nearSphere: pickUpLocation,
                                    $maxDistance: Config.APP_CONSTANTS.SERVER.MAX_DISTANCE_RADIUS_TO_SEARCH
                                },
                                isAvailable: true,
                                carType: data.carType.carCategory,
                                _id: {'$nin': filter}
                            };
                        }

                        var options = {
                            limit: 1
                        };
                        var projections = {
                            _id: 1,
                            deviceType: 1,
                            deviceToken: 1

                        };
                        Service.DriverService.getDriver(query, projections, options, function (err, result) {
                            if (err) {
                                console.log("err mein aaya in find driver");
                                callback(err);
                            } else {
                                console.log("driver in schedule", result);
                                if (result.length) {
                                    ToDeviceType = result[0].deviceType;
                                    ToDeviceToken = result[0].deviceToken;
                                    driverId = result[0]._id;

                                    previousDriver = rides[e].driverId;

                                    console.log("............. driverId", driverId, "...previousDriver", previousDriver, ".busyDriver.....", busyDrivers)


                                    var tempRideIndex = _.findIndex(tempRides, {
                                        rideId: rides[e].rideId
                                    });

                                    console.log(".tempRideIndex.........", tempRideIndex, "......driverId...", driverId);

                                    if (tempRideIndex != -1) {
                                        tempRides.splice(tempRideIndex, 1);
                                    }

                                    tempRides.push({
                                        "rideId": rides[e].rideId,
                                        "requestTime": new Date(),
                                        "driverId": driverId,
                                        "time": rides[e].time
                                    });


                                    var criteria = {
                                        driverId:previousDriver,
                                        rideId:rides[e].rideId
                                    };

                                    var dataToUpdate={
                                        isDeleted: true
                                    };

                                    var options={
                                        multi:true
                                    };

                                    Service.BusyDriverService.updateBusyDriver(criteria,dataToUpdate,options,function (err,result) {
                                        if(err){
                                           console.log("error in previous driver updations");
                                        }
                                        else {
                                            console.log("success of previous driver free");
                                        }
                                    });

                                    var obj = {
                                        rideId: rides[e].rideId,
                                        driverId: driverId

                                    };
                                    console.log("create busy ride obj ", obj);

                                    Service.BusyDriverService.createBusyDriver(obj, function (err, result) {
                                        if (err) {
                                           console.log("error in creating new busy driver")
                                        }
                                        else {
                                            console.log("success of new driver busy");
                                        }
                                    });

                                    console.log("length of tempRides........", tempRides.length);
                                    cb(null)

                                }

                                else {
                                    console.log("not found,remove all busy drivers beta");
                                    var criteria = {
                                        rideId:rides[e].rideId
                                    };

                                    var dataToUpdate={
                                        isDeleted: true
                                    };

                                    var options={
                                        multi:true
                                    };

                                    Service.BusyDriverService.updateBusyDriver(criteria,dataToUpdate,options,function (err,result) {
                                        if(err){
                                            console.log("error in all previous driver updations");
                                            cb(err);
                                        }
                                        else {
                                            console.log("success of all previous drivers free");
                                            cb(null);
                                        }
                                    });
                                }
                            }
                        })
                    }],

                    sendPushNotification: ['findDriver', function (cb) {

                        if(driverId)
                        {
                            console.log("push notification inside fn");
                            var temp = {};
                            temp.name = name;
                            temp.profilePicURL = profilePicURL;
                            temp.phoneNo = phoneNo;
                            temp.rideId = rides[e].rideId;
                            temp.customerId = customerId;
                            temp.pushType = 0;
                            temp.pickUpLocation = pickUpLocation;
                            temp.dropOffLocation = dropOffLocation;
                            temp.title = name + " Sends you new ride request";
                            var title = "New ride request";
                            console.log("to device token", ToDeviceToken);
                            sendPushNotification(ToDeviceType, ToDeviceToken, temp, title, function (err) {
                                if (err) {
                                    cb(null)
                                } else {
                                    cb(null)
                                }
                            })
                        }
                        else {
                            cb(null);
                        }
                    }]

                }, function (err, result) {
                    if (err) {
                        cb1(err);
                    } else {
                        cb1(null);
                    }
                })

            }

        }(i))

    }

    async.series(tasks,function(err,result)
    {
        console.log("err ",err);
        callback(null);
    });

};



var updateMaxTimeReachedRides = function(rides,callback)
{

    var tasks = [];

    for(var i = 0 ; i < rides.length ; i++)
    {

            tasks.push(function(e)
            {
                return function(cb1)
                {
                    async.auto({
                        'freeBusyDriver':function(cb){
                            var criteria = {
                                rideId:rides[e]
                            };

                            var dataToUpdate={
                                isDeleted: true
                            };

                            var options={
                                multi:true
                            };

                            Service.BusyDriverService.updateBusyDriver(criteria,dataToUpdate,options,function (err,result) {
                                if(err){
                                    console.log("error");
                                    cb(err);
                                }
                                else {
                                    console.log("success");
                                    cb(null);
                                }
                            });
                        },
                        'popTempRideFinal':['freeBusyDriver',function(cb)
                        {
                            var tempRideIndex = _.findIndex(tempRides, {
                                rideId: rides[e]
                            });

                            if (tempRideIndex != -1) {
                                tempRides.splice(tempRideIndex, 1);
                            }
                            cb(null);

                        }],
                        updateRide:['popTempRideFinal',function(cb)
                        {
                            var criteria = {
                                _id:rides[e]
                            };

                            var dataToUpdate={
                                status: 'TIMEOUT'
                            };

                            var options={
                                new:true
                            };

                            Service.RideService.updateRide(criteria,dataToUpdate,options,function (err,result) {
                                if(err){
                                    console.log("error");
                                    cb(err);
                                }
                                else {
                                    console.log("success");
                                    cb(null);
                                }
                            });

                        }]

                    },function(err,result)
                    {
                        console.log("err ",err );
                        cb1(null);

                    })
                }

            }(i))

    }

    async.series(tasks,function(err,result)
    {
        console.log("err ",err);
        callback(null);
    });
};



Array.prototype.unique = function() {
    var unique = [];
    for (var i = 0; i < this.length; i++) {
        if (unique.indexOf(this[i].driverId) == -1) {
            unique.push(this[i].driverId);
        }
    }
    return unique;
};
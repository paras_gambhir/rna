'use strict';

var Config = require('../Config');
var async = require('async');

var client = require('twilio')(Config.smsConfig.twilioCredentials.accountSid, Config.smsConfig.twilioCredentials.authToken);
var nodeMailerModule = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var transporter = nodeMailerModule.createTransport(Config.emailConfig.nodeMailer.Mandrill);
var FCM = require('fcm-push');
var apn = require('apn');

var sendPUSHToUser = function (pushToSend, callback) {
    callback();
};

var sendSMSToUser = function (four_digit_verification_code, countryCode, phoneNo, externalCB) {
    console.log('sendSMSToUser');

    var templateData = Config.APP_CONSTANTS.notificationMessages.verificationCodeMsg;
    var variableDetails = {
        four_digit_verification_code: four_digit_verification_code
    };

    var smsOptions = {
        from: Config.smsConfig.twilioCredentials.smsFromNumber,
        To: countryCode || '' + phoneNo.toString(),
        Body: null
    };

    async.series([
        function (internalCallback) {
            smsOptions.Body = renderMessageFromTemplateAndVariables(templateData, variableDetails);
            internalCallback();
        }, function (internalCallback) {
            sendSMS(smsOptions, function (err, res) {
                internalCallback(err, res);
            })
        }
    ], function (err, responses) {
        if (err) {
            externalCB(err);
        } else {
            externalCB(null,{});
        }
    });
};

var sendSMSToLoader = function (message,countryCode,  phoneNo, externalCB) {
    console.log('sendSMSToLoader');

    var smsOptions = {
        from: Config.smsConfig.twilioCredentials.smsFromNumber,
        To:countryCode || ''+ phoneNo.toString(),
        Body: message
    };

   sendSMS(smsOptions, function (err, res) {
        externalCB(err);
    })
};


var sendEmailToUser = function (emailType ,emailVariables, emailId, callback) {
    var mailOptions = {
        from: 'rnatester29@gmail.com',
        to: emailId,
        subject: null,
        html: null
    };
    async.series([
        function(cb){
            switch (emailType){
                case 'REGISTRATION_MAIL' :
                    mailOptions.subject = Config.APP_CONSTANTS.notificationMessages.registrationEmail.emailSubject;
                    mailOptions.html = renderMessageFromTemplateAndVariables(emailVariables.userType=='VENDOR'?Config.APP_CONSTANTS.notificationMessages.registrationEmail.emailMessageVendor:Config.APP_CONSTANTS.notificationMessages.registrationEmail.emailMessageCompany, emailVariables) ;
                    break;
                case 'FORGOT_PASSWORD' :
                    mailOptions.subject = Config.APP_CONSTANTS.notificationMessages.forgotPassword.emailSubject;
                    mailOptions.html = renderMessageFromTemplateAndVariables(Config.APP_CONSTANTS.notificationMessages.forgotPassword.emailMessage, emailVariables) ;
                    break;
                case 'DRIVER_CONTACT_FORM' :
                    mailOptions.subject = Config.APP_CONSTANTS.notificationMessages.contactDriverForm.emailSubject;
                    mailOptions.html = renderMessageFromTemplateAndVariables(Config.APP_CONSTANTS.notificationMessages.contactDriverForm.emailMessage, emailVariables) ;
                    break;
                case 'BUSINESS_CONTACT_FORM' :
                    mailOptions.subject = Config.APP_CONSTANTS.notificationMessages.contactBusinessForm.emailSubject;
                    mailOptions.html = renderMessageFromTemplateAndVariables(Config.APP_CONSTANTS.notificationMessages.contactBusinessForm.emailMessage, emailVariables) ;
                    break;
                case 'OTP' :
                    mailOptions.subject = "Verification Email from RNA";
                    mailOptions.body = "Your verification code is "+emailVariables.OTP
                    break;
                case 'NO_DRIVER_FOUND':
                    mailOptions.subject=Config.APP_CONSTANTS.notificationMessages.noDriverFound.emailSubject;
                    mailOptions.html=renderMessageFromTemplateAndVariables(Config.APP_CONSTANTS.notificationMessages.noDriverFound.emailMessage,emailVariables)  
                case 'POST_QUERY':
                       mailOptions.subject=Config.APP_CONSTANTS.notificationMessages.postQuery.emailSubject;
                    mailOptions.html=renderMessageFromTemplateAndVariables(Config.APP_CONSTANTS.notificationMessages.postQuery.emailMessage,emailVariables)  
            }
            cb();

        },function(cb){
            sendMailViaTransporter(mailOptions, function(err,res){
                cb(err,res);
            })
        }
    ], function (err, responses) {
        if (err){
            callback(err);
        }else {
            callback();
        }
    });

};

function renderMessageFromTemplateAndVariables(templateData, variablesData) {
    var Handlebars = require('handlebars');
    return Handlebars.compile(templateData)(variablesData);
}

/*
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @ sendSMS Function
 @ This function will initiate sending sms as per the smsOptions are set
 @ Requires following parameters in smsOptions
 @ from:  // sender address
 @ to:  // list of receivers
 @ Body:  // SMS text message
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 */
function sendSMS(smsOptions, cb) {
    client.messages.create(smsOptions, function (err, message) {
        if (err) {
            console.log(err)
        }
        else {
            console.log(message);
        }
    });
    cb(null, null); // Callback is outside as sms sending confirmation can get delayed by a lot of time
}


/*
 ==========================================================
 Send the notification to the iOS device for customer
 ==========================================================
 */
var sendIosPushNotification = function(iosDeviceToken, badgeCount, payload,callback)
{

    var debugging_enabled=1;

    var path;
    path = __dirname + "/wizzrideProd.pem";




    var DEBUG=apn;
    var status = 1;
    var msg = payload;
    console.log("msg.......",msg)

    var snd = 'ping.aiff';

    var options = {
        cert: path ,
        certData: null,
        key:path,
        keyData: null,
        passphrase: '12345',
        ca: null,
        pfx: null,
        pfxData: null,
        gateway: 'gateway.sandbox.push.apple.com',
        port: 2195,
        rejectUnauthorized: true,
        enhanced: true,
        cacheLength: 100,
        autoAdjustCache: true,
        connectionTimeout: 0,
        ssl: true
        // production:false
    };

    var apnConnection = new apn.Provider(options);

    var deviceToken = iosDeviceToken;

    var note = new apn.Notification();
    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
    note.sound = "ping.aiff";
    note.payload = payload;
    apnConnection.send(note,deviceToken);


    function log(type) {
        return function () {
            if (DEBUG){
                console.log("payload.......",payload.message)

            }
        }
    }

    apnConnection.on('error', log('error'));
    apnConnection.on('transmitted', log('transmitted'));
    apnConnection.on('timeout', log('timeout'));
    apnConnection.on('connected', log('connected'));
    apnConnection.on('socketError', log('socketError'));
    apnConnection.on('cacheTooSmall', log('cacheTooSmall'));

    callback(null)

};



/*
 ==============================================
 Send the notification to the android device
 =============================================
 /* *!/
 function sendAndroidPushNotification(deviceToken, message) {

 console.log(message)

 var message = new gcm.Message({
 collapseKey: 'demo',
 delayWhileIdle: false,
 timeToLive: 2419200,
 data: {
 message: message,
 brand_name: config.androidPushSettings.brandName
 }
 });
 var sender = new gcm.Sender(config.androidPushSettings.gcmSender);
 var registrationIds = [];
 registrationIds.push(deviceToken);

 sender.send(message, registrationIds, 4, function (err, result) {
 if (debugging_enabled) {
 console.log("ANDROID NOTIFICATION RESULT: " + JSON.stringify(result));
 console.log("ANDROID NOTIFICATION ERROR: " + JSON.stringify(err));
 }
 });
 }*/

/*
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @ sendMailViaTransporter Function
 @ This function will initiate sending email as per the mailOptions are set
 @ Requires following parameters in mailOptions
 @ from:  // sender address
 @ to:  // list of receivers
 @ subject:  // Subject line
 @ html: html body
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 */


function sendMailViaTransporter(mailOptions, cb) {
    transporter.sendMail(mailOptions, function (error, info) {
        console.log('Mail Sent Callback Error:',error);
        console.log('Mail Sent Callback Ifo:',info);
    });
    cb(null, null) // Callback is outside as mail sending confirmation can get delayed by a lot of time
}

var sendAndroidPushNotification = function(deviceToken,data,title,callback){
    var fcm = new FCM(Config.APP_CONSTANTS.SERVER.FCM_KEY_FOR_CUSTOMER);
    //   console.log("data..........",data)
    var message = {
        to: deviceToken, // required
        collapse_key: 'demo',
        data:data,
        notification : {
            "title" : title
        }
    };
    fcm.send(message, function (err, result) {
        console.log("push android/ios..");
        callback(null);
    });
}


var sendAndroidPushNotificationForDriver = function(deviceToken,data,title,callback){
    console.log(' =================   NOTIFICATION DATA FOR DRIVER =============',data)
    var fcm = new FCM(Config.APP_CONSTANTS.SERVER.FCM_KEY_FOR_DRIVER);
    //  console.log("data.......",data)
    var message = {
        data:data,
          notification : {
         }
    };

    if(typeof deviceToken=='string'){
        message.to=deviceToken;
    }else{
        message.registration_ids=deviceToken;
    }
    console.log("sdffd ",message );
    fcm.send(message, function (err, result) {
        console.log("push android/ios..", err,result);
        callback(null);
    });
};



var sendAndroidPushNotificationForLoader = function(deviceToken,data,title,callback){
    console.log(' =================   NOTIFICATION DATA FOR LOADER =============',data)
    var fcm = new FCM(Config.APP_CONSTANTS.SERVER.FCM_KEY_FOR_CUSTOMER);
    //  console.log("data.......",data)
    var message = {
        data:data,
          notification : {
         }
    };

    if(typeof deviceToken=='string'){
        message.to=deviceToken;
    }else{
        message.registration_ids=deviceToken;
    }
    console.log("sdffd ",message );
    fcm.send(message, function (err, result) {
        console.log("push android/ios..", err,result);
        callback(null);
    });
};


module.exports = {
    sendSMSToUser: sendSMSToUser,
    sendEmailToUser: sendEmailToUser,
    sendPUSHToUser: sendPUSHToUser,
    sendIosPushNotification:sendIosPushNotification,
    sendAndroidPushNotification:sendAndroidPushNotification,
    sendAndroidPushNotificationForDriver:sendAndroidPushNotificationForDriver,
    sendAndroidPushNotificationForLoader:sendAndroidPushNotificationForLoader,
    sendSMSToLoader:sendSMSToLoader
};
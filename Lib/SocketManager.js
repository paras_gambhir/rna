'use strict';

var Config = require('../Config');
var TokenManager = require('./TokenManager');
var socketManager = require('./SocketManager');
var async = require('async');
var Service = require('../Services');

var socket = null;


exports.connectSocket = function (server) {
    if (!server.app) {
        server.app = {}
    }
    server.app.socketConnections = {};
    socket = require('socket.io').listen(server.listener);

    socket.on('disconnect', function () {
        console.log('socket disconnected')
    });

    socket.on('connection', function (socket) {

        console.log("socket connected");


        socket.emit('messageFromServer', {message: 'WELCOME TO WIZZRIDE', performAction: 'INFO'});


        socket.on('sendMessage',
            function (data) {
                console.log("..........................socket message.....................", data);
                if (data.receiver && data.sender && data.message && data.rideId) {
                    socketManager.saveMessage(data, function (err, result) {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log(".............result.........");
                        }
                    })
                } else {
                    console.log("data not in format");
                }
            });


        socket.on('updateDriverLocation', function (data) {
            console.log("data ",data);
            data = JSON.parse(data);
            if (data.driverId && data.location) {
                console.log("update location data", data);
                socketManager.updateDriverLocation(data, function (err, result) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(".............result.........");
                    }

                })
            }
            else {
                console.log("wrong data format")
            }

        });


        
        socket.on('rideStatusCustomer', function (data) {
            if (data.customerId) {
                getLastRideStatus(data, function (err, result) {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        console.log(result);
                        console.log("success");
                    }
                })
            }
            else {
                console.log("data not in format");
            }

        });


        socket.on('rideStatusDriver', function (data) {
            if (data.driverId) {
                getLastRideStatusDriver(data, function (err, result) {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        console.log(result);
                        console.log("success");
                    }
                })
            }
            else {
                console.log("data not in format");
            }

        });


        socket.on('getNearbyDrivers', function (data) {
            if (data.customerId && data.currentLocation && data.carType) {

                getNearbyDrivers(data, function (err, result) {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        console.log("success");
                    }

                })

            }
            else {
                console.log("data not in format");
            }

        });


    });
};


function getLastRideStatus(data, callback) {
    var criteria = {
        customerId: data.customerId
    };
    var projection = {};

    var option = {
        sort: {_id: -1},
        limit: 1
    };
    var populate = [
        {
            path: 'driverId',
            match: {},
            select: "firstName lastName vehicleName vehicleNumber averageRating",
            options: {}
        }
    ];

    Service.RideService.getRidePopulate(criteria, projection, option, populate, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            if (result.length) {
                console.log("result of ride ", result[0]);
                socket.emit("rideStatus_" + data.customerId, result[0]);
                callback()

            }
            else {
                socket.emit("rideStatus_" + data.customerId, {});
                callback();
            }
        }

    })

}


function getLastRideStatusDriver(data, callback) {
    var criteria = {
        driverId: data.driverId
    };
    var projection = {};

    var option = {
        sort: {_id: -1},
        limit: 1
    };
    var populate = [
        {
            path: 'customerId',
            match: {},
            select: "firstName lastName",
            options: {}
        }
    ];

    Service.RideService.getRidePopulate(criteria, projection, option, populate, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            if (result.length) {
                console.log("result of ride ", result[0]);
                socket.emit("rideStatus_" + data.driverId, result[0]);
                callback()

            }
            else {
                socket.emit("rideStatus_" + data.driverId, {});
                callback();
            }
        }

    })

}


function getNearbyDrivers(data, callback) {
    var criteria = {
        currentLocation: {
            $nearSphere: data.currentLocation,
            $maxDistance: UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.MAX_DISTANCE_RADIUS_TO_SEARCH
        },
        carType: data.carType,
        isAvailable: true
    };
    var projection = {
        _id: 1,
        currentLocation: 1,
        carType: 1
    };
    var options = {
        lean: true
    };


    Service.DriverService.getDriver(criteria, projection, options, function (err, result) {
        if (err) {
            console.log("err", err);
            callback(err);
        }
        else {
            console.log("data", result);
            socket.emit("sendNearbyDrivers", result);
            cb(null)
        }
    })

}


exports.saveMessage = function (data, callback) {

    var id;

    async.auto({
        saveMessage: function (cb) {

            var criteria = {
                _id: data.rideId
            };
            var setQuery = {
                $push: {
                    Chat: data
                }
            };

            Service.RideService.updateRide(criteria, setQuery, {new: true, upsert: true}, function (err, result) {
                if (err) {
                    console.log("helol=========err============", err);
                    cb(err);
                } else {
                    cb(null);
                }
            })
        },
        sendMessage: ['saveMessage', function (cb) {

            var socketEvent = "receiveMessage_" + data.receiver;
            socket.emit(socketEvent, data);
            console.log("===========dd=========", data);
            cb(null)
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, null)
        }
    })
};


exports.updateDriverLocation = function (data, callback) {

    var previousLocation;
    var distanceCalculated;
    async.auto({
        "updateDriver": function (cb) {
            var criteria = {
                _id: data.driverId
            };

            var setQuery = {
                currentLocation: data.location
            };

            var options = {
                new: true

            };
            Service.DriverService.updateDriver(criteria, setQuery, options, function (err, result) {
                if (err) {
                    console.log(err);
                    cb(err)
                }
                else {
                    cb();
                }
            })
        },
        getRide: ['updateDriver', function (cb) {
            if(data.rideId)
            {
                var criteria = {
                    _id: data.rideId
                };
                var projection = {
                    previousLocation: 1
                };
                var options = {
                    lean: true
                };
                Service.RideService.getRide(criteria, projection, options, function (err, result) {
                    if (err) {
                        cb(err);
                    }
                    else {
                        previousLocation = result[0].previousLocation;
                        cb(null)
                    }
                })
            }

        }],

        updateRide: ['getRide', function (cb) {
            if (data.rideId) {

                var params = {
                    origin: previousLocation.toString(),
                    destination: data.location.toString()
                };

                distance.get(params, function (err, data1) {
                    if (err) {
                        console.log(err);
                        cb(err);

                    }
                    else {
                        console.log(data1);
                        distanceCalculated = data1.distanceValue;
                        distanceCalculated = parseFloat(distanceCalculated/1000);
                        var criteria = {
                            _id: data.rideId
                        };

                        var setQuery = {
                            previousLocation: data.location,
                            $inc: {
                                totalDistance: distanceCalculated
                            }
                        };

                        var options = {
                            new: true

                        };
                        Service.RideService.updateRide(criteria, setQuery, options, function (err, result) {
                            if (err) {
                                console.log(err);
                                cb(err)
                            }
                            else {
                                cb();
                            }
                        })


                    }
                })

            }
            else {
                cb();
            }

        }],
        sendSocket: ['updateRide', function (cb) {
            if (data.rideId) {

                var socketEvent = "receiveLocation_" + data.rideId;
                socket.emit(socketEvent, data);
                cb();
            }
            else {
                cb();
            }

        }]
    }, function (err, result) {
        if (err) {
            console.log(err);
            callback(err)
        }
        else {
            callback();
        }

    })

};


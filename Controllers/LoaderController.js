/**
 * Created by paras
 */
'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');
var UploadManager = require('../Lib/UploadManager');
var TokenManager = require('../Lib/TokenManager');
var Config = require('../Config');
var NotificationManager = require('../Lib/NotificationManager');
var distance = require('google-distance');
var emailPassword = require('../Lib/email');
var appConstants = require('../Config/appConstants');
var mongoose = require('mongoose')




var registerLoader=function(payloadData,callback){
    async.auto({
        checkLoader:function (cb) {
            var criteria = {
                email:{ $regex: new RegExp("^" +payloadData.email+"$",'i')}
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.LoaderService.getLoader(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.LOADER_EMAIL_EXISTS);
                    }
                    else {

                        var criteria = {
                            countryCode: payloadData.countryCode,
                            phoneNumber: payloadData.phoneNumber
                        };
                        var projection = {};
                        var option = {
                            lean: true
                        };
                        Service.LoaderService.getLoader(criteria, projection, option, function (err, result) {

                            if(err){
                                cb(err)
                            }
                            else {
                                if (result.length) {
                                    callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.LOADER_PHONE_NO_EXISTS);
                                }
                                else {
                                    cb();
                                }
                            }

                        })

                    }
                }
            })

        },
        insertDriver:['checkLoader',function(cb)
        {

            var dataToBeInserted = {
                name: payloadData.name,
                dob: payloadData.dob,
                countryCode : payloadData.countryCode,
                address:payloadData.address,
                city:payloadData.city,
                state:payloadData.state,
                pincode:payloadData.pincode,
                phoneNumber: payloadData.phoneNumber,
                email:payloadData.email,
                password: UniversalFunctions.CryptData(payloadData.password)
            };
            Service.LoaderService.createLoader(dataToBeInserted, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb(null);
                }
            });


        }]

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null,{});
        }

    })
}


var editLoader=function(payloadData,callback){
    async.auto({
        checkLoaderId:function(cb){
             var criteria = {
                _id: payloadData.id,
                 isDeleted:false
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.LoaderService.getLoader(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_ID)
                    }
                }
            })

        },
        checkLoader:['checkLoaderId',function (cb) {
            var criteria = {
                email:{ $regex: new RegExp("^" + payloadData.email+"$", "i") },
                _id:{$ne:payloadData.id},
                 isDeleted:false
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.LoaderService.getLoader(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.LOADER_EMAIL_EXISTS);
                    }
                    else {

                        var criteria = {
                            countryCode: payloadData.countryCode,
                            phoneNumber: payloadData.phoneNumber,
                             _id:{$ne:payloadData.id},
                              isDeleted:false
                        };
                        var projection = {};
                        var option = {
                            lean: true
                        };
                        Service.LoaderService.getLoader(criteria, projection, option, function (err, result) {

                            if(err){
                                cb(err)
                            }
                            else {
                                if (result.length) {
                                    callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.LOADER_PHONE_NO_EXISTS);
                                }
                                else {
                                    cb();
                                }
                            }

                        })

                    }
                }
            })

        }],
        updateLoader:['checkLoader',function(cb)
        {

            var criteria={
                _id:payloadData.id
            }

            var dataToBeInserted = {
                name: payloadData.name,
                dob: payloadData.dob,
                countryCode : payloadData.countryCode,
                address:payloadData.address,
                city:payloadData.city,
                state:payloadData.state,
                pincode:payloadData.pincode,
                phoneNumber: payloadData.phoneNumber,
                email:payloadData.email
            };


            var setQuery={
                $set:dataToBeInserted
            }

            var options={
                new:true
            }



            Service.LoaderService.updateLoader(criteria,setQuery,options, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    if(Object.keys(dataAry).length){
                         cb(null);
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                   
                }
            });


        }]

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null,{});
        }

    })
}




var getLoaders=function(queryData,callback){
   

    var criteria={
         isDeleted:false
    }

    if(queryData.id){
        criteria._id=queryData.id
    }

    var projection={
        name:1,
        countryCode:1,
        phoneNumber:1,
        dob:1,
        email:1,
        address:1,
        city:1,
        state:1,
        pincode:1,
        assignedLocations:1
    
    }

    var options={
        lean:true,
        skip:queryData.skip,
        limit:queryData.limit   
    }

     var populate = [
                 {
                    path: 'assignedLocations',
                    model:'Location',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1,
                        isBlocked:1,
                        cityId:1,
                        companyId:1
                    },
                    options: {}
                }
                ]  

    
    Service.LoaderService.getLoaderPopulateAdmin(criteria,projection,options,populate,function(err,result){
        callback(err,result)
    })
}


var enterTruckNumber = function (payloadData, callback) {
    var truckId;
    var data;
    var loadingLocations = [];
    var bookingId;
    async.auto({

        checkTruckNoExists: function (cb) {
            var criteria = {
                truckNo: payloadData.truckNumber
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.VendorTruckService.getVendorTruck(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        truckId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TRUCK_NO);
                    }
                }
            })

        },
        getBooking: ['checkTruckNoExists', function (cb) {
            var criteria = {
                truckId: truckId
            };
            var projection = {
                loadingLocations: 1,
                _id: 1
            };
            var option = {
                lean: true
            };

            Service.BookingService.getBooking(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        bookingId = result[0]._id;
                        data = result[0].loadingLocations;
                        data.forEach(function (location) {

                            loadingLocations.push({
                                _id: location._id,
                                address: location.locationAddress
                            })
                        });
                        cb();
                    } else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.INVALID_TRUCK_NO)
                    }
                }
            })

        }]

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {"loadingLocations": loadingLocations, "bookingId": bookingId});
        }

    })


};


var selectLoadingLocation = function (payloadData, callback) {
    var materialsNames = [];


    async.auto({

        getMaterials: function (cb) {
            var criteria = {
                _id: payloadData.bookingId
            };

            var projection = {
                loadingLocations: {$elemMatch: {_id: payloadData.loadingLocationId}}
            };
            var option = {
                lean: true
            };
            var populate = [

                {
                    path: 'loadingLocations.materials.materialId',
                    match: {},
                    select: "materialName",
                    options: {}
                }
            ];

            Service.BookingService.getBookingsPopulate(criteria, projection, option, populate, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {

                        console.log("result ", result[0]);
                        result[0].loadingLocations[0].materials.forEach(function (materials) {
                            console.log("beta material aa jaa", materials);
                            materialsNames.push({
                                "materialId": materials.materialId._id,
                                "materialName": materials.materialId.materialName
                            });

                        });

                        cb();

                    } else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.INVALID_BOOKING_ID_OR_LOCATION)
                    }
                }
            })

        }

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {"materialNames": materialsNames});
        }

    })

};



var fillForm = function (payloadData, callback) {


    var OTP = UniversalFunctions.generateOTP();
    var materials=payloadData.materials.map(function(obj){
        return obj.materialId;
    })

    let bookingId;
    let startLocation;
    let endLocation;
    let message='';

    async.auto({

        updateMaterials: function (cb) {


            var criteria = {
                pickupLocationId:  payloadData.loadingLocationId
            };

            if(payloadData.length==12){
                criteria._id=payloadData.bookingId
            }else{
                criteria.bookingId=payloadData.bookingId
            }

            var setQuery = {
                dropOffLocationId:payloadData.dropOffLocationId,
                loadingInfo:payloadData.materials,
                materials:materials,
                otpVerified:false,
                OTP:OTP
            };

            var options = {
                new: true
            };

            Service.BookingService.updateBookings(criteria, setQuery, options, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    bookingId=result.bookingId;
                    startLocation=result.pickupAddress;
                    endLocation=result.dropOffAddress;
                    cb();
                }
            })
        },
        getDriver:['updateMaterials',function(cb)
        {
            var criteria = {
            };

             if(payloadData.length==12){
                criteria._id=payloadData.bookingId
            }else{
                criteria.bookingId=payloadData.bookingId
            }


            var projection = {
                driverId:1,
                loadingInfo:1
            };

            var options = {
                new: true
            };
            var populate = [

                {
                    path: 'driverId',
                    match: {},
                    select: "countryCode phoneNumber",
                    options: {}
                },
                {
                    path: 'loadingInfo.materialId',
                    match: {},
                    select: {},
                    options: {}
                }
            ];                               

            Service.BookingService.getBookingsPopulate(criteria, projection, options, populate,function (err, result) {
                if (err) {
                    cb(err);
                } else {

                    let materials=' ';
                    for(let i=0;i<result[0].loadingInfo.length;i++){
                        materials+=result[0].loadingInfo[i].materialId.materialName +'('+result[0].loadingInfo[i].materialQuantity +' ), '; 
                    }

                    materials=materials.replace(materials.lastIndexOf(','),'');

                    message='Booking id '+bookingId+' from '+startLocation+' to '+endLocation+' is updated with following details : '+materials +' . Your OTP is '+OTP;
                     console.log('rrrrrrrrrrrrrrrrr console =========== ',message)
                    cb()
                }
            })

        }],
          getAuthPerNo:['getDriver',function(cb){
            var criteria={
                _id:payloadData.loadingLocationId
            }

            var projection={
                phoneNumber:1
            }

            var options={
                lean:true
            }
            Service.LocationService.getLocation(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                   
                    let authPerNo;
                    if(result.length){
                         authPerNo=result && result.length && result[0].phoneNumber;
                          NotificationManager.sendSMSToLoader(message, null,authPerNo,function(err,result)
                            {
                                cb();

                            });
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {"OTP":OTP});
        }

    })

};



var confirmOTP = function(payloadData,callback)
{

    let bookingId;

    async.auto({

        getBooking:function(cb)
        {
            var criteria = {
                _id: payloadData.bookingId,
                OTP:payloadData.OTP,
                otpVerified:false

            };

            var projection = {
                pickupLocationId: payloadData.loadingLocationId,
                OTP:1,
                bookingId:1
            };

            var options = {
                lean: true
            };

            Service.BookingService.getBooking(criteria, projection, options,function (err, result) {
                if (err) {
                    cb(err);
                } else {

                    if(result.length && result[0].OTP && result[0].OTP == payloadData.OTP )
                    {
                        bookingId=result[0].bookingId;
                        cb();
                    }
                    else
                    {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_OTP);
                    }

                }
            })

        },
        getDriver:['getBooking',function(cb)
        {
            var criteria = {
                _id: payloadData.bookingId
            };

            var projection = {
                driverId:1
            };

            var options = {
                new: true
            };
            var populate = [

                {
                    path: 'driverId',
                    match: {},
                    select: "countryCode phoneNumber deviceToken",
                    options: {}
                }
            ];

            Service.BookingService.getBookingsPopulate(criteria, projection, options, populate,function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if(result[0].driverId.deviceToken){
                        var temp = {};
                        temp.title = "Vendor has filled the form. Please continue";
                        temp.bookingId=bookingId;
                        temp.pushType = 1;
                        var title =  temp.title;
                        sendPushNotification('ANDROID',result[0].driverId.deviceToken, temp, title, function (err) {
                            if (err) {
                                cb(null)
                            } else {
                                cb(null)
                            }
                        })
                    }else{
                        cb()
                    }
                    

                }
            })

        }],
        updateBooking:['getBooking',function(cb)
        {
            var criteria = {
                _id: payloadData.bookingId,
                pickupLocationId: payloadData.loadingLocationId
            };

            var setQuery = {
                otpVerified:true,
            loadLocationLeftTime:parseInt(+new Date()+19800000)
            };

            var options = {
                new: true
            };

            Service.BookingService.updateBookings(criteria, setQuery, options, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    cb();
                }
            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {});
        }

    })


};



var sendPushNotification = function (deviceType, deviceToken, data, title, cb) {
    if (deviceType == "IOS" && deviceToken.length) {
        var temp = {};
        temp = data;

        NotificationManager.sendAndroidPushNotificationForDriver(deviceToken, temp, title, function (err, result) {
            if (err) {
                cb(err)
            } else {
                cb(null)
            }
        })
    } else if (deviceType == "ANDROID") {
        var temp = {};
        temp = data;

        NotificationManager.sendAndroidPushNotificationForDriver(deviceToken, temp, title, function (err, result) {
            if (err) {
                cb(err)
            } else {
                console.log(err);
                console.log(result);
                cb(null)
            }
        })
    }
};





var loaderLogin = function (payloadData, callback) {

    var loaderData;
    var loaderId;
    var accessToken;
    let responseObject={};
    async.auto({

        checkForUserExists: function (cb) {

               var criteria = {
                    countryCode: payloadData.countryCode,
                    phoneNumber: payloadData.phoneNumber
                };


            var projection = {};
            var option = {
                lean: true
            };
            Service.LoaderService.getLoader(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        var cryptedPassword = UniversalFunctions.CryptData(payloadData.password);
                        if (cryptedPassword == result[0].password) {
                           loaderId = result[0]._id;
                           loaderData = result[0];
                           cb();
                        }
                        else {
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_PASSWORD);
                        }

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACCOUNT_NOT_REGISTERED);
                    }
                }
            })

        },
        updateLoaderParams: ['checkForUserExists', function (cb) {
            var criteria = {
                _id: loaderId
            };
            var setQuery = {
                deviceType: payloadData.deviceType ? payloadData.deviceType : loaderData.deviceType,
                deviceToken: payloadData.deviceToken ? payloadData.deviceToken : loaderData.deviceToken
            };
            Service.LoaderService.updateLoader(criteria, setQuery, {
                new: true,
                upsert: true
            }, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    loaderData = dataAry;
                    cb();
                }
            });

        }],
        setToken: ['updateLoaderParams', function (cb) {
            var tokenData = {
                id: loaderId,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.LOADER
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        loaderData.accessToken = accessToken;
                        responseObject={
                            _id:loaderData._id,
                            phoneNumber:loaderData.phoneNumber,
                            accessToken:loaderData.accessToken,
                            profilePicURL:loaderData.profilePicURL,
                            email:loaderData.email,
                            address:loaderData.address,
                            city:loaderData.city,
                            state:loaderData.state,
                            pincode:loaderData.pincode,
                            countryCode:loaderData.countryCode,
                            dob:loaderData.dob,
                            name:loaderData.name
                        }
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })

        }]

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {loaderData: responseObject});
        }

    })
};


var sendOTP = function(payloadData,callback)
{
    var OTP;
    var loaderId;
    async.auto({

        checkForUserExists: function (cb) {

            var criteria = {
                countryCode: payloadData.countryCode,
                phoneNumber: payloadData.phoneNumber
            };


            var projection = {};
            var option = {
                lean: true
            };
            Service.LoaderService.getLoader(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {

                     OTP = UniversalFunctions.generateOTP();
                    if (result.length) {
                        loaderId =  result[0]._id;
                        NotificationManager.sendSMSToUser(OTP, payloadData.countryCode, payloadData.phoneNumber,function(err,result)
                        {
                            cb();
                        })
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACCOUNT_NOT_REGISTERED);
                    }
                }
            })

        },
        saveOTP:['checkForUserExists',function (cb) {
            var dataToSet = {
                OTP: OTP
            };

            var criteria = {
                _id: loaderId
            };
            Service.LoaderService.updateLoader(criteria, dataToSet,function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    cb();
                }
            })
        }]

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {OTP: OTP});
        }

    })
};




var OTPLogin = function(payloadData,callback)
{
    var loaderData;
    var loaderId;
    var accessToken;
    let responseObject={};
    async.auto({

        checkForUserExists: function (cb) {

            var criteria = {
                countryCode: payloadData.countryCode,
                phoneNumber: payloadData.phoneNumber
            };


            var projection = {};
            var option = {
                lean: true
            };
            Service.LoaderService.getLoader(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        loaderData=result[0];
                        if (payloadData.OTP == result[0].OTP) {
                            loaderId=result[0]._id;
                            cb(null);
                        }
                        else {
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_OTP);
                        }

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACCOUNT_NOT_REGISTERED);
                    }
                }
            })

        },
        updateLoaderParams: ['checkForUserExists', function (cb) {
            var criteria = {
                _id:loaderId
            };
            var setQuery = {
                deviceType: payloadData.deviceType ? payloadData.deviceType : loaderData.deviceType,
                deviceToken: payloadData.deviceToken ? payloadData.deviceToken : loaderData.deviceToken

            };
            Service.LoaderService.updateLoader(criteria, setQuery, {
                new: true,
                upsert: true
            }, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    loaderData = dataAry;
                    cb();
                }
            });

        }],
        setToken: ['updateLoaderParams', function (cb) {
            var tokenData = {
                id: loaderId,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.LOADER
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        loaderData.accessToken = accessToken;
                         responseObject={
                            _id:loaderData._id,
                            phoneNumber:loaderData.phoneNumber,
                            accessToken:loaderData.accessToken,
                            profilePicURL:loaderData.profilePicURL,
                            email:loaderData.email,
                            address:loaderData.address,
                            city:loaderData.city,
                            state:loaderData.state,
                            pincode:loaderData.pincode,
                            countryCode:loaderData.countryCode,
                            dob:loaderData.dob,
                            name:loaderData.name
                        }
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })

        }]

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {loaderData: responseObject});
        }

    })
};



const bookingListing = function (payloadData,callback) {
    console.log('========================',payloadData)
   let responseObject={};
   let criteria={
       $and:[
           {$or:[
               {$and:[
                   {pickupLocationId:{$in:payloadData.assignedLocations}},
                   {otpVerified:false}
               ]}
       ]},
           {$or:[
           {status:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.ACCEPTED},
           {status:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.LOADING_LOCATION},
           {status:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.STARTED},
           {status:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.TOWARDS_UNLOADING_LOCATION},
           {status:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.UNLOADING_LOCATION}]}
           
       ]
       
       
       
   }

   payloadData.bookingId && (criteria.bookingId=payloadData.bookingId)

    async.auto({
        getBookingsCount:function(cb){
            Service.BookingService.getBookingCount(criteria,function(err,result){
                if(err){
                    cb(err)
                }else{
                    responseObject.count=result;
                    console.log('========================',result)
                    cb()
                }
            })
        },
        getBookings:function(cb){
           
                var options = {
                    lean:true
                };

                payloadData.skip && (options.skip=payloadData.skip)

                payloadData.limit && (options.limit=payloadData.limit)

                var projections = {
                    status:1,
                    contractId:1,
                    pickupLocationId:1,
                    materials:1,
                    dropOffLocationId:1,
                    truckId:1,
                    bookingId:1,
                    driverId:1,
                    companyId:1,
                    bookingDate:1
                }
                 var populate = [
                {
                    path: 'driverId',
                    match: {},
                    select: {
                        name:1,
                        dob:1,
                        countryCode:1,
                        address:1,
                        pincode:1,
                        city:1,
                        state:1,
                        phoneNumber:1,
                        email:1,
                        currentLocation:1,
                        profilePicURL:1,
                        truckId:1,
                        vendorId:1,
                        VendorTruck:1,
                        currentBooking:1,
                        truckAssign:1

                    },
                    options: {}
                },
                 {
                    path: 'contractId',
                    match: {
                        
                    },
                    select: {
                        truckType:1,
                        freightAmount:1,
                        detentionCharges:1,
                        billingPeriod:1,
                        startCityId:1,
                        endCityId:1,
                        truckId:1
                    },
                    options: {}
                },
                 {
                    path: 'companyId',
                    match: {},
                    select: {
                        name:1
                    },
                    options: {
                        lean:true
                    }
                },
                 {
                    path: 'materials',
                     model:'Materials',
                    match: {},
                    select: {
                        materialName:1,
                        length:1,
                        breadth:1,
                        height:1,
                         accessionNo:1,
                        capacity:1
                    },
                    options: {}
                },
                 {
                    path: 'pickupLocationId',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1,
                        authPerson: 1,
                        phoneNumber: 1
                    },
                    options: {
                        lean:true
                    }
                },
                 {
                    path: 'dropOffLocationId',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1,
                        authPerson: 1,
                        phoneNumber: 1,
                    },
                    options: {
                        lean:true
                    }
                }
                ]  
                Service.BookingService.getBookingsPopulate(criteria,projections,options,populate,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        responseObject.results = result;
                        cb(null);

                    }
                })

        }
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,responseObject)
        }
    })
}


var updateBooking=function(payloadData,callback){
    
    async.auto({
        checkBookingId:function(cb){
            var criteria={

            }
        }
    },function(err){

    })
}



var listMaterial = function(payloadData,callback)
{

    var materials;
    var companyId;
    async.auto({

        checkBookingId: function (cb) {
            var criteria = {
                bookingId: payloadData.bookingId,
                status:{$ne:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.PENDING}
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.BookingService.getBooking(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        companyId = result[0].companyId;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_BOOKING_ID);
                    }
                }
            })

        },
        listMaterial:['checkBookingId', function (cb) {
            var criteria = {
                CompanyId: companyId
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.MaterialsService.getMaterial(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    materials = result;
                    cb();
                }
            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {materials:materials});
        }
    })
};





var getLocations=function(queryData,callback){

    let count=0;
    let contractData;
    let responseObject={};

    async.auto({
        getContractData:function(cb){
           var criteria={
                    _id:queryData.contractId,
                    isDeleted:false,
                    isBlocked:false
                }

                var projection={}
                var options={
                    lean:true
                }

                Service.ContractsService.getContract(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result.length){
                            contractData=result[0];
                            cb()
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CONTRACT_ID)
                        }
                    }
                })
        },
        getResults:['getContractData',function(cb){
            let aggregateArray=[
                {
                    $match:{
                        $or:[
                            {cityId:mongoose.Types.ObjectId(contractData.startCityId)},
                            {cityId:mongoose.Types.ObjectId(contractData.endCityId)}
                        ],
                        companyId:mongoose.Types.ObjectId(contractData.companyId)
                    }
                },
                {
                    $group:{
                        _id:null,
                        pickUpLocations:{$push:{ $cond: { 
                            if: {$eq:['$cityId',mongoose.Types.ObjectId(contractData.startCityId)]},
                            then:{
                                _id:'$_id',
                                pointName:'$pointName',
                                location:'$location',
                                address:'$address',
                                authPerson:'$authPerson',
                                phoneNumber:'$phoneNumber'
                            },
                            else: null} }},
                            dropLocations:{$push:{ $cond: { 
                            if: {$eq:['$cityId',mongoose.Types.ObjectId(contractData.endCityId)]},
                            then:{
                                _id:'$_id',
                                pointName:'$pointName',
                                location:'$location',
                                address:'$address',
                                authPerson:'$authPerson',
                                phoneNumber:'$phoneNumber'
                            },
                            else: null} }}
                    }
                },
                {
                    $project:{
                        pickUpLocations:{ 
                            $filter: { 
                                input: '$pickUpLocations', 
                                as: 'p',
                                cond: {$ne:['$$p',null]} } },
                        dropLocations:{ 
                            $filter: { 
                                input: '$dropLocations', 
                                as: 'p',
                                cond: {$ne:['$$p',null]} } },
                        _id:0
                    }
                }
            ]
            Service.LocationService.getLocationAggregated(aggregateArray,function(err,result){
                if(err){
                    cb(err)
                }else{
                        if(result.length){
                    responseObject=result[0]
                }else{
                    responseObject={
                        pickUpLocations:[],
                        dropLocations:[]
                    }
                }

                cb()
                }
                
            })
        }]
    },function(err,result){
        callback(err,responseObject)
    })

}


var updateBookingDetails=function(payloadData,callback){
    let bookingData;
    let contractData;
    let deviceToken;
    let allowNot=true;
    let vendorId;

    async.auto({
        checkBookingId:function(cb){
            var criteria={
                bookingId:payloadData.bookingId,
                $or:[
                    {status:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.STARTED},
                    {status:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.LOADING_LOCATION}
                ]
            }

            var projection={}
            var option={
                lean:true
            }

            Service.BookingService.getBooking(criteria,projection,option,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        bookingData=result[0];
                        cb()
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_BOOKING_ID)
                    }
                }
            })
        },
        getContractDetails:['checkBookingId',function(cb){
             if(payloadData.dropOffLocationId){
                    var criteria={
                    _id:bookingData.contractId,
                    companyId:bookingData.companyId
                }

                var projection={}
                var option={
                    lean:true
                }

                Service.ContractsService.getContract(criteria,projection,option,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result.length){
                            contractData=result[0];
                            cb()
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }
                    }
                })
             }else{
                 cb()
             }
        }],
        checkDropOffLocationId:['getContractDetails',function(cb){
            if(payloadData.dropOffLocationId){
                    var criteria={
                    _id:payloadData.dropOffLocationId,
                    companyId:bookingData.companyId,
                    cityId:contractData.endCityId
                }

                var projection={}
                var option={
                    lean:true
                }

                Service.LocationService.getLocation(criteria,projection,option,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result.length){
                            cb()
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_DROP_LOCATION)
                        }
                    }
                })
            }else{
                cb()
            }
        }],
        updateBooking:['checkDropOffLocationId',function(cb){
            var criteria={
                companyId:contractData.companyId,
                bookingId:payloadData.bookingId,
                $or:[
                    {status:{$eq:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.STARTED}},
                    {status:{$eq:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.LOADING_LOCATION}}
                ]
            }


            var setQuery={
                $set:{
                    // authPersonName:payloadData.authPersonName,
                    // city:payloadData.city,
                    // state:payloadData.state,
                    // countryCode:payloadData.countryCode || undefined,
                    // phoneNumber:payloadData.phoneNumber,
                    // pincode:payloadData.pincode || undefined,
                    dropOffLocationId:payloadData.dropOffLocationId
                }
            }

            if(payloadData.materialIds&& payloadData.materialIds.length){
                setQuery.$addToSet={
                    materials:{$each:payloadData.materialIds}
                }
            }


            var options={
                lean:true,
                new:true

            }


            Service.BookingService.updateBookings(criteria,setQuery,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result && Object.keys(result).length){
                        cb()
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
    }]
    // getDriverDetails:['updateBooking',function(cb){
    //     var criteria={
    //         _id:bookingData.driverId
    //     }

    //     var projection={
    //         deviceToken:1,
    //         vendorId:1
    //     }

    //     var options={
    //         lean:true
    //     }


    //     Service.DriverService.getDriver(criteria,projection,options,function(err,result){
    //         if(err){
    //             cb(err)
    //         }else{
    //             if(result.length){
    //                 deviceToken=result[0].deviceToken || null
    //                 vendorId=result[0].vendorId || null
    //                 cb()
    //             }else{
    //                 cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    //             }
    //         }
    //     })
    // }],
    //   getVendorDetails:['getDriverDetails',function(cb){
    //     var criteria={
    //         _id:vendorId
    //     }

    //     var projection={
    //         pushNotifications:1
    //     }

    //     var options={
    //         lean:true
    //     }


    //     Service.VendorService.getVendor(criteria,projection,options,function(err,result){
    //         if(err){
    //             cb(err)
    //         }else{
    //             if(result.length){
    //                 allowNot=result[0].pushNotifications || true
    //                 cb()
    //             }else{
    //                 cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    //             }
    //         }
    //     })
    // }],
    // sendNotification:['getVendorDetails',function(cb){
    //     if(deviceToken && allowNot){
    //         console.log('=========================== UPDATE BOKKING DATA ====================',payloadData,'  =====  BOOKING ID ==== ',payloadData.bookingId)
    //             var temp = {};
    //             temp.pushType=1;
    //             temp.bookingId = payloadData.bookingId;
    //             temp.title = "Your booking "+payloadData.bookingId+" is updated by loader "+payloadData.name;
    //             var title =  'BOOKING UPDATED';
    //             sendPushNotification('ANDROID', deviceToken, temp, title, function (err) {
    //                 if (err) {
    //                     console.log('ERRRRRRRRRRRRRRR',err);
    //                     cb(null)
    //                 } else {
    //                     cb(null)
    //                 }
    //             })
    //         }else{
    //             cb()
    //         }
    // }]
},function(err){
        callback(err,{});
    });
    
}



var editProfilePic=function(payloadData,callback){

    let imageData={};

    async.auto({
        uploadImage:function(cb){
            UploadManager.uploadFile(payloadData.image && payloadData.image.filename ? payloadData.image :payloadData.image[1], Math.random()*1000, "hello", function (err, uploadedInfo) {
                if (err) {
                    callback(err)
                } else {
                    console.log("uploaded info", uploadedInfo);
                    imageData.original = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                    imageData.thumbnail = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.thumbnail
                    cb();
                }
            })
        },
        updateLoaderInDb:['uploadImage',function(cb){

            if(imageData && imageData.original){
                var criteria={
                    _id:payloadData.id,
                    isDeleted:false
                }

                var setQuery={
                    $set:{
                        profilePicURL:imageData
                    }
                    
                }

                var options={
                    new:true
                }

                Service.LoaderService.updateLoader(criteria,setQuery,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            cb()
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }
                    }
                })


            }else{
                cb('Image is required.')
            }

        }]
    },function(err,result){
        callback(err,imageData);
    })

   
}


module.exports = {
    enterTruckNumber: enterTruckNumber,
    selectLoadingLocation: selectLoadingLocation,
    fillForm: fillForm,
    confirmOTP:confirmOTP,
    registerLoader:registerLoader,
    getLoaders:getLoaders,
    loaderLogin:loaderLogin,
    sendOTP:sendOTP,
    OTPLogin:OTPLogin,
    editLoader:editLoader,
    bookingListing:bookingListing,
    listMaterial:listMaterial,
    getLocations:getLocations,
    updateBookingDetails:updateBookingDetails,
    editProfilePic:editProfilePic
    
};
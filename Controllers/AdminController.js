'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');
var Config = require('../Config');

var UploadManager = require('../Lib/UploadManager');
var TokenManager = require('../Lib/TokenManager');
var NotificationManager = require('../Lib/NotificationManager');


var adminLogin = function (payloadData,callback) {

    var accessToken;
    var adminId;
    async.auto({
        checkForCredentials:function(cb)
        {
            var criteria = {
                Email: payloadData.email,
              //  Password: UniversalFunctions.CryptData(payloadData.password)
            };
            var projection = {
            };
            var options = {
                lean:true
            };
            Service.AdminService.getAdmin(criteria,projection,options,function(err,result)
            {
                if(err){
                    cb(err);
                }
                else{
                    if(result.length)
                   {
                       adminId = result[0]._id;
                       cb();
                   }
                   else{
                       callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_USER_PASS);
                   }
                }

            })

        },
        setToken: ['checkForCredentials', function (cb) {
            var tokenData = {
                id: adminId,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })

        }]

    },function(err,result)
    {
        if(err){
            callback(err);
        }
        else{
            callback(null,{"accessToken": accessToken})
        }

    })

};


const addTruck  = function(payloadData,callback){
    async.auto({
        addNewTruck:function(cb){
            let obj = {
                truckName:payloadData.truckName,
                Capacity:payloadData.Capacity,
                length:payloadData.length,
                breadth:payloadData.breadth,
                height:payloadData.height
            }

            Service.TruckService.createTruck(obj,function(err,result){
                if(err){
                    if(err.code == 11000){
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }else{
                        cb(err)
                    }

                }else{
                    cb(null)
                }
            })
        }
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null)
        }
    })
}



const addAccount  = function(payloadData,callback){

    let receiverName;

    if(payloadData.type!='VENDOR' && payloadData.type!='LOADER' && !payloadData.bookingId){
        return callback('Booking id is required.')
    }

    if(payloadData.type=='VENDOR' && (!payloadData.startDate || !payloadData.endDate)){
        return callback('Start and End date is required in case of vendor.')
    }
    
    async.auto({
        checkBookingId:function(cb){
            if(payloadData.bookingId){
                var criteria={
                    bookingId:payloadData.bookingId
                }

                Service.BookingService.getBookingCount(criteria,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result){
                            cb()
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_BOOKING_ID)
                        }
                    }
                })
            }else{
                cb()
            }
            
        },
        checkReceiverId:function(cb){
            var criteria={
                _id:payloadData.receiverId,
                isDeleted:false
            }

            var projection={}
            var options={
                lean:true
            }

            if(payloadData.type=='DRIVER'){
                Service.DriverService.getDriver(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            receiverName=result[0].name;
                            cb()
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_RECEIVER_ID)
                        }
                    }
                })
            }else if(payloadData.type=='COMPANY'){
                 Service.CompanyService.getCompany(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            receiverName=result[0].name;
                            cb()
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_RECEIVER_ID)
                        }
                    }
                })
            }else if(payloadData.type=='VENDOR'){
                 Service.VendorService.getVendor(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            receiverName=result[0].name;
                            cb()
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_RECEIVER_ID)
                        }
                    }
                })
            }else if(payloadData.type=='LOADER'){
                 Service.LoaderService.getLoader(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            receiverName=result[0].name;
                            cb()
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_RECEIVER_ID)
                        }
                    }
                })
            }else{
                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        },
        // checkDuplicasy:['checkReceiverId','checkBookingId',function(cb){
        //     var criteria={
        //         receiverId:payloadData.receiverId,
        //         isDeleted:false,
        //         type:payloadData.type,
        //         isClear:false
        //     }

        //     payloadData.bookingId && (criteria.bookingId=payloadData.bookingId)

        //     payloadData.invoiceNo && (criteria.invoiceNo=payloadData.invoiceNo)

        //     Service.AccountService.getAccountCount(criteria,function(err,result){
        //         if(err){
        //             cb(err)
        //         }else{
        //             if(result){
        //                 cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE)
        //             }else{
        //                 cb()
        //             }
        //         }
        //     })
        // }],
        addNewAccount:['checkBookingId','checkBookingId',function(cb){
            let obj = {
                receiverId:payloadData.receiverId,
                type:payloadData.type,
                bookingId:payloadData.bookingId || undefined,
                invoiceNo:payloadData.invoiceNo || undefined,
                date:payloadData.date,
                startDate:payloadData.startDate || undefined,
                endDate:payloadData.endDate || undefined,
                amount:payloadData.amount,
                receiverName:receiverName,
                comment:payloadData.comment || ''
            }




            Service.AccountService.createAccount(obj,function(err,result){
                if(err){
                    if(err.code == 11000){
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }else{
                        cb(err)
                    }

                }else{
                    cb(null)
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,{})
        }
    })
}



const editAccount  = function(payloadData,callback){

    let receiverName;
    if(payloadData.type!='VENDOR' && payloadData.type!='LOADER' && !payloadData.bookingId){
        return callback('Booking id is required.')
    }

     if(payloadData.type=='VENDOR' && (!payloadData.startDate || !payloadData.endDate)){
        return callback('Start and End date is required in case of vendor.')
    }

    async.auto({
         checkAccountId:function(cb){
            var criteria={
                _id:payloadData.id,
                isDeleted:false
            }

            var projection={}

            var options={
                lean:true
            }

            Service.AccountService.getAccount(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        if(result[0].isClear){
                            cb('Sorry this account entry is already marked cleared by you, so you can not edit it.')
                        }else{
                            cb()
                        }
                        
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_ID)
                    }
                }
            })
            
        },
        checkBookingId:['checkAccountId',function(cb){
            if(payloadData.bookingId){
                var criteria={
                    bookingId:payloadData.bookingId
                }

                Service.BookingService.getBookingCount(criteria,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result){
                            cb()
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_BOOKING_ID)
                        }
                    }
                })
            }else{
                cb()
            }
            
        }],
        checkReceiverId:['checkAccountId',function(cb){
            var criteria={
                _id:payloadData.receiverId,
                isDeleted:false
            }

            var projection={}
            var options={
                lean:true
            }

            if(payloadData.type=='DRIVER'){
                Service.DriverService.getDriver(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            receiverName=result[0].name;
                            cb()
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_RECEIVER_ID)
                        }
                    }
                })
            }else if(payloadData.type=='COMPANY'){
                 Service.CompanyService.getCompany(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            receiverName=result[0].name;
                            cb()
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_RECEIVER_ID)
                        }
                    }
                })
            }else if(payloadData.type=='VENDOR'){
                 Service.VendorService.getVendor(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            receiverName=result[0].name;
                            cb()
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_RECEIVER_ID)
                        }
                    }
                })
            }else if(payloadData.type=='LOADER'){
                 Service.LoaderService.getLoader(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            receiverName=result[0].name;
                            cb()
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_RECEIVER_ID)
                        }
                    }
                })
            }else{
                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }],
        // checkDuplicasy:['checkReceiverId','checkBookingId',function(cb){
        //     if(payloadData.type!='VENDOR'){
        //         var criteria={
        //             id:{$ne:payloadData.id},
        //             receiverId:payloadData.receiverId,
        //             isDeleted:false,
        //             type:payloadData.type,
        //             isClear:false
        //         }

        //         payloadData.bookingId && (criteria.bookingId=payloadData.bookingId)

        //         payloadData.invoiceNo && (criteria.invoiceNo=payloadData.invoiceNo)

        //         Service.AccountService.getAccountCount(criteria,function(err,result){
        //             if(err){
        //                 cb(err)
        //             }else{
        //                 if(result){
        //                     cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE)
        //                 }else{
        //                     cb()
        //                 }
        //             }
        //         })
        //     }else{
        //         cb()
        //     }
        // }],
        updateAccount:['checkReceiverId','checkBookingId',function(cb){
            let obj = {
                receiverId:payloadData.receiverId,
                type:payloadData.type,
                bookingId:payloadData.bookingId || undefined,
                invoiceNo:payloadData.invoiceNo || undefined,
                date:payloadData.date,
                startDate:payloadData.startDate || undefined,
                endDate:payloadData.endDate|| undefined,
                amount:payloadData.amount,
                receiverName:receiverName,
                comment:payloadData.comment || ''
            }

            var criteria={
                _id:payloadData.id
            }


            var setQuery={
                $set:obj
            }

            var options={
                new:true
            }


            Service.AccountService.updateAccount(criteria,setQuery,options,function(err,result){
                if(err){
                    if(err.code == 11000){
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }else{
                        cb(err)
                    }

                }else{
                    if(result && Object.keys(result).length){
                        cb()
                    }else{
                         cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                   
                }
            })
        }],
        uploadInBookingDb:['updateAccount',function(cb){
            if(payloadData.type=='COMPANY' && payloadData.type=='DRIVER' && payloadData.bookingId){
                let criteria={
                    bookingId:parseInt(payloadData.bookingId)
                }

                let setQuery={
                    $set:{}
                }

                if(payloadData.type=='COMPANY'){
                    setQuery.$set.companyAmount=payloadData.amount;
                    setQuery.$set.companyDate=date;
                }else{
                    setQuery.$set.driverAmount=payloadData.amount;
                     setQuery.$set.driverDate=date;
                }

                var options={
                    new:true
                }

                Service.BookingService.updateBookings(criteria,setQuery,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            cb()
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }
                    }
                })

            }else{
                cb()
            }
        }]
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,{})
        }
    })
}



const clearAccount  = function(payloadData,callback){

  
    async.auto({
         checkAccountId:function(cb){
            var criteria={
                _id:payloadData.id,
                isDeleted:false
            }

            var projection={}

            var options={
                lean:true
            }

            Service.AccountService.getAccount(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        if(result[0].isClear){
                            cb('Sorry this account entry is already marked cleared by you.')
                        }else{
                            cb()
                        }
                        
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_ID)
                    }
                }
            })
            
        },
        updateAccount:['checkAccountId',function(cb){

            let obj = {
                isClear:true
            }

            var criteria={
                _id:payloadData.id
            }


            var setQuery={
                $set:obj
            }

            var options={
                new:true
            }


            Service.AccountService.updateAccount(criteria,setQuery,options,function(err,result){
                if(err){
                    if(err.code == 11000){
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }else{
                        cb(err)
                    }

                }else{
                    if(result && Object.keys(result).length){
                        cb()
                    }else{
                         cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                   
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,{})
        }
    })
}


const getAccounts  = function(queryData,callback){
    let responseObject={};

    let criteria={
        isDeleted:false
    }

    let model='';

    if(queryData.options=='DRIVER'){
        model='Driver'
    }else if(queryData.options=='COMPANY'){
        model='Company'
    }else if(queryData.options=='VENDOR'){
        model='Vendor'
    }else{
        model='Loader'
    }

    criteria.bookingId && (criteria.bookingId=queryData.bookingId)

    if(queryData.type==Config.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN){
        queryData.options && (criteria.type=queryData.options)
        queryData.receiverId  && (criteria.receiverId=queryData.receiverId)
        
    }else{
        if(!queryData.options || queryData.options!=queryData.type){
            return callback('Invalid selection you are not authorized for this.')
        }else{
            criteria.type=queryData.options;
            criteria.receiverId=queryData.id;
        }
    }

     if('cleared' in queryData){
        criteria.isClear=queryData.cleared;
    }

    console.log('DDDDDDDDDDDDDDDDDd',criteria)

    async.auto({
        getCounts:function(cb){
            Service.AccountService.getAccountCount(criteria,function(err,result){
                if(err){
                    cb(err)
                }else{
                    responseObject.count=result;
                    cb();
                }
            })
        },
        getRecords:['getCounts',function(cb){

            var projection={
                isDeleted:0,
                receiverName:0
            }

            queryData.type!=Config.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN && (projection.isBlocked=0)

            var options={
                skip:queryData.skip,
                limit:queryData.limit,
                lean:true,
                sort:{
                    date:-1
                }
            }

            var populate=[
                {
                    path: 'receiverId',
                     model:model,
                    match: {},
                    select: {
                       name:1
                    },
                    options: {}
                },
            ]

            Service.AccountService.getAccountPopulate(criteria,projection,options,populate,function(err,result){
                if(err){
                    cb(err)
                }else{
                    responseObject.results=result;
                    cb();
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,responseObject)
        }
    })
}

const getMessages  = function(queryData,callback){
    let responseObject={};

    let criteria={
        isDeleted:false
    }
    queryData.id  && (criteria.driverId=queryData.id)

    async.auto({
        getCounts:function(cb){
            Service.DriverService.getMessagesCount(criteria,function(err,result){
                if(err){
                    cb(err)
                }else{
                    responseObject.count=result;
                    cb();
                }
            })
        },
        getRecords:['getCounts',function(cb){

            var projection={
                isDeleted:0
            }

            var options={
                skip:queryData.skip,
                limit:queryData.limit,
                lean:true
            }

            var populate=[
                {
                    path: 'driverId',
                    match: {},
                    select: {
                       name:1,
                       email:1,
                       phoneNumber:1
                    },
                    options: {}
                },
                {
                    path: 'vendorId',
                    match: {},
                    select: {
                       name:1,
                       email:1,
                       contactNo:1
                    },
                    options: {}
                },
            ]

            Service.DriverService.getMessagePopulate(criteria,projection,options,populate,function(err,result){
                if(err){
                    cb(err)
                }else{
                    responseObject.results=result;
                    cb();
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,responseObject)
        }
    })
}


const listTruck  = function(callback){
    let data = null;
    async.auto({
        getList:function(cb){
            let query = {};
            let options = {lean:true};
            let projection = {};
            Service.TruckService.getTruck(query,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result && result.length){
                        data = result;
                        cb(null)
                    }else{
                        data = [];
                        cb(null)
                    }
                }
            })
        }
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,data)
        }
    })
}


const listVendor = function(queryData,callback){
    let data;
    async.auto({
        getlistVendor:function(cb){
            let query = {
                approvedByAdmin:queryData.adminVerifiy,
                profileCompleted:true
            };
            let projection = {};
            Service.VendorService.getVendor(query,projection,{lean:true},function(err,result){
                if(err){
                    cb(err)
                }else{
                    data = result;
                    cb(null)
                }
            })
        }

    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,data)
        }
    })
}


const verifiyVendor = function(payloadData,callback){
    async.auto({

        checkVendorId:function(cb){
            var criteria={
                _id:payloadData._id
            }

            var projection={}
            var options={}

            Service.VendorService.getVendor(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        if(!result[0].emailVerify){
                             cb('Sorry email verification is pending of this vendor.')
                        }else{
                            cb();
                        }
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_ID)
                    }
                }
            })
        },
        updateVendor:['checkVendorId',function(cb){
            let query = {
                _id:payloadData._id
            }
            let options = {lean:true};
            let setData = {
                approvedByAdmin:true
            }
            Service.VendorService.updateVendor(query,setData,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    cb(null)
                }   
            })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null)
        }
    })
}



const verifyCompany = function(payloadData,callback){
    async.auto({

        checkCompanyId:function(cb){
            var criteria={
                _id:payloadData._id
            }

            var projection={}
            var options={}

            Service.CompanyService.getCompany(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        if(!result[0].emailVerify){
                             cb('Sorry email verification is pending of this company.')
                        }else{
                            cb();
                        }
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_ID)
                    }
                }
            })
        },
        updateCompany:['checkCompanyId',function(cb){
            let query = {
                _id:payloadData._id
            }
            let options = {lean:true};
            let setData = {
                approvedByAdmin:true
            }
            Service.CompanyService.updateCompany(query,setData,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    cb(null)
                }   
            })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null)
        }
    })
}


const listVendorTruck = function(queryData,callback){
    let data = [];
    async.auto({
        getVendorTruckList:function(cb){
            
            let query = {};
            if(queryData.vendorId){
                query.vendorId = queryData.vendorId
            }


            if(queryData.driverId){
                query.driverId = queryData.driverId
            }
            
            let options = {lean:true};
            let projections = {};
            let populate = [
                {
                    path: "truckId",
                    match: {},
                    select: '',
                    options: {lean: true}
                },
                {
                    path: "vendorId",
                    match: {},
                    select: 'name email contactNo',
                    options: {lean: true}
                },
                {
                    path: "driverId",
                    match: {},
                    select: 'name dob phoneNumber address documents vehicleNumber currentLocation currentBooking',
                    options: {lean: true}
                }
            ];
            Service.VendorTruckService.getVendorTruckPopulate(query,projections,options,populate,function(err,result){
                if(err){
                    cb(err)
                }else{
                    data = result;
                    cb(null)
                }
            })
        }
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,data)
        }
    })
}

const listDriver = function(queryData,callback){
    let data = [];
    async.auto({
        listDrivers:function(cb){
            let query = {
                approvedByAdmin:queryData.adminVerifiy
            };
            if(queryData.vendorId){
                query.vendorId = queryData.vendorId
            }
            if(queryData.truckId){
                query.truckId = queryData.truckId
            }
          
            let projections = {};
            let options = {lean:true};
            let populate = [
                {
                    path: "truckId",
                    match: {},
                    select: '',
                    options: {lean: true}
                },
                {
                    path: "vendorId",
                    match: {},
                    select: '',
                    options: {lean: true}
                },
                {
                    path: "VendorTruck",
                    match: {},
                    select: '',
                    options: {lean: true}
                },
            ];

            Service.DriverService.getDriverPopulate(query,projections,options,populate,function(err,result){
                if(err){
                    cb(err)
                }else{
                    data = result;
                    cb(null)
                }
            })
        }
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,data)
        }
    })
}

const approveDriver = function(queryData,callback){
    async.auto({
        checkDriverId:function(cb){
            let query = {
                _id:queryData.driverId
            };
            let options = {lean:true};
            let projections = {
                approvedByAdmin:1
            };

            Service.DriverService.getDriver(query,projections,options,function(err,result){
                if(err){
                    callback(err)
                }else{
                    if(result && result.length){
                        if(result[0].approvedByAdmin == true){
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DRIVER_ALREADY_APPROVE)
                        }else{
                            cb(null)
                        }
                    }else{
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DRIVER_NO_EXISTS)
                    }
                }
            })
        },
        UpdateStatus:['checkDriverId',function(cb){
            let query = {_id:queryData.driverId};
            let options = {lean:true};
            let setData = {
                approvedByAdmin:true
            };

            Service.DriverService.updateDriver(query,setData,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    cb(null)
                }
            })
        }]

    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null)
        }
    })
}


const driverLocations = function(queryData,callback){
   let data = [];
   let objects=[];
    async.auto({
        getDriver:function(cb){
            let query = {
            };
            let truck={}
            if(queryData.truckStatus){
                truck.status=queryData.truckStatus
            }
            if(queryData.vendorId){
                query = {vendorId:queryData.vendorId}
            }
            if(queryData.driverId){
                query = {_id:queryData.driverId}
            }

            if('status' in queryData){
                query.currentBooking=queryData.status;
            }

            let options = {lean:true};
            let projections = {
                currentLocation :1,
                currentBooking :1,
                name:1,
                VendorTruck:1
            };

            var populate = [
                {
                    path: 'VendorTruck',
                    match: truck,
                    select: "status",
                    options: {}
                }
            ];
            Service.DriverService.getDriverPopulate(query,projections,options,populate,function(err,result){
                if(err){
                    cb(err)
                }else{
                    async.each(result,function(obj,cl){
                        if(obj.VendorTruck){
                            obj.currentLocation.push({
                                currentBooking:obj.currentBooking,
                                name:obj.name,
                                truckStatus:obj.VendorTruck  && obj.VendorTruck.status
                            })
                            data.push(obj.currentLocation);
                            // objects.push({
                            //     currentBooking:obj.currentBooking,
                            //     name:obj.name,
                            //     truckStatus:obj.VendorTruck  && obj.VendorTruck.status
                            // });
                        }
                        cl(null);
                    },function(err,res){
                          cb(null)
                    })
                  
                }
            })
        }
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,{locations:data,details:objects})
        }
    })
}

const bookingList = function(payloadData,callback){
    let data = [];
    let truckIds=[];
    let isDate=false;
    if(payloadData.startDate && payloadData.endDate){
       isDate=true;
   }

    async.auto({
        getTruckIds:function(cb){
            if(payloadData.truckId || payloadData.vendorId){
                var criteria={}

                if(payloadData.truckId){
                    criteria.truckId=payloadData.truckId
                }
                if(payloadData.vendorId){
                    criteria.vendorId=payloadData.vendorId
                }

                var projection={
                    _id:1
                }
                var options={}

                Service.VendorTruckService.getVendorTruck(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        truckIds=result.map(function(id){
                            return id._id;
                        })
                        cb();
                    }else{
                        return callback(null,data)
                    }
                }
            })
            }else{
                cb(null)
            }
        },
        bookingLisitng:['getTruckIds',function(cb){
            let query = {};
            if(payloadData.status == 'ALL'){
                query = {};
            } else if(payloadData.status == 'ON_THE_WAY'){
                query = {
                    status:
                        {
                            $in: ['TOWARDS_UNLOADING_LOCATION','STARTED','LOADING_LOCATION','UNLOADING_LOCATION']
                        }
                };
            } else {
                query = {
                    status:payloadData.status
                };
            }

            if(payloadData.truckId || payloadData.vendorId){
                query.truckId={$in:truckIds}
            }

            if(isDate){
                query.bookingDate={
                    $gt:payloadData.startDate,
                    $lt:payloadData.endDate
                }
            }

            if(payloadData.bookingId){
                query._id=payloadData.bookingId
            }


            let projection = {
                materials:1,
                companyId:1,
                contractId:1,
                pickupLocationId:1,
                dropOffLocationId:1,
                driverId:1,
                bookingDate:1,
                status:1,
                rejection:1,
                locationLogs:1,
                unloadInfo:1,
                bookingId:1,
                acceptTime:1,
                loadLocationReachTime:1,
                startTime:1,
                unloadLocationReachTime:1,
                endTime:1,
                loadingInfo:1

            };

            let options = {
                lean:true,
            }

                var populate = [
                    {
                    path: 'materials',
                    model:'Materials',
                    match: {},
                    select: {
                        materialName:1,
                        length:1,
                        breadth:1,
                        height:1,
                         accessionNo:1,
                        capacity:1
                    },
                    options: {
                        lean:true
                    }
                },
                 {
                    path: 'unloadInfo.materialId',
                    model:'Materials',
                    match: {},
                    select: {
                        materialName:1,
                        length:1,
                        breadth:1,
                        height:1,
                         accessionNo:1,
                        capacity:1
                    },
                    options: {
                        lean:true
                    }
                },
                 {
                    path: 'loadingInfo.materialId',
                    model:'Materials',
                    match: {},
                    select: {
                        materialName:1,
                        length:1,
                        breadth:1,
                        height:1,
                         accessionNo:1,
                        capacity:1
                    },
                    options: {
                        lean:true
                    }
                },
                {
                    path: 'companyId',
                    match: {},
                    select: {
                        name:1
                    },
                    options: {
                        lean:true
                    }
                },
               {
                    path: 'contractId',
                    match: {
                        
                    },
                    select: {
                        truckType:1,
                        freightAmount:1,
                        detentionCharges:1,
                        billingPeriod:1,
                        startCityId:1,
                        endCityId:1,
                        truckId:1
                    },
                    options: {}
                },
                 {
                    path: 'pickupLocationId',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1,
                        authPerson: 1,
                        phoneNumber: 1,
                    },
                    options: {
                        lean:true
                    }
                },
                 {
                    path: 'dropOffLocationId',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1,
                        authPerson: 1,
                        phoneNumber: 1,
                    },
                    options: {
                        lean:true
                    }
                },
                   {
                    path: 'driverId',
                    match: {},
                    select: {
                        name:1,
                        dob:1,
                        countryCode:1,
                        address:1,
                        pincode:1,
                        city:1,
                        state:1,
                        phoneNumber:1,
                        email:1,
                        currentLocation:1,
                        profilePicURL:1,
                        truckId:1,
                        vendorId:1,
                        VendorTruck:1,
                        currentBooking:1,
                        truckAssign:1

                    },
                    options: {}
                },
                ]  


            Service.BookingService.getBookingsPopulate(query,projection,options,populate,function(err,result){
                if(err){
                    cb(err)
                } else {
                    if(result && result.length){
                        data = result;
                    } 
                    cb(null)
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,data)
        }
    })
}

const trackBookingRoute = function(payloadData,callback){
    let data={};
    async.auto({
        bookingLisitng:function(cb){
            let query = {
                _id:payloadData.bookingId
            };
            let projection = {
                locationLogs:1
            };

            let options = {
                lean:true,
            }

            Service.BookingService.getBooking(query,projection,options,function(err,result){
                if(err){
                    cb(err)
                } else {
                    if(result && result.length){
                        data = result[0];
                    } 
                    cb(null)
                }
            })
        }
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,data)
        }
    })
}


var updateContractStatus=function(payloadData,callback){
    let oldStatus;

    async.auto({
        checkContractId:function(cb){
            var criteria={
                _id:payloadData.contractId,
                isDeleted:false
            }

            var projection={
                isBlocked:1
            }

            var options={}

            Service.ContractsService.getContract(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        oldStatus=result[0].isBlocked || false;
                        if(oldStatus==payloadData.block){
                            cb('Sorry Contract already '+(payloadData.block?'BLOCKED':'UNBLOCKED')+' .')
                        }else{
                             cb(null);
                        }
                       
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_ID)
                    }
                }
            })
        },
        updateStatus:['checkContractId',function(cb){
            var criteria={
                _id:payloadData.contractId,
                isDeleted:false
            }

            var setQuery={
                isBlocked:payloadData.block
            }

            var options={
                new:true
            }

            Service.ContractsService.updateContract(criteria,setQuery,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result && Object.keys(result).length){
                        cb(null)
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        }]
    },function(err,result){
        callback(err,{blockStatus:payloadData.block});
    })
}


var updateVendorTruckStatus=function(payloadData,callback){
    let oldStatus;

    

    async.auto({
        checkTruckId:function(cb){
            var criteria={
                _id:payloadData.truckId
            }
            if(payloadData.vendorId){
                criteria.vendorId=payloadData.vendorId
            }

            var projection={
                status:1
            }

            var options={}

            Service.VendorTruckService.getVendorTruck(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        oldStatus=result[0].status;
                        cb(null);
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_ID)
                    }
                }
            })
        },
        updateStatus:['checkTruckId',function(cb){

            if(payloadData.status==Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.BOOKED){
                if(oldStatus!=Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.VACANT || oldStatus==Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.BOOKED){
                    return cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_OPERATION)
                }
            }else if(payloadData.status==Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.VACANT){
                if(oldStatus==Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.VACANT){
                     return cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_OPERATION)
                }
            }else{
                if(oldStatus==Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.BOOKED){
                    return cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_OPERATION)
                }
            }


            var criteria={
                _id:payloadData.truckId
            }

            if(payloadData.vendorId){
                criteria.vendorId=payloadData.vendorId
            }

            var setQuery={
                status:payloadData.status
            }

            var options={
                new:true
            }

            Service.VendorTruckService.updateVendorTruck(criteria,setQuery,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result && Object.keys(result).length){
                        cb(null)
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        }],
        updateDriverStatus:['updateStatus',function(cb){
            
            var criteria={
                VendorTruck:payloadData.truckId
            }

            if(payloadData.vendorId){
                criteria.vendorId=payloadData.vendorId
            }

            var setQuery={
                $set:{}
            }

            var options={
                new:true
            }

            if(payloadData.status==Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.VACANT ){
                setQuery.$set.isAvailable=true;
                setQuery.$set.currentBooking=false;
            }else{
                setQuery.$set.isAvailable=false;
                setQuery.$set.currentBooking=true
            }
            
            Service.DriverService.updateDriver(criteria,setQuery,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                   cb()
                }
            })

        }],
        updateBookingStatus:['updateStatus',function(cb){
            if(payloadData.status==Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.VACANT){
                var criteria={
                    truckId:payloadData.truckId,
                    status:{$ne:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.ENDED}
                }

                var setQuery={
                    $set:{
                        status:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.ENDED
                    }
                }

                var options={
                    new:true
                }
                
                Service.BookingService.updateBookings(criteria,setQuery,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        cb()
                    }
                })
            }else{
                cb()
            }
        }]
    },function(err,result){
        callback(err,{});
    })
}



var getProfileData=function(queryData,callback){
    var criteria={
        _id:queryData.id
    }

    var projection={
        Email:1,
        emailNotifications:1,
        smsNotifications:1,
        pushNotifications
    }

    var options={
        lean:true
    }

    Service.AdminService.getAdmin(criteria,projection,options,function(err,result){
        if(err){
            callback(err)
        }else{
            if(result.length){
                callback(null,result[0])
            }else{
                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }
    })
}




var forgotPassword=function (payloadData,callback) {
    let accessToken;
    let userId;
    let userName;
    let userType;
    async.auto({
        checkEmailExist:function (cb) {
            var criteria={
                email:{ $regex: new RegExp("^" + payloadData.email+"$", "i") }
            }

            var projection={}
            var options={}

            if(payloadData.userType=='VENDOR'){
                userType=Config.APP_CONSTANTS.DATABASE.USER_ROLES.VENDOR;
                Service.VendorService.getVendor(criteria,projection,options,function (err,result) {
                    if(err){
                        cb(err);
                    }else{
                        if(result.length){
                            userId=result[0]._id;
                            userName=(result[0].name).trim();
                            return cb(null)
                        }else{
                            return cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_FOUND);
                        }
                    }
                })
            }else if(payloadData.userType=='DRIVER'){
                userType=Config.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER;
                Service.DriverService.getDriver(criteria,projection,options,function (err,result) {
                    if(err){
                        cb(err);
                    }else{
                        if(result.length){
                            userId=result[0]._id;
                            userName=(result[0].name).trim();
                            return cb(null)
                        }else{
                            return cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_FOUND);
                        }
                    }
                }) 
            }else if(payloadData.userType=='COMPANY'){
                userType=Config.APP_CONSTANTS.DATABASE.USER_ROLES.COMPANY;
                  Service.CompanyService.getCompany(criteria,projection,options,function (err,result) {
                    if(err){
                        cb(err);
                    }else{
                        if(result.length){
                            userId=result[0]._id;
                            userName=result[0].name.trim()
                            return cb(null)
                        }else{
                            return cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_FOUND);
                        }
                    }
                }) 
            }else{
                 userType=Config.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER;
                 Service.DriverService.getDriver(criteria,projection,options,function (err,result) {
                    if(err){
                        cb(err);
                    }else{
                        if(result.length){
                            userId=result[0]._id;
                            userName=(result[0].name).trim();
                            return cb(null)
                        }else{
                            return cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_FOUND);
                        }
                    }
                }) 
            }
        },
        generateTokenAndUpdate:['checkEmailExist',function(cb){

            var tokenData={
                id:userId,
                type:userType
            }
            TokenManager.setToken(tokenData,function(err,data){
                if(err){
                    cb(err);
                }else{
                    if(Object.keys(data).length){
                        accessToken=data.accessToken;
                        return cb(null);
                    }else{
                        return cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                    }
                }
            })
        }],
        sendEmail:['generateTokenAndUpdate',function (cb) {
             let emailVariables={
                user_name:userName,
                accessToken:accessToken
            }

            NotificationManager.sendEmailToUser('FORGOT_PASSWORD' ,emailVariables,payloadData.email,function(err,result){
                console.log('Error in email sending..................',err)
                cb(err);
            })

        }]
    },function (err,result) {
        callback(err,{});
    })
}


var updatePassword=function(payloadData,callback){
    var userData;
    var id;
    async.auto({
        checkToken:function(cb){
            TokenManager.decodeToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result && result.id && result.type){
                        userData=result;
                        
                        cb()
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        },
        checkTokenExist:['checkToken',function(cb){
            console.log('PPPPPPPPPPPPPPPPP',userData,payloadData)
            var criteria={
                accessToken:payloadData.accessToken
            }

            var projection={}

            var options={
                lean:true
            }

            if(userData.type==Config.APP_CONSTANTS.DATABASE.USER_ROLES.VENDOR){
                Service.VendorService.getVendor(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result.length){
                            id=result[0]._id;
                            cb(null)
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN)
                        }
                    }
                })
            }else if(userData.type==Config.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER){
                 Service.DriverService.getDriver(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result.length){
                            id=result[0]._id;
                            cb(null)
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN)
                        }
                    }
                })
            }else if(userData.type==Config.APP_CONSTANTS.DATABASE.USER_ROLES.COMPANY){
                 Service.CompanyService.getCompany(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result.length){
                            id=result[0]._id;
                            cb(null)
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN)
                        }
                    }
                })
            }else{
                 Service.LoaderService.getLoader(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result.length){
                            id=result[0]._id;
                            cb(null)
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN)
                        }
                    }
                })
            }
        }],
        updatePassword:['checkTokenExist',function(cb){
             var criteria={
                _id:id
            }

            var setQuery={
                $set:{
                    password:UniversalFunctions.CryptData(payloadData.password)
                }
            }

            var options={
                new:true
            }

            if(userData.type==Config.APP_CONSTANTS.DATABASE.USER_ROLES.VENDOR){
                Service.VendorService.updateVendor(criteria,setQuery,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            cb(null)
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }
                    }
                })
            }else if(userData.type==Config.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER){
                 Service.DriverService.updateDriver(criteria,setQuery,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            cb(null)
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }
                    }
                })
            }else if(userData.type==Config.APP_CONSTANTS.DATABASE.USER_ROLES.COMPANY){
                 Service.CompanyService.updateCompany(criteria,setQuery,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            cb(null)
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }
                    }
                })
            }else{
                  Service.LoaderService.updateLoader(criteria,setQuery,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            cb(null)
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }
                    }
                })
            }
        }]
    },function(err,result){
        callback(err,{});
    })

}

var logoutUser=function(payloadData,callback){
    var userData;
    var id;
    async.auto({
        update:function(cb){
             var criteria={
                _id:payloadData.id
            }

            var setQuery={
                $set:{
                    deviceToken:null,
                    accessToken:null,
                    isAvailable:false
                }
            }

            var options={
                new:true
            }

            if(payloadData.type==Config.APP_CONSTANTS.DATABASE.USER_ROLES.VENDOR){
                Service.VendorService.updateVendor(criteria,setQuery,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            cb(null)
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }
                    }
                })
            }else if(payloadData.type==Config.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER){
                 Service.DriverService.updateDriver(criteria,setQuery,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            cb(null)
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }
                    }
                })
            }else if(payloadData.type==Config.APP_CONSTANTS.DATABASE.USER_ROLES.COMPANY){
                 Service.CompanyService.updateCompany(criteria,setQuery,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            cb(null)
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }
                    }
                })
            }else if(payloadData.type==Config.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN){
                 Service.AdminService.updateAdmin(criteria,setQuery,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            cb(null)
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }
                    }
                })
            }
            else{
                  Service.LoaderService.updateLoader(criteria,setQuery,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && Object.keys(result).length){
                            cb(null)
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }
                    }
                })
            }
        }
    },function(err,result){
        callback(err,{});
    })

}




var postQuery=function(payloadData,callback){
        let emailVariables={
            source:payloadData.source,
            destination:payloadData.destination,
            companyName:payloadData.name,
            truckType:payloadData.truckType
        }

        let email=Config.APP_CONSTANTS.DATABASE.ADMIN_EMAIL;

        NotificationManager.sendEmailToUser('POST_QUERY' ,emailVariables,email,function(err,result){
            console.log('Error in email sending..................',err,'......RRRRRRRRr...',result)
            callback(err);
        })
}


var bookingAgain=function(payloadData,callback){
    var truckId;
    var contractId;
    var drivers=[];
    var vendors=[];
    var sendNot=false;
    var deviceTokens=[];
    var driverVendors=[];
    var sendMail=false;
    var startCityId;
    var endCityId;
    var startCityName;
    var endCityName;

    async.auto({
        checkBookingId:function(cb){
            var criteria={
                bookingId:parseInt(payloadData.bookingId),
                status:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.NO_DRIVER_FOUND
            }

            var projection={
                contractId:1
            }

            var options={
                lean:true
            }

            Service.BookingService.getBooking(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        contractId=result[0].contractId;
                        cb()
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_BOOKING_ID)
                    }
                }
            })
        },
        checkContractId:['checkBookingId',function (cb) {
            var criteria={
                _id:contractId
            }
            var projection={
                truckId:1,
                startCityId:1,
                endCityId:1
            }


             var populate = [
                {
                    path: 'startCityId',
                    match: {},
                    select:{},
                    options: {}
                },
                {
                    path: 'endCityId',
                    match: {},
                    select: {},
                    options: {}
                }
            ];
            var options={}

            Service.ContractsService.getContractPopulate(criteria,projection,options,populate,function (err,result) {
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        truckId=result[0].truckId;
                        startCityId=result[0].startCityId._id;
                        endCityId=result[0].endCityId._id;
                        startCityName=result[0].startCityId.cityName;
                        endCityName=result[0].endCityId.cityName;
                        cb(null);
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CONTRACT_ID)
                    }
                }
            })
        }],
         getDriversWitinRadius:['checkContractId',function (cb) {
         


            var criteria={
                truckId:truckId,
                status:Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.VACANT,
                 driverId:{$ne:null},
                $or:[
                    {$and:[
                            {startCityId:startCityId},
                            {endCityId:endCityId},
                            {currentLocation:{$ne:endCityId}}
                        ]},
                    {$and:[
                            {startCityId:endCityId},
                            {currentLocation:startCityId}
                        ]},
                        {$and:[
                            {startCityId:startCityId},
                            {endCityId:endCityId},
                            {currentLocation:{$exists:false}}
                        ]}  
                ]
            }

            var projection={
                driverId:1,
                vendorId:1
            }

            var options={
                lean:true
            }

            Service.VendorTruckService.getVendorTruck(criteria,projection,options,function (err,result) {
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        drivers=result.map(function (obj) {
                            return obj.driverId;
                        });

                        driverVendors=result.map(function(obj){
                            return {
                                vendorId:obj.vendorId,
                                driverId:obj.driverId
                            }
                        })

                        vendors=result.map(function(obj){
                            return obj.vendorId;
                        })

                    }else{
                        sendMail=true;
                    }

                    console.log("driver  ",drivers,'ffffffffffffff',vendors,' oooo ',truckId,' sss ',startCityId,'  eee',endCityId)
                    cb(null);
                }
            })

        }],
         checkVendorNotifications:['getDriversWitinRadius',function(cb){
            if(vendors.length && !sendMail){
                var criteria={
                    _id:{$in:vendors},
                    isDeleted:false,
                    isBlocked:false,
                    pushNotifications:true
                }
                var projection={
                }

                var options={}

                Service.VendorService.getVendor(criteria,projection,options,function (err,result) {

                    if(err){
                        cb(err)
                    }else{
                        if(result.length){
                            vendors=result.map(function(obj){
                                return obj._id;
                            })
                            
                        }else{
                            sendMail=true;
                        }

                        console.log('kkkkkkkkkkkkkk',driverVendors)

                        cb(null)

                    }
                })
            }else{
                 cb()
            }
        }],
        checkDriverAvalilabilty:['checkVendorNotifications',function (cb) {
            if(drivers.length && vendors.length && !sendMail){

                 driverVendors=driverVendors.filter(function(obj){
                                return vendors.indexOf(obj.vendorId)

                            })

                            driverVendors=driverVendors.map(function(obj){
                                return obj.driverId;
                            })

                var criteria={
                    _id:{$in:driverVendors},
                    isAvailable:true,
                    currentBooking:false,
                    truckAssign:true,
                    approvedByAdmin:true
                }
                var projection={
                    deviceToken:1,
                    deviceType:1,
                    email:1
                }

                var options={}

                Service.DriverService.getDriver(criteria,projection,options,function (err,result) {

                    if(err){
                        cb(err)
                    }else{
                        if(result.length){
                            drivers=result;
                            sendNot=true;
                          
                        }else{
                            sendMail=true;
                        }
                        cb()
                        

                    }
                })
            }else{
                
                cb()
            }
        }],
        sendEmail:['checkDriverAvalilabilty',function(cb){
            if(sendMail){
                let emailVariables={
                    startAddress:startCityName,
                    endAddress:endCityName,
                    bookingId:payloadData.bookingId
                }

                let email=Config.APP_CONSTANTS.DATABASE.ADMIN_EMAIL;

                NotificationManager.sendEmailToUser('NO_DRIVER_FOUND' ,emailVariables,email,function(err,result){
                    console.log('Error in email sending..................',err,'......RRRRRRRRr...',result)
                    cb(err);
                })
            }else{
                cb()
            }
        }],
        updateBookingInfoToDb:['checkDriverAvalilabilty',function (cb) {

            if(drivers.length){
                var criteria={
                    bookingId:parseInt(payloadData.bookingId)
                }

                var objectTOSave={
                    $set:{
                        status: Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.PENDING,
                        driverIds:drivers,
                        bookingDate:parseInt(+new Date()+19800000),
                        requestTime:parseInt(+new Date()+19800000)
                    }
                    
                }

                var options={
                    new:true
                }

                Service.BookingService.updateBookings(criteria,objectTOSave,options,function (err,result) {
                    if(err){
                        cb(err);
                    }else{
                        if(Object.keys(result).length){
                            cb(null);
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }

                    }
                })
            }else{
                cb()
            }

        }],
        sendNotification:['updateBookingInfoToDb',function (cb) {
            if(sendNot && !sendMail){
                deviceTokens=drivers.map(function (obj) {
                    return obj.deviceToken;
                });
                if(deviceTokens.length){
                    var temp = {};
                    temp.pushType=0;
                    temp.bookingId = bookingId;
                    temp.title =  company.firstName +" "+ company.lastName + " sends you new booking request";
                    var title =  'BOOKING_REQUEST';
                    DriverController.sendPushNotification('ANDROID', deviceTokens, temp, title, function (err) {
                        if (err) {
                            console.log('ERRRRRRRRRRRRRRR',err);
                            cb(null)
                        } else {
                            cb(null)
                        }
                    })
                }else{
                    cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                }
            }else{
                cb()
            }
        }]
    },function(err){
        callback(err,{})
    })
}



const sendPush=function(payloadData,callback){
    let sendPush=false;
    let isDriver=false;
    let deviceTokens=[];
     if(payloadData.option==Config.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER || payloadData.option==Config.APP_CONSTANTS.DATABASE.USER_ROLES.LOADER){
            sendPush=true;
            if(!(payloadData.ids && payloadData.ids.length)){
                return callback('ids are required');
            }
            if(payloadData.option==Config.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER){
                isDriver=true;
            }
    }

    async.auto({
      getDeviceTokens:function(cb){
           if(sendPush){
               var criteria={
                   _id:{$in:payloadData.ids},
                   isDeleted:false,
                   deviceToken:{$ne:null}
               }

               if(isDriver){
                   criteria.notification='1';
               }

               var projection={
                   deviceToken:1
               }

               var options={
                   lean:true
               }

               if(isDriver){
                   Service.DriverService.getDriver(criteria,projection,options,function(err,result){
                       if(err){
                           cb(err)
                       }else{
                           if(result && result.length){
                               deviceTokens=result.map(function(obj){
                                   return obj.deviceToken;
                               })
                           }
                           cb()
                       }
                   })
               }else{
                   Service.LoaderService.getLoader(criteria,projection,options,function(err,result){
                       if(err){
                           cb(err)
                       }else{
                           if(result && result.length){
                               deviceTokens=result.map(function(obj){
                                   return obj.deviceToken;
                               })
                           }
                           cb()
                       }
                   })
               }
            }else{
                cb()
            }
      },
      storePush:function(cb){
          var object={
                to: payloadData.ids || null,
                type:payloadData.option,
                message:payloadData.message
          }

          Service.NotificationService.createNotification(object,function(err,result){
              if(err){
                  cb(err)
              }else{
                  if(result && Object.keys(result).length){
                      cb();
                  }else{
                      cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                  }
              }
          })
      },
      sendPush:['getDeviceTokens','storePush',function(cb){
          if(sendPush && deviceTokens.length){
                var temp = {};
                temp.pushType=2;
                temp.title = payloadData.message;
                var title =  'ADMIN NOTIFICATION';
              if(isDriver){
                  
                NotificationManager.sendAndroidPushNotificationForDriver( deviceTokens, temp, title, function (err) {
                    if (err) {
                        console.log('ERRRRRRRRRRRRRRR in sending notification of driver',err);
                        cb(null)
                    } else {
                        cb(null)
                    }
                })
              }else{
             
                NotificationManager.sendAndroidPushNotificationForLoader( deviceTokens, temp, title, function (err) {
                    if (err) {
                        console.log('ERRRRRRRRRRRRRRR in sending notification of loader',err);
                        cb(null)
                    } else {
                        cb(null)
                    }
                })
              }
          }else{
              cb()
          }
      }]
    },function(err,result){
        callback(err,{});
    })
}


const getPushNotifications=function(queryData,callback){
    let responseObject={};
    let criteria={
        isDeleted:false
    }

    if(queryData.userType!=Config.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN){
        criteria.$or=[
            {to:{$elemMatch:{$eq:queryData.id}}},
            {to:null}
        ]
        criteria.type=queryData.userType
    }else{
        if(queryData.type){
            criteria.type=queryData.type
        }
    }
    async.auto({
        getCounts:function(cb){
            Service.NotificationService.getNotificationCount(criteria,function(err,result){
                if(err){
                    cb(err)
                }else{
                    responseObject.count=result;
                    cb()
                }
            })
        },
        getDetails:function(cb){
            var projection={
                isDeleted:0,
                to:0
            }

            var options={
                skip:queryData.skip,
                limit:queryData.limit
            }

            Service.NotificationService.getNotification(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    responseObject.results=result;
                    cb()
                }
            })
        }
    },function(err){
        callback(err,responseObject)
    })
}





var sendSMS=function(payloadData,callback){
    var NotificationManager = require('../Lib/NotificationManager');
     NotificationManager.sendSMSToUser('2134', payloadData.countryCode, payloadData.phoneNumber,function(err,result)
                        {
                            // NotificationManager.sendEmailToUser('OTP' ,{'OTP':OTP},payloadData.email,function(err,result)
                            // {
                            //     cb();
                            // })
                            callback(err)

                        })
}


module.exports = {
    adminLogin: adminLogin,
    addTruck:addTruck,
    listTruck:listTruck,
    listVendor:listVendor,
    verifiyVendor:verifiyVendor,
    listVendorTruck:listVendorTruck,
    listDriver:listDriver,
    approveDriver:approveDriver,
    driverLocations:driverLocations,
    bookingList:bookingList,
    updateContractStatus:updateContractStatus,
    updateVendorTruckStatus:updateVendorTruckStatus,
    getProfileData:getProfileData,
    forgotPassword:forgotPassword,
    updatePassword:updatePassword,
    verifyCompany:verifyCompany,
    postQuery:postQuery,
    bookingAgain:bookingAgain,
    addAccount:addAccount,
    editAccount:editAccount,
    clearAccount:clearAccount,
    getAccounts:getAccounts,
    trackBookingRoute:trackBookingRoute,
    getMessages:getMessages,
    logoutUser:logoutUser,
    sendPush:sendPush,
    getPushNotifications:getPushNotifications,
    sendSMS:sendSMS
};
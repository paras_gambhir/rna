var Config = require('../Config');
var Service = require('../Services');
var async = require('async');

var updateSetting=function(payloadData,callback){
   var criteria={
       _id:payloadData.id
   }

   var setQuery={
       $set:{}
   }
   if(payloadData.email){
       setQuery.$set.emailNotifications=payloadData.emailNotifications ? !payloadData.emailNotifications : true
   }

   if(payloadData.sms){
       setQuery.$set.smsNotifications=payloadData.smsNotifications ? !payloadData.smsNotifications : true
   }

   if(payloadData.push){
       setQuery.$set.pushNotifications=payloadData.pushNotifications ? !payloadData.pushNotifications : true
   }

   var options={
       new:true
   }

   if(payloadData.userType==Config.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN){
       Service.AdminService.updateAdmin(criteria,setQuery,options,function(err,result){
          callback(err,{});
       })
   }else if(payloadData.userType==Config.APP_CONSTANTS.DATABASE.USER_ROLES.VENDOR){
        Service.VendorService.updateVendor(criteria,setQuery,options,function(err,result){
          callback(err,{});
       })
   }else{
       Service.DriverService.updateDriver(criteria,setQuery,options,function(err,result){
           callback(err,{})
       })
   }

}

module.exports={
    updateSetting:updateSetting
}
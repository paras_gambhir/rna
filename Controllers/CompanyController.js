/**
 * Created by parasgambhir on 23/04/17.
 */


'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');
var UploadManager = require('../Lib/UploadManager');
var TokenManager = require('../Lib/TokenManager');
var Config = require('../Config');
var NotificationManager = require('../Lib/NotificationManager');
var distance = require('google-distance');
var emailPassword = require('../Lib/email');
var appConstants = require('../Config/appConstants');
var geoDist       =require('geodist')
var DriverController   =require('./DriverController')
var moment   = require('moment')

var companySignup = function(payloadData,callback)
{

    var companyId;
    var accessToken;
    var OTP;
    var insert=true;
    let obj={};
    async.auto({
        checkInVendor:function(cb){
            var criteria = {
                countryCode: payloadData.countryCode,
                contactNo: payloadData.phoneNumber
            };

            Service.VendorService.getVendorCount(criteria,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result){
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.COMPANY_NO_EXISTS)
                    }else{
                        cb()
                    }
                }
            })


        },
        checkInVendorEmail:function(cb){
            var criteria = {
                email:new RegExp('^'+payloadData.email+'$','i')
            };

            Service.VendorService.getVendorCount(criteria,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result){
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_ALREADY_EXIST)
                    }else{
                        cb()
                    }
                }
            })


        },
        checkForUserExists: ['checkInVendor','checkInVendorEmail',function (cb) {
            var criteria = {
                countryCode: payloadData.countryCode,
                phoneNumber: payloadData.phoneNumber,
                email :{$nin: [new RegExp('^'+payloadData.email+'$','i')] }
            };
            var projection = {};
            var option = {
                lean: true
            };

            Service.CompanyService.getCompany(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                }
                else {
                    if (result.length) {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.COMPANY_NO_EXISTS);
                    }
                    else {

                        var criteria = {
                            email : payloadData.email
                        };
                        var projection = {};
                        var option = {
                            lean: true
                        };
                        Service.CompanyService.getCompany(criteria, projection, option, function (err, result) {
                            if (err) {
                                cb(err)
                            }
                            else {

                                if (result.length) {
                                    if(result[0].step1 && result[0].step2 && result[0].step3 && result[0].emailVerify && result[0].approvedByAdmin){
                                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.COMPANY_EMAIL_EXISTS);
                                    }else if(result[0].step1 && result[0].step2 && result[0].step3 && !result[0].emailVerify){
                                        callback('Sorry email verification is pending , visit to your email to verification.')
                                    }else if(result[0].step1 && result[0].step2 && result[0].step3 && !result[0].approvedByAdmin){
                                        callback('Sorry your account is not approved by Admin.')
                                    }
                                    else{
                                        insert=false;
                                        obj=result[0];
                                        cb();
                                    }
                                    
                                }
                                else{
                                    cb();
                                }

                            }
                        });
                    }
                }

            });

        }],

        insertData: ['checkForUserExists',function(cb){
            OTP = UniversalFunctions.generateOTP();
            var cryptedPassword = UniversalFunctions.CryptData(payloadData.password);
            var criteria={
                email:new RegExp("^" +payloadData.email+"$",'i')
            }

            var dataToBeInserted = {
                $set:{
                    countryCode: payloadData.countryCode,
                    phoneNumber: payloadData.phoneNumber,
                    email : payloadData.email,
                    name: payloadData.name,
                    password: cryptedPassword,
                    step1:obj.step1 || true,
                    step2:obj.step2 || false,
                    step3:obj.step3 || false,
                    OTP:OTP
                },
                $setOnInsert:{
                    codeUpdatedAt: new Date(),
                    registrationDate: new Date(),
                    isBlocked:false,
                    profilePicURL: {
                        original:null,
                        thumbnail: null
                    },
                    verificationCode: null,
                    verifiedCodeVerified: false,
                    isApproved: false,
                    OTPVerified:false,
                    profileCompleted: false,
                    approvedByAdmin:false,
                    address: null,
                    emailVerify:false,
                    emailNotifications:true,
                    smsNotifications:true,
                    pushNotifications:true,
                    isDeleted:false
                }
                
            };

            var options={
                upsert:true,
                new:true
            }
            console.log(dataToBeInserted);
            Service.CompanyService.updateCompany(criteria,dataToBeInserted,options, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    obj=result;
                    if(obj.step1 && !obj.step2){

                        NotificationManager.sendSMSToUser(OTP, payloadData.countryCode, payloadData.phoneNumber,function(err,result)
                        {
                        // NotificationManager.sendEmailToUser('OTP' ,{'OTP':OTP},payloadData.email,function(err,result)
                        // {
                        //     cb();
                        // })
                        cb()

                        })
                    }else{
                        OTP='';
                        cb()
                    }
                    
                }
            });

        }],

        setToken: ['insertData', function (cb) {
            var tokenData = {
                id: companyId || obj._id,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.COMPANY
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        cb()
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })

        }]

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null,{accessToken: accessToken, OTP : OTP,profileCompleted:obj.profileCompleted,details:obj});
        }

    })

};


var verifyOTP = function (payloadData,callback) {

    var companyId;
    var OTPSaved;

    async.auto({

        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CompanyService.getCompany(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        companyId = result[0]._id;
                        OTPSaved = result[0].OTP;
                        if(OTPSaved == payloadData.OTP)
                        {
                            cb();
                        }
                        else{
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_OTP);
                        }
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })

        },
        updateCompany: ['checkForUserExists', function (cb) {
            var criteria = {
                _id: companyId
            };
            var setQuery = {
                OTPVerified : true,
                step2:true
            };
            Service.CompanyService.updateCompany(criteria, setQuery, {
                new: true,
                upsert: true
            }, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {});
        }

    })

};


var completeProfile = function(payloadData,callback)
{
    var companyId;
    var companyData;
    let accessToken;
    let userName;
    let email;
    async.auto({

        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CompanyService.getCompany(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        userName=result[0].name;
                        companyId = result[0]._id;
                        email=result[0].email;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })

        },
          setToken:['checkForUserExists',function(cb){
             var tokenData = {
                id: companyId,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.COMPANY
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })
        }],
        updateCompany:['setToken',function(cb)
        {
            var criteria = {
                _id: companyId
            };
            var setQuery = {
                address : payloadData.address,
                 pincode:payloadData.pincode,
                city:payloadData.city,
                state:payloadData.state,
                profileCompleted: true,
                accessToken:accessToken,
                step3:true
            };
            Service.CompanyService.updateCompany(criteria, setQuery, {
                new: true,
                upsert: true
            }, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    companyData = dataAry;
                    cb(null);
                }
            });
        }],
        sendEmail:['updateCompany',function(cb){
            let emailVariables={
                user_name:userName,
                accessToken:accessToken,
                userType:'COMPANY'
            }

            NotificationManager.sendEmailToUser('REGISTRATION_MAIL' ,emailVariables,email,function(err,result){
                console.log('Error in email sending..................',err,'......RRRRRRRRr...',result)
                cb(err);
            })
        }]
        
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {companyData:companyData});
        }
    })

};


var emailLogin  = function(payloadData,callback)
{

    var companyData;
    async.auto({

        checkForUserExists: function (cb) {
            var criteria = {
                email: { $regex: new RegExp("^" +payloadData.email+"$",'i')},
                password: UniversalFunctions.CryptData(payloadData.password)
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CompanyService.getCompany(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        companyData = result[0];
                        if(companyData.OTPVerified){
                            if(companyData.profileCompleted){
                                if(companyData.emailVerify){
                                     if(companyData.approvedByAdmin)
                                        {
                                            if(!companyData.isBlocked){
                                                 cb();
                                            }else{
                                                cb('Sorry your account is blocked by Admin.')
                                            }
                                        }
                                        else{
                                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_APPROVED);
                                        }
                                }else{
                                    cb('Sorry email verification is pending , visit to your email to verification.')
                                }
                            }else{
                                 cb('Sorry your profile is nor completed yet.');
                            }
                        }else{
                              cb('Sorry OTP verification pending.');
                        }
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_USER_PASS);
                    }
                }
            })

        }
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {companyData:companyData});
        }
    })
};



var addMaterial  = function(payloadData,callback)
{

    var companyId;
    var materials;
    async.auto({

        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CompanyService.getCompany(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        companyId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })

        },
         checkDuplicate:[ 'checkForUserExists',function (cb) {
            var criteria = {
                materialName:{ $regex: new RegExp("^" +payloadData.materialName+"$")} ,
                breadth: payloadData.breadth,
                length:payloadData.length,
                height:payloadData.height,
                CompanyId: companyId,
                isDeleted:false
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.MaterialsService.getMaterial(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE);
                    }
                    else {
                        cb()
                    }
                }
            })

        }],
        addMaterial:['checkDuplicate',function(cb)
        {


            var obj = {
                materialName: payloadData.materialName,
                breadth: payloadData.breadth,
                length:payloadData.length,
                height:payloadData.height,
                accessionNo:payloadData.accessionNo,
                capacity:payloadData.capacity,
                CompanyId: companyId
            };

            Service.MaterialsService.createMaterial(obj,function (err, result) {
                if (err) {
                    cb(err);
                } else {

                    materials = result;
                    cb();
                }
            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {materials:materials});
        }
    })
};


var editMaterial  = function(payloadData,callback)
{

    var companyId;
    var materials;
    async.auto({

        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CompanyService.getCompany(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        companyId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        },
        checkMaterialExist: function (cb) {
            var criteria = {
                _id: payloadData.id,
                isDeleted:false
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.MaterialsService.getMaterial(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_ID);
                    }
                }
            })

        },
         checkDuplicate:[ 'checkForUserExists','checkMaterialExist',function (cb) {
            var criteria = {
                _id: {$ne:payloadData.id},
                materialName:{ $regex: new RegExp("^" +payloadData.materialName+"$")} ,
                breadth: payloadData.breadth,
                length:payloadData.length,
                height:payloadData.height,
                CompanyId: companyId,
                isDeleted:false
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.MaterialsService.getMaterial(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE);
                    }
                    else {
                        cb()
                    }
                }
            })

        }],
        editMaterial:['checkDuplicate',function(cb)
        {

            var criteria={
                _id:payloadData.id,
                isDeleted:false
            }

            var obj = {
                materialName: payloadData.materialName,
                breadth: payloadData.breadth,
                length:payloadData.length,
                height:payloadData.height,
                CompanyId: companyId,
                 accessionNo:payloadData.accessionNo,
                capacity:payloadData.capacity
            };

            var options={
                new:true
            }

            Service.MaterialsService.updateMaterial(criteria,obj,options,function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    materials = result;
                    cb();
                }
            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {materials:materials});
        }
    })
};


var listMaterial = function(payloadData,callback)
{

    var materials;
    var companyId;
    async.auto({

        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CompanyService.getCompany(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        companyId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })

        },
        listMaterial:['checkForUserExists', function (cb) {
            var criteria = {
                CompanyId: companyId
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.MaterialsService.getMaterial(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    materials = result;
                    cb();
                }
            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {materials:materials});
        }
    })
};


var addContract = function(payloadData,callback)
{
    var contracts;
      if(payloadData.startCityId == payloadData.endCityId){
        return callback('Start end End Cities should be different. ')
    }
    async.auto({
        checkForTruckIdExists: function (cb) {
            var criteria = {
                _id: payloadData.truckId
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.TruckService.getTruck(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TRUCK_ID);
                    }
                }
            })

        },
         checkCompanyIdExists: function (cb) {
            var criteria = {
                _id: payloadData.companyId
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CompanyService.getCompany(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COMPANY_ID);
                    }
                }
            })

        },
         getStartCityLat:function(cb){
            var criteria={
                _id:payloadData.startCityId,
                isDeleted:false
            }

            var projection={
                cityName:1,
                location:1
            }

            var options={}

            Service.CityService.getCity(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        cb(null)
                    }else{
                        cb('Invalid Start City Id.')
                    }
                }
            })
        },
         getEndCityLat:function(cb){
            var criteria={
                _id:payloadData.endCityId,
                isDeleted:false
            }

            var projection={
                cityName:1,
                location:1
            }

            var options={}

            Service.CityService.getCity(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        cb(null)
                    }else{
                        cb('Invalid End City Id.')
                    }
                }
            })
        },
        checkDuplicateExist:['checkForTruckIdExists','getStartCityLat','getEndCityLat','checkCompanyIdExists',function(cb){
             var criteria={
                companyId:payloadData.companyId,
                truckId:payloadData.truckId,
                startCityId:payloadData.startCityId,
                endCityId:payloadData.endCityId,
                isDeleted:false
            }

            var projection={}

            var options={}

            Service.ContractsService.getContract(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE)
                    }else{
                        cb()
                    }
                }
            })
        }],
        addContractToDb:['checkDuplicateExist',function(cb)
        {
            var dataToSet = {
                companyId:payloadData.companyId,
                truckType:payloadData.truckType,
                freightAmount:payloadData.freightAmount,
                detentionCharges:payloadData.detentionCharges,
                billingPeriod:payloadData.billingPeriod,
                truckId:payloadData.truckId,
                startCityId:payloadData.startCityId,
                endCityId:payloadData.endCityId
            };

            Service.ContractsService.createContract(dataToSet,function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    contracts = result;
                    cb();
                }
            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {contracts:contracts});
        }
    })

};



var editContract = function(payloadData,callback)
{
    var contracts;

     if(payloadData.startCityId == payloadData.endCityId){
        return callback('Start end End Cities should be different. ')
    }

    async.auto({
        checkForContractIdExists: function (cb) {
            var criteria = {
                _id: payloadData.id
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.ContractsService.getContract(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_ID);
                    }
                }
            })

        },
        checkForTruckIdExists: function (cb) {
            var criteria = {
                _id: payloadData.truckId
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.TruckService.getTruck(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TRUCK_ID);
                    }
                }
            })

        },
         checkCompanyIdExists: function (cb) {
            var criteria = {
                _id: payloadData.companyId
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CompanyService.getCompany(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COMPANY_ID);
                    }
                }
            })

        },
         getStartCityLat:function(cb){
            var criteria={
                _id:payloadData.startCityId,
                isDeleted:false
            }

            var projection={
                cityName:1,
                location:1
            }

            var options={}

            Service.CityService.getCity(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        cb(null)
                    }else{
                        cb('Invalid Start City Id.')
                    }
                }
            })
        },
         getEndCityLat:function(cb){
            var criteria={
                _id:payloadData.endCityId,
                isDeleted:false
            }

            var projection={
                cityName:1,
                location:1
            }

            var options={}

            Service.CityService.getCity(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        cb(null)
                    }else{
                        cb('Invalid End City Id.')
                    }
                }
            })
        },
        checkDuplicasy:['checkForTruckIdExists','getStartCityLat','getEndCityLat','checkCompanyIdExists','checkForContractIdExists',function(cb){
            var criteria={
                _id:{$ne:payloadData.id},
                companyId:payloadData.companyId,
                truckType:{ $regex: new RegExp("^" +payloadData.truckType+"$")},
                startCityId: payloadData.startCityId,
                endCityId: payloadData.endCityId,
                truckId:payloadData.truckId
            }

            var projection={}
            var options={
                lean:true
            }
            Service.ContractsService.getContract(criteria,projection,options,function(err,result){
                if(err){
                    cb(er)
                }else{
                    if(result.length){
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE)
                    }else{
                        cb()
                    }
                }
            })
        }],
        editContractToDb:['checkDuplicasy',function(cb)
        {

            var criteria={
                _id:payloadData.id
            }

            var dataToSet = {
                companyId:payloadData.companyId,
                truckType:payloadData.truckType,
                freightAmount:payloadData.freightAmount,
                detentionCharges:payloadData.detentionCharges,
                billingPeriod:payloadData.billingPeriod,
                startCityId:payloadData.startCityId,
                endCityId:payloadData.endCityId,
                truckId:payloadData.truckId
            };

            var options={
                new:true
            }

            Service.ContractsService.updateContract(criteria,dataToSet,options,function (err, result) {
                if (err) {
                    cb(err);
                } else {

                    if(result && Object.keys(result).length){
                        cb()
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {contracts:contracts});
        }
    })

};






var listContractsAdmin = function(payloadData,callback)
{

    var contracts;
    var companyId;
    var count=0;
    async.auto({

        checkCompnayId: function (cb) {

            if(payloadData.companyId){

                var criteria = {
                    _id: payloadData.companyId
                };
                var projection = {};
                var option = {
                    lean: true
                };
                Service.CompanyService.getCompany(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err);
                    } else {
                        if (result.length) {
                            cb();
                        }
                        else {
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COMPANY_ID);
                        }
                    }
                })

            }else{
                cb(null)
            }

        },
        getCounts:['checkCompnayId',function(cb){
            var criteria={}
            if(payloadData.companyId){
                criteria._id=payloadData.companyId
            }

            Service.ContractsService.getContractsCount(criteria,function(err,result){
                if(err){
                    cb(err)
                }else{
                    count=result;
                    cb(null)
                }
            })
            
        }],
        getContracts:['checkCompnayId',function(cb)
        {
            var criteria = {};

            if(payloadData.companyId){
                criteria._id=payloadData.companyId
            }

            var projection = {
                isDeleted:0
            };
            var option = {
                lean: true,
                skip:payloadData.skip,
                limit:payloadData.limit
            };

            var populate = [
                 {
                    path: 'companyId',
                    match: {},
                    select: {},
                    options: {}
                },
                {
                    path: 'startCityId',
                    match: {},
                    select: {
                        _id:1,
                        cityName:1,
                        location:1
                    },
                    options: {}
                },
                {
                    path: 'endCityId',
                    match: {},
                    select: {
                         _id:1,
                        cityName:1,
                        location:1
                    },
                    options: {}
                }
                ]  
            Service.ContractsService.getContractPopulate(criteria, projection, option,populate, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                        contracts = result;
                        cb();

                }
            })

        }]

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {count:count,results:contracts});
        }
    })
};




var listContracts = function(payloadData,callback)
{

    var contracts;
    var companyId;
    let count=0;
    async.auto({

        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CompanyService.getCompany(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        contracts = result[0].contracts;
                        companyId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        },
        getCounts:['checkForUserExists',function(cb){
            var criteria={
                companyId:companyId
            }
            
            Service.ContractsService.getContractsCount(criteria,function(err,result){
                if(err){
                    cb(err)
                }else{
                    count=result;
                    cb(null)
                }
            })
            
        }],
        getContracts:['checkForUserExists',function(cb)
        {
            var criteria = {
                 companyId:companyId
            };

            var projection = {};
            var option = {
                lean: true,
                skip:payloadData.skip,
                limit:payloadData.limit
            };

            var populate = [
                {
                    path: 'companyId',
                    match: {},
                    select: {
                        _id:1,
                        name:1,
                        phoneNumber:1
                    },
                    options: {}
                },
                 {
                    path: 'startCityId',
                    match: {},
                    select: {
                        _id:1,
                        cityName:1,
                        location:1
                    },
                    options: {}
                },
                {
                    path: 'endCityId',
                    match: {},
                    select: {
                         _id:1,
                        cityName:1,
                        location:1
                    },
                    options: {}
                }
                ]  
            Service.ContractsService.getContractPopulate(criteria, projection, option,populate, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                        contracts = result;
                        cb();

                }
            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {count:count,contracts:contracts});
        }
    })
};




var addBooking = function(payloadData,callback)
{
    console.log("payload ",payloadData);
    var company;
    var drivers=[];
    var truckId;
    var radius;
    var sendNot=false;
    var deviceTokens=[];
    var loadingLocations=[];
    var unloadingLocations=[];
    var allMaterials=[];
    var bookingId;
    var startCityId;
    var endCityId;
    var vendors=[];
    var driverVendors=[];
    var bookingId=11111;
    var sendMail=false;
    var startCityName;
    var endCityName;
    let truckType;

    if(!moment(payloadData.bookingDate).isValid){
        return callback('Invalid booking date.')
    }

     console.log('LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL')

    async.auto({
        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken,
                isDeleted:false,
                isBlocked:false
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CompanyService.getCompany(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        company = result[0];
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })

        },
        checkContractId:['checkForUserExists',function (cb) {
            var criteria={
                _id:payloadData.contractId
            }
            var projection={
                truckId:1,
                startCityId:1,
                endCityId:1,
                truckType:1
            }


             var populate = [
                {
                    path: 'startCityId',
                    match: {},
                    select:{},
                    options: {}
                },
                {
                    path: 'endCityId',
                    match: {},
                    select: {},
                    options: {}
                }
            ];
            var options={}

            Service.ContractsService.getContractPopulate(criteria,projection,options,populate,function (err,result) {
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        truckId=result[0].truckId;
                        startCityId=result[0].startCityId._id;
                        endCityId=result[0].endCityId._id;
                        startCityName=result[0].startCityId.cityName;
                        endCityName=result[0].endCityId.cityName;
                        truckType = result[0].truckType;
                        cb(null);
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CONTRACT_ID)
                    }
                }
            })
        }],
        // getTrucks:['checkContractId',function (cb) {
        //     var startLocation={
        //         lat:payloadData.loadingLocations[0].location[0],
        //         lon:payloadData.loadingLocations[0].location[1]
        //     }

        //     var endLocation={
        //         lat:payloadData.unloadingLocations[payloadData.unloadingLocations.length-1].location[0],
        //         lon:payloadData.unloadingLocations[payloadData.unloadingLocations.length-1].location[1]
        //     }
        //     var options={
        //         unit:'km',
        //         exact:true
        //     }

        //     radius=(geoDist(startLocation,endLocation,options)/10);
        //     console.log("radius  ",radius)
        //     cb(null);
        // }],
        getDriversWitinRadius:['checkContractId',function (cb) {
            // var startLat=payloadData.loadingLocations[0].location[0];
            // var startLong=payloadData.loadingLocations[0].location[1];


            // var endLat=payloadData.unloadingLocations[payloadData.unloadingLocations.length-1].location[0]
            // var endLong=payloadData.unloadingLocations[payloadData.unloadingLocations.length-1].location[1]

            // var criteria={
            //     // startLocation: {
            //     //     $geoWithin: { $centerSphere: [ [startLong, startLat], (radius/6378.1) ] }
            //     // },
            //     // endLocation: {
            //     //     $geoWithin: { $centerSphere: [ [endLong, endLat], (radius/6378.1) ] }
            //     // },
            //     truckId:truckId
            // }



            var criteria={
                truckId:truckId,
                status:Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.VACANT,
                 driverId:{$ne:null},
                $or:[
                    {$and:[
                            {startCityId:startCityId},
                            {endCityId:endCityId},
                            {currentLocation:{$ne:endCityId}}
                        ]},
                    {$and:[
                            {startCityId:endCityId},
                            {currentLocation:startCityId}
                        ]},
                        {$and:[
                            {startCityId:startCityId},
                            {endCityId:endCityId},
                            {currentLocation:{$exists:false}}
                        ]}  
                ]
            }

            var projection={
                driverId:1,
                vendorId:1
            }

            var options={
                lean:true
            }

            Service.VendorTruckService.getVendorTruck(criteria,projection,options,function (err,result) {
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        drivers=result.map(function (obj) {
                            return obj.driverId;
                        });

                        driverVendors=result.map(function(obj){
                            return {
                                vendorId:obj.vendorId,
                                driverId:obj.driverId
                            }
                        })

                        vendors=result.map(function(obj){
                            return obj.vendorId;
                        })

                    }else{
                        sendMail=true;
                    }

                    console.log("driver  ",drivers,'ffffffffffffff',vendors,' oooo ',truckId,' sss ',startCityId,'  eee',endCityId)
                    cb(null);
                }
            })

        }],
        checkVendorNotifications:['getDriversWitinRadius',function(cb){
            if(vendors.length && !sendMail){
                var criteria={
                    _id:{$in:vendors},
                    isDeleted:false,
                    isBlocked:false,
                    pushNotifications:true
                }
                var projection={
                }

                var options={}

                Service.VendorService.getVendor(criteria,projection,options,function (err,result) {

                    if(err){
                        cb(err)
                    }else{
                        if(result.length){
                            vendors=result.map(function(obj){
                                return obj._id;
                            })
                            
                        }else{
                            sendMail=true;
                        }

                        console.log('kkkkkkkkkkkkkk',driverVendors)

                        cb(null)

                    }
                })
            }else{
                 cb()
            }
        }],
        checkDriverAvalilabilty:['checkVendorNotifications',function (cb) {
            if(drivers.length && vendors.length && !sendMail){

                 driverVendors=driverVendors.filter(function(obj){
                                return vendors.indexOf(obj.vendorId)

                            })

                            driverVendors=driverVendors.map(function(obj){
                                return obj.driverId;
                            })

                var criteria={
                    _id:{$in:driverVendors},
                    isAvailable:true,
                    currentBooking:false,
                    truckAssign:true,
                    approvedByAdmin:true
                }
                var projection={
                    deviceToken:1,
                    deviceType:1,
                    email:1
                }

                var options={}

                Service.DriverService.getDriver(criteria,projection,options,function (err,result) {

                    if(err){
                        cb(err)
                    }else{
                        if(result.length){
                            drivers=result;
                            sendNot=true;
                          
                        }else{
                            sendMail=true;
                        }
                        cb()
                        

                    }
                })
            }else{
                
                cb()
            }
        }],
        getBookingId:['checkDriverAvalilabilty',function(cb){
                let aggregateArray = [
                    {
                        $group: {
                            _id: '',
                            bookingId: {$max: '$bookingId'}
                        }
                    }
                ]

                Service.BookingService.getBookingAggregated(aggregateArray, function (err, result) {
                    if (err) {
                        cb(err);
                    } else {
                        if (result.length && result[0].bookingId && typeof result[0].bookingId !='NaN') {
                            bookingId = parseInt(result[0].bookingId) + 1;
                        }
                        cb(null);
                    }
                })
        }],
        sendEmail:['getBookingId',function(cb){
            if(sendMail){
                let emailVariables={
                    startAddress:startCityName,
                    endAddress:endCityName,
                    bookingId:bookingId
                }

                let email=Config.APP_CONSTANTS.DATABASE.ADMIN_EMAIL;

                NotificationManager.sendEmailToUser('NO_DRIVER_FOUND' ,emailVariables,email,function(err,result){
                    console.log('Error in email sending..................',err,'......RRRRRRRRr...',result)
                    cb(err);
                })
            }else{
                cb()
            }
        }],
        insertBookingInfoToDb:['getBookingId',function (cb) {

            let date=payloadData.bookingDate.split('/');

            date=+new Date(date[1]+'/'+date[0]+'/'+date[2])

            var objectTOSave={
                companyId:company._id,
                status: !sendMail?Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.PENDING:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.NO_DRIVER_FOUND,
                contractId:payloadData.contractId,
                driverIds:drivers,
                pickupLocationId:payloadData.pickupLocationId,
                bookingId:bookingId,
                bookingDate:parseInt(+new Date()+19800000),
                requestTime:parseInt(+new Date()+19800000),
                pickupAddress:startCityName || '',
                dropOffAddress:endCityName || '',
                truckType:truckType || ''
            }

           

            payloadData.dropOffLocationId  &&  (objectTOSave.dropOffLocationId=payloadData.dropOffLocationId)

            payloadData.materialIds && payloadData.materialIds.length && (objectTOSave.materials=payloadData.materialIds)

            Service.BookingService.createBookings(objectTOSave,function (err,result) {
                if(err){
                    cb(err);
                }else{
                    if(Object.keys(result).length){
                        bookingId=result.bookingId;
                        cb(null);
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }

                }
            })

        }],
        sendNotification:['insertBookingInfoToDb',function (cb) {
            if(sendNot && !sendMail){
                deviceTokens=drivers.map(function (obj) {
                    return obj.deviceToken;
                });
                if(deviceTokens.length){
                    var temp = {};
                    temp.pushType=0;
                    temp.bookingId = bookingId;
                    temp.title =  company.name + " sends you new booking request";
                    var title =  'BOOKING_REQUEST';
                    DriverController.sendPushNotification('ANDROID', deviceTokens, temp, title, function (err) {
                        if (err) {
                            console.log('ERRRRRRRRRRRRRRR',err);
                            cb(null)
                        } else {
                            cb(null)
                        }
                    })
                }else{
                    cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                }
            }else{
                cb()
            }
        }]
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {});
        }
    })

};

const globalTruck = function(payloadData,callback){
    var data = [];
    async.auto({
        list:function(cb){
            var query = {};
            var options = {lean:true};
            var projections = {};
            Service.TruckService.getTruck(query,projections,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    data = result;
                    cb(null)
                }
            })
        }
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,data)
        }
    })
};

const bookingListing = function (payloadData,callback) {
   var company ;
   var  data = [];
   var isDate=false;
   if(payloadData.startDate && payloadData.endDate){
       isDate=true;
   }
    async.auto({
        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                
            };
            var option = {
                lean: true
            };
            Service.CompanyService.getCompany(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        company = result[0];
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        },
        currentBooking:['checkForUserExists',function(cb){
            if(payloadData.status == 'CURRENT'){
                var query = {
                    companyId:company._id,
                    status:{$nin:['ENDED','REJECTED']}
                };
                 if(isDate){
                    query.bookingDate={
                    $gt:payloadData.startDate,
                    $lt:payloadData.endDate
                }
            }
            if(payloadData.bookingId){
                query._id=payloadData.bookingId;
            }
            

                var options = {lean:true};
                var projections = {
                    status:1,
                    contractId:1,
                    pickupLocationId:1,
                    materials:1,
                    dropOffLocationId:1,
                    truckId:1,
                    bookingId:1,
                    driverId:1,
                    companyId:1,
                    companyAmount:1,
                    companyDate:1,
                    acceptTime:1,
                    loadLocationReachTime:1,
                    startTime:1,
                    unloadLocationReachTime:1,
                    endTime:1,
                    loadingInfo:1,
                     bookingDate:1,
                     unloadInfo:1,
                      locationLogs:1

                }
                 var populate = [
                {
                    path: 'driverId',
                    match: {},
                    select: {
                        name:1,
                        dob:1,
                        countryCode:1,
                        address:1,
                        pincode:1,
                        city:1,
                        state:1,
                        phoneNumber:1,
                        email:1,
                        currentLocation:1,
                        profilePicURL:1,
                        truckId:1,
                        vendorId:1,
                        VendorTruck:1,
                        currentBooking:1,
                        truckAssign:1

                    },
                    options: {}
                },
                 {
                    path: 'contractId',
                    match: {
                        
                    },
                    select: {
                        truckType:1,
                        freightAmount:1,
                        detentionCharges:1,
                        billingPeriod:1,
                        startCityId:1,
                        endCityId:1,
                        truckId:1
                    },
                    options: {}
                },
                 {
                    path: 'materials',
                     model:'Materials',
                    match: {},
                    select: {
                        materialName:1,
                        length:1,
                        breadth:1,
                        height:1,
                        accessionNo:1,
                        capacity:1
                    },
                    options: {}
                },
                 {
                    path: 'pickupLocationId',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1,
                        authPerson: 1,
                        phoneNumber: 1,
                    },
                    options: {
                        lean:true
                    }
                },
                 {
                    path: 'dropOffLocationId',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1,
                        authPerson: 1,
                        phoneNumber: 1,
                    },
                    options: {
                        lean:true
                    }
                },
                  {
                    path: 'loadingInfo.materialId',
                    model:'Materials',
                    match: {},
                    select: {
                        materialName:1,
                        length:1,
                        breadth:1,
                        height:1,
                         accessionNo:1,
                        capacity:1
                    },
                    options: {
                        lean:true
                    }
                },
                 {
                    path: 'unloadInfo.materialId',
                    model:'Materials',
                    match: {},
                    select: {
                        materialName:1,
                        length:1,
                        breadth:1,
                        height:1,
                         accessionNo:1,
                        capacity:1
                    },
                    options: {
                        lean:true
                    }
                }
                ]  
                Service.BookingService.getBookingsPopulate(query,projections,options,populate,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && result.length){
                            data = result;
                        }
                        cb(null);

                    }
                })
            }else{
                cb(null)
            }

        }],
        historyBooking:['checkForUserExists',function(cb){
            if(payloadData.status == 'HISTORY'){
                var query = {
                    companyId:company._id,
                    status:{$in:['ENDED','REJECTED']}
                };
                if(isDate){
                    query.bookingDate={
                    $gt:payloadData.startDate,
                    $lt:payloadData.endDate
                }
            }
            
              if(payloadData.bookingId){
                query._id=payloadData.bookingId;
            }

                var options = {lean:true};
                var projections = {
                     status:1,
                    contractId:1,
                    pickupLocationId:1,
                    materials:1,
                    dropOffLocationId:1,
                    truckId:1,
                    bookingId:1,
                    driverId:1,
                    companyId:1,
                    companyAmount:1,
                    companyDate:1,
                    acceptTime:1,
                    loadLocationReachTime:1,
                    startTime:1,
                    unloadLocationReachTime:1,
                    endTime:1,
                    loadingInfo:1,
                     bookingDate:1,
                     unloadInfo:1,
                      locationLogs:1

                }
                 var populate = [
               {
                    path: 'driverId',
                    match: {},
                    select: {
                        name:1,
                        dob:1,
                        countryCode:1,
                        address:1,
                        pincode:1,
                        city:1,
                        state:1,
                        phoneNumber:1,
                        email:1,
                        currentLocation:1,
                        profilePicURL:1,
                        truckId:1,
                        vendorId:1,
                        VendorTruck:1,
                        currentBooking:1,
                        truckAssign:1

                    },
                    options: {}
                },
                 {
                    path: 'contractId',
                    match: {
                        
                    },
                    select: {
                        truckType:1,
                        freightAmount:1,
                        detentionCharges:1,
                        billingPeriod:1,
                        startCityId:1,
                        endCityId:1,
                        truckId:1
                    },
                    options: {}
                },
                 {
                    path: 'materials',
                     model:'Materials',
                    match: {},
                    select: {
                        materialName:1,
                        length:1,
                        breadth:1,
                        height:1,
                        accessionNo:1,
                        capacity:1
                    },
                    options: {}
                },
                 {
                    path: 'pickupLocationId',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1,
                        authPerson: 1,
                        phoneNumber: 1,
                    },
                    options: {
                        lean:true
                    }
                },
                 {
                    path: 'dropOffLocationId',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1,
                        authPerson: 1,
                        phoneNumber: 1,
                    },
                    options: {
                        lean:true
                    }
                },
                  {
                    path: 'loadingInfo.materialId',
                    model:'Materials',
                    match: {},
                    select: {
                        materialName:1,
                        length:1,
                        breadth:1,
                        height:1,
                         accessionNo:1,
                        capacity:1
                    },
                    options: {
                        lean:true
                    }
                },
                 {
                    path: 'unloadInfo.materialId',
                    model:'Materials',
                    match: {},
                    select: {
                        materialName:1,
                        length:1,
                        breadth:1,
                        height:1,
                         accessionNo:1,
                        capacity:1
                    },
                    options: {
                        lean:true
                    }
                }
                ]  

                Service.BookingService.getBookingsPopulate(query,projections,options,populate,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result && result.length){
                            data = result;
                        }
                        cb(null)

                    }
                })
            }else{
                cb(null)
            }
        }]
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,data)
        }
    })
}


const getCompaniesList=function(queryData,callback){
    var count=0;
    var responseArray=[];
    async.auto({
        getCountFromDb:function(cb){
            var criteria={}
            if('isApproved' in queryData){
                criteria.approvedByAdmin=queryData.isApproved;
            }

            Service.CompanyService.getCompaniesCount(criteria,function(err,result){
                if(err){
                    cb(err)
                }else{
                    count=result;
                    cb()
                }
            })
        },
        getCompaniesData:function(cb){
            var criteria={}
            if('isApproved' in queryData){
                criteria.approvedByAdmin=queryData.isApproved;
            }

            var projection={
                name:1,
                countryCode:1,
                phoneNumber:1,
                email:1,
                registrationDate:1,
                profilePicURL:1,
                profileCompleted:1,
                address:1,
                pincode:1,
                city:1,
                state:1,
                approvedByAdmin:1
            }

            var options={
             lean:true,
             skip:queryData.skip,
             limit:queryData.limit   
            }

            Service.CompanyService.getCompany(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    responseArray=result;
                    cb();
                }
            })
        }
    },function(err,result){
        callback(err,{count:count,results:responseArray})
    })
}


var updateCompanyBlockStatus=function(payloadData,callback){
    let oldStatus;

    async.auto({
        checkCompanyId:function(cb){
            var criteria={
                _id:payloadData.companyId,
                isDeleted:false
            }

            var projection={
                isBlocked:1
            }

            var options={}

            Service.CompanyService.getCompany(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        oldStatus=result[0].isBlocked || false;
                        if(oldStatus==payloadData.block){
                            cb('Sorry Company already '+(payloadData.block?'BLOCKED':'UNBLOCKED')+' .')
                        }else{
                             cb(null);
                        }
                       
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_ID)
                    }
                }
            })
        },
        updateStatus:['checkCompanyId',function(cb){
            var criteria={
                _id:payloadData.companyId,
                isDeleted:false
            }

            var setQuery={
                isBlocked:payloadData.block
            }

            var options={
                new:true
            }

            Service.CompanyService.updateCompany(criteria,setQuery,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result && Object.keys(result).length){
                        cb(null)
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        }]
    },function(err,result){
        callback(err,{blockStatus:payloadData.block});
    })
}


var editProfilePic=function(payloadData,callback){

    let imageData={};

    async.auto({
        uploadImage:function(cb){
            UploadManager.uploadFile(payloadData.image && payloadData.image.filename ? payloadData.image :payloadData.image[1], Math.random()*1000, "hello", function (err, uploadedInfo) {
                if (err) {
                    callback(err)
                } else {
                    console.log("uploaded info", uploadedInfo);
                    imageData.original = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                    imageData.thumbnail = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.thumbnail
                    cb();
                }
            })
        },
        updateCompanyInDb:['uploadImage',function(cb){

            if(imageData && imageData.original){
                var criteria={
                    _id:payloadData.companyId,
                    isDeleted:false
                }

                var setQuery={
                    $set:{
                        profilePicURL:imageData
                    }
                    
                }

                var options={
                    new:true
                }

                Service.CompanyService.updateCompany(criteria,setQuery,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{

                        console.log('result')
                        if(result && Object.keys(result).length){
                            cb()
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }
                    }
                })


            }else{
                cb('Image is required.')
            }

        }]
    },function(err,result){
        callback(err,imageData);
    })

   
}

module.exports = {
    companySignup : companySignup,
    verifyOTP:verifyOTP,
    completeProfile:completeProfile,
    emailLogin:emailLogin,
    addMaterial:addMaterial,
    listMaterial:listMaterial,
    addContract:addContract,
    listContracts:listContracts,
    addBooking:addBooking,
    globalTruck:globalTruck,
    bookingListing:bookingListing,
    listContractsAdmin:listContractsAdmin,
    getCompaniesList:getCompaniesList,
    editContract:editContract,
    updateCompanyBlockStatus:updateCompanyBlockStatus,
    editMaterial:editMaterial,
    editProfilePic:editProfilePic
    
};
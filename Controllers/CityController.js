'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');
var Config = require('../Config');

var UploadManager = require('../Lib/UploadManager');
var TokenManager = require('../Lib/TokenManager');
var NotificationManager = require('../Lib/NotificationManager');
var mongoose  = require('mongoose')


var addCity=function(payloadData,callback){
    async.auto({
        checkDuplicate:function(cb){
            var criteria={
                cityName:{ $regex: new RegExp("^" + payloadData.cityName+"$")},
                isDeleted:false
            }
            
            var projection={}
            var options={}

            Service.CityService.getCity(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE)
                    }else{
                        cb(null)
                    }
                }
            });
        },
        insertCity:['checkDuplicate',function(cb){
            var object={
                cityName:payloadData.cityName,
                location:[payloadData.long,payloadData.lat]
            }

            Service.CityService.createCity(object,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(Object.keys(result).length){
                        cb(null)
                    }else{
                        cb(null)
                    }
                }
            })
        }]
    },function(err,result){
        callback(err,{});
    })
}


var addLocation=function(payloadData,callback){
    async.auto({
        checkDuplicate:function(cb){
            var criteria={
                companyId:payloadData.userId,
                pointName:{ $regex: new RegExp("^" + payloadData.pointName+"$")},
                address:{$regex:new RegExp("^" + payloadData.address+"$")},
                cityId:payloadData.cityId,
                isDeleted:false
            }
            
            var projection={}
            var options={}

            Service.LocationService.getLocation(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE)
                    }else{
                        cb(null)
                    }
                }
            });
        },
        checkCityId:function(cb){
             var criteria={
                _id:payloadData.cityId,
                isDeleted:false
            }
            
            var projection={}
            var options={}

            Service.CityService.getCity(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(!result.length){
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CITY_ID)
                    }else{
                        cb(null)
                    }
                }
            });
        },
        insertLocation:['checkDuplicate','checkCityId',function(cb){
            var object={
                pointName:payloadData.pointName,
                location:[payloadData.long,payloadData.lat],
                address:payloadData.address,
                cityId:payloadData.cityId,
                 companyId:payloadData.userId,
                 authPerson:payloadData.authPerson,
                 phoneNumber:payloadData.phoneNo
            }

            Service.LocationService.createLocation(object,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(Object.keys(result).length){
                        cb(null)
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        }]
    },function(err,result){
        callback(err,{});
    })
}


var editLocation=function(payloadData,callback){
    async.auto({
        checkLocationId:function(){
             var criteria={
                _id:payloadData.id,
                isDeleted:false
            }
            
            var projection={}
            var options={}

            Service.LocationService.getLocation(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(!result.length){
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_ID)
                    }else{
                        cb(null)
                    }
                }
            });
        },
        checkDuplicate:['checkLocationId',function(cb){
            var criteria={
                _id:{$ne:payloadData.id},
                pointName:{ $regex: new RegExp("^" + payloadData.pointName+"$")},
                address:{$regex:new RegExp("^" + payloadData.address+"$")},
                city:{$regex:new RegExp("^" + payloadData.city+"$")},
                state:{$regex:new RegExp("^" + payloadData.state+"$")},
                pincode:payloadData.pincode,
                cityId:payloadData.cityId,
                companyId:payloadData.userId,
                isDeleted:false
            }
            
            var projection={}
            var options={}

            Service.LocationService.getLocation(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE)
                    }else{
                        cb(null)
                    }
                }
            });
        }],
        checkCityId:function(cb){
             var criteria={
                _id:payloadData.cityId,
                isDeleted:false
            }
            
            var projection={}
            var options={}

            Service.CityService.getCity(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(!result.length){
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CITY_ID)
                    }else{
                        cb(null)
                    }
                }
            });
        },
        editLocation:['checkDuplicate','checkCityId',function(cb){

            var criteria={
                _id:payloadData.id,
                isDeleted:false
            }


            var object={
                pointName:payloadData.pointName,
                location:[payloadData.long,payloadData.lat],
                address:payloadData.address,
                city:payloadData.city,
                state:payloadData.state,
                pincode:payloadData.pincode,
                cityId:payloadData.cityId,
                companyId:payloadData.userId
            }

            var setQuery={
                $set:object
            }

            var options={
                new:true
            }

            Service.LocationService.updateLocation(criteria,setQuery,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result && Object.keys(result).length){
                        cb(null)
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        }]
    },function(err,result){
        callback(err,{});
    })
}


var getCities=function(queryData,callback){
    var criteria={
        isDeleted:false
    }

    if(queryData.status=='NOT ASSIGNED'){
        criteria.assignedTo=[];
    }


    var projection={
        _id:1,
        cityName:1,
        isBlocked:1,
        location:1
    }

    var options={
        lean:true
    }

    Service.CityService.getCity(criteria,projection,options,function(err,result){
        callback(err,result)
    })
}



var getLocationsAdmin=function(queryData,callback){

    let loaderData=[];
    let response=[];
    let count=0;

   
    async.auto({
        checkCityId:function(cb){
            if(queryData.cityId){
               var query={
                   _id:queryData.cityId,
                   isDeleted:false
               }

               Service.CityService.getCityCount(query,function(err,result){
                   if(err){
                       cb(err)
                   }else{
                       if(result){
                           cb()
                       }else{
                           cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CITY_ID)
                       }
                   }
               })

            }else{
                cb()
            }
        },
         checkCompanyId:function(cb){
            if(queryData.companyId){
               var query={
                   _id:queryData.companyId,
                   isDeleted:false
               }

               Service.CompanyService.getCompaniesCount(query,function(err,result){
                   if(err){
                       cb(err)
                   }else{
                       if(result){
                           cb()
                       }else{
                           cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COMPANY_ID)
                       }
                   }
               })

            }else{
                cb()
            }
        },
          checkLoaderId:function(cb){
            if(queryData.loaderId){
               var query={
                   _id:queryData.loaderId,
                   isDeleted:false
               }

               var projection={
                   assignedLocations:1
               }

               var options={
                   lean:true
               }
               Service.LoaderService.getLoader(query,projection,options,function(err,result){
                   if(err){
                       cb(err)
                   }else{
                       if(result && result.length){
                           //loaderData=result.length && result[0].assignedLocations || []
                           cb()
                       }else{
                           cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_LOADER_ID)
                       }
                   }
               })

            }else{
                cb()
            }
        },
        checkAssignedLocations:function(cb){
            if('assigned' in queryData && queryData.loaderId && queryData.assigned){
               var query={
                   isDeleted:false
               }

               var projection={
                   assignedLocations:1
               }

               var options={
                   lean:true
               }
               Service.LoaderService.getLoader(query,projection,options,function(err,result){
                   if(err){
                       cb(err)
                   }else{
                       if(result && result.length){
                          // loaderData=result.length && result[0].assignedLocations || [];
                           loaderData=result.reduce(function(prev,curr){
                               return prev.concat(curr.assignedLocations)
                           },[])
                           console.log('mmmmmmmmmmmmmmmmmmmmmmm',loaderData);
                       }
                       cb()
                   }
               })

            }else{
                cb()
            }
        },
        getCounts:['checkCityId','checkLoaderId','checkCompanyId','checkAssignedLocations',function(cb){
             var criteria={
                 isDeleted:false
            }

            if(queryData.companyId){
                criteria.companyId=queryData.companyId
            }
            else if(queryData.loaderId){
                if('assigned' in queryData ){
                    if(queryData.assigned)
                        criteria._id={$in:loaderData}
                    else
                        criteria._id={$nin:loaderData}

                }
            }else{
                if(queryData.cityId){
                    criteria.cityId=queryData.cityId
                }
            }


            Service.LocationService.getLocationsCount(criteria,function(err,result){
                if(err){
                    cb(err)
                }else{
                    count = result || 0
                    cb()
                }
            })
        }],
        getRecords:['checkCityId','checkLoaderId','checkCompanyId','checkAssignedLocations',function(cb){
              var criteria={
                 isDeleted:false
            }

            if(queryData.companyId){
                criteria.companyId=queryData.companyId
            }
            else if(queryData.loaderId){
                if('assigned' in queryData ){
                    if(queryData.assigned)
                        criteria._id={$in:loaderData}
                    else
                        criteria._id={$nin:loaderData}

                }
            }else{
                if(queryData.cityId){
                    criteria.cityId=queryData.cityId
                }
            }

            var projection={
                pointName:1,
                location:1,
                address:1,
                cityId:1,
                companyId:1,
                isBlocked:1,
                authPerson:1,
                phoneNumber:1
            }

            var options={
                lean:true,
                skip:queryData.skip,
                limit:queryData.limit
            }

            var populate=[
                {
                    path: 'cityId',
                    model:'City',
                    match: {},
                    select: {
                        cityName:1,
                        location:1
                    },
                    options: {
                        lean:true
                    }
                },
                 {
                    path: 'companyId',
                    model:'Company',
                    match: {},
                    select: {
                        name:1,
                        email:1,
                        countryCode:1,
                        phoneNumber:1
                    },
                    options: {
                        lean:true
                    }
                }
            ]

            Service.LocationService.getLocationPopulate(criteria,projection,options,populate,function(err,result){
                if(err){
                    cb(err)
                }else{
                    response=result;
                    cb()
                }
            })
        }]
    },function(err){
        callback(err,{count:count,results:response})
    })

}



var getLocations=function(queryData,callback){

    let count=0;
    let contractData;
    let responseObject={};

    async.auto({
        getContractData:function(cb){
            if(queryData.contractId){
                var criteria={
                    _id:queryData.contractId,
                    companyId:queryData.userId,
                    isDeleted:false,
                    isBlocked:false
                }

                var projection={}
                var options={
                    lean:true
                }

                Service.ContractsService.getContract(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result.length){
                            contractData=result[0];
                            cb()
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CONTRACT_ID)
                        }
                    }
                })

            }else{
                cb()
            }
        },
        getCountsFromDb:['getContractData',function(cb){
            if(!queryData.contractId){
                var criteria={
                    isDeleted:false,
                    isBlocked:false,
                    companyId:queryData.userId
                }


                Service.LocationService.getLocationsCount(criteria,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        count=result;
                        cb()
                    }
                })

            }else{
                cb()
            }

        }],
        getResults:['getContractData',function(cb){
            if(!queryData.contractId){
                var criteria={
                    isDeleted:false,
                    isBlocked:false,
                    companyId:queryData.userId
                }

                var projection={
                    isDeleted:0
                }

                var options={
                    lean:true,
                    skip:queryData.skip || 0,
                    limit:queryData.limit || 0
                }


            var populate = [
                {
                    path: 'cityId',
                    match: {},
                    select: {
                        _id:1,
                        cityName:1,
                        location:1
                    },
                    options: {}
                }
                ]


                Service.LocationService.getLocationPopulate(criteria,projection,options,populate,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        responseObject={
                            count:count,
                            results:result
                        }
                        cb();
                    }
                })



            }else{

                let aggregateArray=[
                    {
                        $match:{
                            $or:[
                                {cityId:mongoose.Types.ObjectId(contractData.startCityId)},
                                {cityId:mongoose.Types.ObjectId(contractData.endCityId)}
                            ],
                            companyId:mongoose.Types.ObjectId(queryData.userId)
                        }
                    },
                    {
                        $group:{
                            _id:null,
                            pickUpLocations:{$push:{ $cond: { 
                                if: {$eq:['$cityId',mongoose.Types.ObjectId(contractData.startCityId)]},
                                then:{
                                    _id:'$_id',
                                    pointName:'$pointName',
                                    location:'$location',
                                    address:'$address',
                                    authPerson:'$authPerson',
                                    phoneNumber:'$phoneNumber'
                                },
                                else: null} }},
                             dropLocations:{$push:{ $cond: { 
                                if: {$eq:['$cityId',mongoose.Types.ObjectId(contractData.endCityId)]},
                                then:{
                                    _id:'$_id',
                                    pointName:'$pointName',
                                    location:'$location',
                                    address:'$address',
                                    authPerson:'$authPerson',
                                    phoneNumber:'$phoneNumber'
                                },
                                else: null} }}
                        }
                    },
                    {
                        $project:{
                            pickUpLocations:{ 
                                $filter: { 
                                    input: '$pickUpLocations', 
                                    as: 'p',
                                    cond: {$ne:['$$p',null]} } },
                            dropLocations:{ 
                                $filter: { 
                                    input: '$dropLocations', 
                                    as: 'p',
                                    cond: {$ne:['$$p',null]} } },
                            _id:0
                        }
                    }
                ]
                Service.LocationService.getLocationAggregated(aggregateArray,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                         if(result.length){
                        responseObject=result[0]
                    }else{
                        responseObject={
                            pickUpLocations:[],
                            dropLocations:[]
                        }
                    }

                    cb()
                    }
                   
                })

            }
        }]
    },function(err,result){
        callback(err,responseObject)
    })

}



var assignLocationToLoader=function(payloadData,callback){
    console.log('PPPPPPPPPPPPP',payloadData)
    var criteria={
        _id:payloadData.loaderId,
        isDeleted:false
    }

    var setQuery={
        $addToSet:{
            assignedLocations:{$each:payloadData.locations}
        }
    }

    var options={
        new:true
    }

    Service.LoaderService.updateLoader(criteria,setQuery,options,function(err,result){
        if(err){
            callback(err)
        }else{
            if(result && Object.keys(result).length){
                callback(null,{})
            }else{
                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }
    })
}




var removeLoaderLocation=function(payloadData,callback){
    async.auto({
        checkLocationId:function(cb){
            var criteria={
                _id:payloadData.locationId,
                isDeleted:false
            }

            var projection={}
            var options={}

            Service.LocationService.getLocation(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        cb(null)
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_LOCATION_ID)
                    }
                }
            })
        },
        checkLoaderId:function(cb){
              var criteria={
                _id:payloadData.loaderId,
                isDeleted:false
            }

            var projection={}
            var options={}

            Service.LoaderService.getLoader(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        cb(null)
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_LOADER_ID)
                    }
                }
            })
        },
        removeCityOfLoader:['checkLoaderId','checkLocationId',function(cb){
            var criteria={
                _id:payloadData.loaderId,
                isDeleted:false
            }

            var setQuery={
                $pull:{
                    assignedLocations:payloadData.locationId
                }
            }

            var options={
                new:true
            }

            Service.LoaderService.updateLoader(criteria,setQuery,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result && Object.keys(result).length){
                        cb(null)
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        }]
    },function(err,result){
        callback(err,{});
    })
}

module.exports={
    addCity:addCity,
    getCities:getCities,
    assignLocationToLoader:assignLocationToLoader,
   removeLoaderLocation:removeLoaderLocation,
   addLocation:addLocation,
   editLocation:editLocation,
    getLocations:getLocations,
    getLocationsAdmin:getLocationsAdmin
}
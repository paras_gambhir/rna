/**
 * Created by paras
 */
'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');
var UploadManager = require('../Lib/UploadManager');
var TokenManager = require('../Lib/TokenManager');
var Config = require('../Config');
var NotificationManager = require('../Lib/NotificationManager');
var distance = require('google-distance');
var emailPassword = require('../Lib/email');
var appConstants = require('../Config/appConstants');


var stripe = require("stripe")(
    appConstants.SERVER.STRIPE_KEY
);


var _ = require('lodash');

var moment = require('moment');

var Models = require('../Models');



var driverLogin = function (payloadData, callback) {

    var driverData;
    var driverId;
    var accessToken;
    async.auto({

        checkForUserExists: function (cb) {

               var criteria = {
                    countryCode: payloadData.countryCode,
                    phoneNumber: payloadData.phoneNumber
                };


            var projection = {};
            var option = {
                lean: true
            };
            Service.DriverService.getDriver(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {

                        var cryptedPassword = UniversalFunctions.CryptData(payloadData.password);

                        if (cryptedPassword == result[0].password) {
                            if (result[0].approvedByAdmin) {
                                driverId = result[0]._id;
                                driverData = result[0];
                                cb();
                            }
                            else {
                                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_APPROVED);
                            }

                        }
                        else {
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_PASSWORD);
                        }

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACCOUNT_NOT_REGISTERED);
                    }
                }
            })

        },
        updateDriverParams: ['checkForUserExists', function (cb) {
            var criteria = {
                _id: driverId
            };
            var setQuery = {
                deviceType: payloadData.deviceType ? payloadData.deviceType : driverData.deviceType,
                deviceToken: payloadData.deviceToken ? payloadData.deviceToken : driverData.deviceToken,
                currentLocation: [payloadData.latitude ? payloadData.latitude : driverData.latitude,payloadData.longitude ? payloadData.longitude: driverData.longitude],
                isAvailable: true
            };
            Service.DriverService.updateDriver(criteria, setQuery, {
                new: true,
                upsert: true
            }, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    driverData = dataAry;
                    cb();
                }
            });

        }],
        setToken: ['updateDriverParams', function (cb) {
            var tokenData = {
                id: driverId,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        driverData.accessToken = accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })

        }]

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {driverData: driverData});
        }

    })
};


var OTPLogin = function(payloadData,callback)
{
    var driverData;
    var driverId;
    var accessToken;
    async.auto({

        checkForUserExists: function (cb) {

            var criteria = {
                countryCode: payloadData.countryCode,
                phoneNumber: payloadData.phoneNumber
            };


            var projection = {};
            var option = {
                lean: true
            };
            Service.DriverService.getDriver(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {


                        if (payloadData.OTP == result[0].OTP) {
                            driverId = result[0]._id;
                            driverData = result[0];
                            cb();

                        }else {
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_OTP);
                        }

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACCOUNT_NOT_REGISTERED);
                    }
                }
            })

        },
        updateDriverParams: ['checkForUserExists', function (cb) {
            var criteria = {
                _id: driverId
            };
            var setQuery = {
                deviceType: payloadData.deviceType ? payloadData.deviceType : driverData.deviceType,
                deviceToken: payloadData.deviceToken ? payloadData.deviceToken : driverData.deviceToken,
                currentLocation: [payloadData.latitude ? payloadData.latitude : driverData.latitude,payloadData.longitude ? payloadData.longitude: driverData.longitude],
                isAvailable: true

            };
            Service.DriverService.updateDriver(criteria, setQuery, {
                new: true,
                upsert: true
            }, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    driverData = dataAry;
                    cb();
                }
            });

        }],
        setToken: ['updateDriverParams', function (cb) {
            var tokenData = {
                id: driverId,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        driverData.accessToken = accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })

        }]

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {driverData: driverData});
        }

    })
};


var sendOTP = function(payloadData,callback)
{
    var OTP;
    var driverId;

    async.auto({

        checkForUserExists: function (cb) {

            var criteria = {
                countryCode: payloadData.countryCode,
                phoneNumber: payloadData.phoneNumber
            };


            var projection = {};
            var option = {
                lean: true
            };
            Service.DriverService.getDriver(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {

                     OTP = UniversalFunctions.generateOTP();

                    if (result.length) {
                        driverId =  result[0]._id;
                        NotificationManager.sendSMSToUser(OTP, payloadData.countryCode, payloadData.phoneNumber,function(err,result)
                        {
                            cb();
                        })
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACCOUNT_NOT_REGISTERED);
                    }
                }
            })

        },
        saveOTP:['checkForUserExists',function (cb) {
            var dataToSet = {
                OTP: OTP

            };

            var criteria = {
                _id: driverId
            };
            Service.DriverService.updateDriver(criteria, dataToSet,function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    cb();
                }
            })
        }]

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {OTP: OTP});
        }

    })
};


var accessTokenLogin = function (payloadData, callback) {

    var driverData;
    var driverId;
    var accessToken;
    async.auto({

        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.DriverService.getDriver(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        driverId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })

        },
        updateDriverParams: ['checkForUserExists', function (cb) {
            var criteria = {
                _id: driverId
            };
            var setQuery = {
                deviceType: payloadData.deviceType,
                deviceToken: payloadData.deviceToken,
                latitude: payloadData.latitude,
                longitude: payloadData.longitude,
                currentLocation: [payloadData.latitude, payloadData.longitude],
                isAvailable: true

            };
            Service.DriverService.updateDriver(criteria, setQuery, {
                new: true,
                upsert: true
            }, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    driverData = dataAry;
                    cb();
                }
            });


        }],
        setToken: ['updateDriverParams', function (cb) {
            var tokenData = {
                id: driverId,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        driverData.accessToken = accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })

        }]

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {driverData: driverData});
        }

    })
};


var driverLogout = function (payloadData, callback) {

    var driverId;
    async.auto({

        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.DriverService.getDriver(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        driverId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })

        },
        updateDriverParams: ['checkForUserExists', function (cb) {
            var criteria = {
                _id: driverId
            };
            var setQuery = {
                deviceToken: null,
                accessToken: null,
                isAvailable: false
            };
            Service.DriverService.updateDriver(criteria, setQuery, {
                new: true,
                upsert: true
            }, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });


        }]

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {});
        }

    })
};


var updateDriverLocation = function (payloadData, callback) {

    var driverId;
    async.auto({

        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.DriverService.getDriver(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        driverId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })

        },
        "updateDriver": ['checkForUserExists',function (cb) {
            var criteria = {
                _id: driverId,
                
            };

            var setQuery = {
                currentLocation: payloadData.location
            };

            var options = {
                new: true

            };
            Service.DriverService.updateDriver(criteria, setQuery, options, function (err, result) {
                if (err) {
                    console.log(err);
                    cb(err)
                }
                else {
                    cb();
                }
            })
        }],
        updateBooking:['checkForUserExists',function(cb)
        {
            if(payloadData.bookingId)
            {
                var criteria = {
                    bookingId: payloadData.bookingId
                };
                var setQuery = {
                    $push:{
                        locationLogs: payloadData.location
                    }
                };

                var options = {
                    new: true

                };
                Service.BookingService.updateBookings(criteria, setQuery, options, function (err, result) {
                    if (err) {
                        console.log(err);
                        cb(err)
                    }
                    else {
                        cb();
                    }
                })
            }
            else{
                cb();
            }
        }]
    }, function (err, result) {
        if (err) {
            console.log(err);
            callback(err)
        }
        else {
            callback(null,{});
        }

    })

};


var acceptRide = function (payloadData, callback) {
    var driverId;
    var data = [];
    var loading;
    var current;
    var name;
    var phoneNo;
    var profilePic;
    var bookingData;
    var truckId;
    let truckNo;
    async.auto({

        getDriver: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var options = {
                lean: true
            };

            Service.DriverService.getDriver(criteria, projection, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                    if (result.length) {
                        current = result[0].currentLocation;
                        name = result[0].name;
                        phoneNo = result[0].countryCode + "" + result[0].phoneNumber;
                        driverId = result[0]._id;
                        profilePic = result[0].profilePicURL.thumbnail;
                        truckId=result[0].VendorTruck;
                        cb();

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        },
        getBooking: ['getDriver', function (cb) {
            var criteria = {
                bookingId: payloadData.bookingId
            };
            var projection = {
                _id:1,
                pickupLocationId:1,
                dropOffLocationId:1,
                contractId:1,
                status:1
            };
            var options = {
                lean: true
            };

            var populate = [
                 {
                    path: 'contractId',
                    match: {
                        
                    },
                    select: {
                        truckType:1,
                        freightAmount:1,
                        detentionCharges:1,
                        billingPeriod:1,
                        startCityId:1,
                        endCityId:1,
                        truckId:1
                    },
                    options: {}
                },
                 {
                    path: 'pickupLocationId',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1
                    },
                    options: {
                        lean:true
                    }
                },
                 {
                    path: 'dropOffLocationId',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1
                    },
                    options: {
                        lean:true
                    }
                }
            ];

            Service.BookingService.getBookingsPopulateDriverUpdateRide(criteria, projection, options, populate, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    if(result.length){
                        // if(result[0].status!=Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.PENDING){
                        //     cb('Sorry booking is already accepted by another driver.')
                        // }else{
                        //     data=result[0]
                        //     cb()
                        // }

                         data=result[0]
                            cb()
                    }else{
                        cb("Invalid booking id.")
                    }
                }
            })
        }],
        updateDriver: ['getBooking', function (cb) {
            var criteria = {
                _id: driverId
            };
            var dataToUpdate;

            dataToUpdate = {
                isAvailable: false,
                currentBooking:true
            };


            var options = {
                lean: true
            };

            Service.DriverService.updateDriver(criteria, dataToUpdate, options, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    cb(null)
                }
            })
        }],
         updateVendorTruckStatus: ['getBooking', function (cb) {
            var criteria = {
                _id: truckId
            };
            var dataToUpdate;

            dataToUpdate = {
                status:Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.BOOKED
            };


            var options = {
                lean: true
            };

            Service.VendorTruckService.updateVendorTruck(criteria, dataToUpdate, options, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    truckNo=result && result.truckNo || ''
                    cb(null)
                }
            })
        }],
        updateRide: ['updateVendorTruckStatus', function (cb) {

            var criteria = {
                bookingId: payloadData.bookingId
            };
            var dataToUpdate = {
                driverId: driverId,
                status: Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.ACCEPTED,
                acceptTime: parseInt(+new Date()+19800000),
                truckId:truckId,
                truckNo:truckNo
            };
            var options = {
                new: true,
                lean:true
            };
            Service.BookingService.updateBookings(criteria, dataToUpdate, options, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    bookingData = result;
                    cb(null)
                }
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            data.status=Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.ACCEPTED;
            callback(null,{bookingData:data})
        }
    })

};

var sendMessage = function (payloadData, callback) {
    var driverId;
    var vendorId;
    async.auto({

        getDriver: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var options = {
                lean: true
            };

            Service.DriverService.getDriver(criteria, projection, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                    if (result.length) {
                        driverId=result[0]._id;
                        vendorId=result[0].vendorId;
                        cb();

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        },
        insertMessage: ['getDriver', function (cb) {
            var insertObject = {
               driverId:driverId,
               message:payloadData.message,
               vendorId:vendorId
            };
            
            Service.DriverService.createMessage(insertObject, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    if(result && Object.keys(result).length){
                            cb()
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null,{})
        }
    })

};


var rejectRide = function (payloadData, callback) {
    var driverId;
    var data = [];
    var loading;
    var current;
    var name;
    var phoneNo;
    var profilePic;

    var driverIds;

    async.auto({

        getDriver: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var options = {
                lean: true
            };

            Service.DriverService.getDriver(criteria, projection, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                    if (result.length) {
                        current = result[0].currentLocation;
                        name = result[0].name;
                        phoneNo = result[0].countryCode + "" + result[0].phoneNumber;
                        driverId = result[0]._id;
                        profilePic = result[0].profilePicURL.thumbnail;
                        cb();

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        },
        getBooking: ['getDriver', function (cb) {
            var criteria = {
                bookingId: payloadData.bookingId
            };
            var projection = {
                companyId: 1,
                _id: 1,
                driverIds:1
            };
            var options = {
                lean: true
            };
            var populate = [
                {
                    path: 'companyId',
                    match: {},
                    select: "name",
                    options: {}
                }
            ];

            Service.BookingService.getBookingsPopulate(criteria, projection, options, populate, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    data = result[0];
                    loading = result[0].pickUpLocation;
                    driverIds=result[0].driverIds;
                    cb(null)
                }
            })
        }],
        updateDriver: ['getBooking', function (cb) {
            var criteria = {
                _id: driverId
            };
            var dataToUpdate;

            dataToUpdate = {
                isAvailable: true
            };


            var options = {
                lean: true
            };

            Service.DriverService.updateDriver(criteria, dataToUpdate, options, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    cb(null)
                }
            })
        }],
        updateRide: ['updateDriver', function (cb) {

            var rejection = {
                driverId: driverId,
                rejectionReason: payloadData.rejectionReason,
                rejectionTime: parseInt(+new Date()+19800000)
            };


            var criteria = {
                bookingId: payloadData.bookingId
            };
            var dataToUpdate = {
                driverId: driverId,
                status:driverIds.length==1 && Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.REJECTED ||  Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.PENDING,
                $push: {
                    rejection: rejection
                },
                $pull:{
                    driverIds:driverId
                }
            };
            var options = {
                new: true
            };
            Service.BookingService.updateBookings(criteria, dataToUpdate, options, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    cb(null)
                }
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null,{})
        }
    })

};





var reachedLoadingLocation = function(payloadData,callback)
{
    var driverId;
    var name;
    var deviceToken;
    let location;
    let authPerNo;
    let contractId;
    let truckId;
    let truckNo;
    let truckType;
    let startLocation;
    let endLocation;
    async.auto({

        getDriver: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var options = {
                lean: true
            };

            Service.DriverService.getDriver(criteria, projection, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                    if (result.length) {
                        driverId = result[0]._id;
                        name=result[0].name;
                        cb();

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        },
        updateBooking: ['getDriver', function (cb) {

            var criteria = {
                bookingId: payloadData.bookingId,
                pickupLocationId: payloadData.loadingLocationId
            };
            var setQuery = {
                loadLocationReachTime: parseInt(+new Date()+19800000),
                status:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.LOADING_LOCATION
            };
            var options = {
                new: true
            };

            Service.BookingService.updateBookings(criteria,setQuery,options,function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    if(result && Object.keys(result).length){
                        contractId=result.contractId;
                        truckId= result.truckId;
                        truckNo= result.truckNo;
                        truckType=result.truckType;
                        startLocation= result.pickupAddress;
                        endLocation= result.dropOffAddress;
                        cb(null)
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                    }
                    
                }
            })
        }],
        getLoaderData:['updateBooking',function(cb){
            var criteria={
                isDeleted:false,
                isBlocked:false,
                assignedLocations:{$elemMatch:{$eq:payloadData.loadingLocationId}}
            }

            var projection={
                deviceToken:1,
                assignedLocations:{$elemMatch:{$eq:payloadData.loadingLocationId}}
            }

            var options={
                lean:true
            }

            var populate=[
                {
                    path: 'assignedLocations',
                    model:'Location',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1
                    },
                    options: {
                        lean:true
                    }
                },
            ]

            Service.LoaderService.getLoaderPopulate(criteria,projection,options,populate,function(err,result){
                if(err){
                    cb(err)
                }else{
                    console.log('rrrrrrrrrrrrrrrrr',result)

                    if(result.length){
                         deviceToken=result[0].deviceToken;
                    location=result[0].assignedLocations && result[0].assignedLocations.length && result[0].assignedLocations[0].pointName || ''
                    console.log('000000000000',location)
                    }
                   
                    cb()
                }
            })
        }],
        getAuthPerNo:['getLoaderData',function(cb){
            var criteria={
                _id:payloadData.loadingLocationId
            }

            var projection={
                phoneNumber:1
            }

            var options={
                lean:true
            }
            Service.LocationService.getLocation(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    console.log('rrrrrrrrrrrrrrrrr',result)

                    if(result.length){
                         authPerNo=result && result.length && result[0].phoneNumber;
                         cb()
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        }],
        getVendorTruckData:['updateBooking',function(cb){
            let criteria={
                _id:truckId
            }

            let projection={
                truckNo:1
            }
            let options={
                lean:true
            }

            Service.VendorTruckService.getVendorTruck(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result && result.length){
                        truckNo=result[0].truckNo;
                        cb()
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        }],
        getContractData:['updateBooking',function(cb){
            let criteria={
                _id:contractId
            }

            let projection={
                truckNo:1
            }
            let options={
                lean:true
            }

            Service.ContractsService.getContract(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result && result.length){
                        truckNo=result[0].truckNo;
                        cb()
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        }],
         sendNotification:['getLoaderData',function (cb) {
            if(deviceToken){
                var temp = {};
                temp.pushType=0;
                temp.bookingId = payloadData.bookingId;
                temp.title = "Driver "+ name + " is reached on pickup location "+location+" .";
                var title =  'DRIVER ARRIVED';
                NotificationManager.sendAndroidPushNotificationForLoader( deviceToken, temp, title, function (err) {
                    if (err) {
                        console.log('ERRRRRRRRRRRRRRR',err);
                        cb(null)
                    } else {
                        cb(null)
                    }
                })
            }else{
                cb()
            }
        }],
        sendSMS:['getAuthPerNo',function(cb){
            let message= "Driver "+ name + " is reached on pickup location "+location+" for booking no "+payloadData.bookingId+" from "+startLocation+" to "+endLocation+" on truck "+truckType+" ("+truckNo+")." 
            NotificationManager.sendSMSToLoader(message, null, authPerNo,function(err,result)
            {
                cb(err)
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null,{})
        }
    })

};

var updateBookingStatus = function(payloadData,callback)
{
    var driverId;
    var name;
    var deviceToken;
    var location;
    var locationId;

    async.auto({

        getDriver: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var options = {
                lean: true
            };

            Service.DriverService.getDriver(criteria, projection, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                    if (result.length) {
                        driverId = result[0]._id;
                        name=result[0].name;
                        cb();

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        },
        updateBooking: ['getDriver', function (cb) {
            var arriveTime = new Date();

            var criteria = {
                bookingId: payloadData.bookingId
            };

           
            var setQuery = {
                status:payloadData.status
            };

            if(payloadData.status==Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.UNLOADING_LOCATION){
                criteria.status=Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.STARTED
                 setQuery.unloadLocationReachTime=new Date();
            }else{
                if(payloadData.status==Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.ENDED){
                    criteria.status=Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.UNLOADING_LOCATION
                    setQuery.endTime=new Date();
                }
            }



            var options = {
                new: true
            };

            Service.BookingService.updateBookings(criteria,setQuery,options,function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    if(result && Object.keys(result).length){
                        locationId=result.dropOffLocationId || null
                        cb(null)
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_OPERATION);
                    }
                    
                }
            })
        }],
        getLoaderData:['updateBooking',function(cb){

            if(payloadData.status==Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.UNLOADING_LOCATION && locationId){

                    var criteria={
                        isDeleted:false,
                        isBlocked:false,
                        assignedLocations:{$elemMatch:{$eq:locationId}}
                    }

                    var projection={
                        deviceToken:1,
                        assignedLocations:{$elemMatch:{$eq:locationId}}
                    }

                    var options={
                        lean:true
                    }

                    var populate=[
                        {
                            path: 'assignedLocations',
                            model:'Location',
                            match: {},
                            select: {
                                pointName:1,
                                location:1,
                                address:1
                            },
                            options: {
                                lean:true
                            }
                        },
                    ]

                    Service.LoaderService.getLoaderPopulate(criteria,projection,options,populate,function(err,result){
                        if(err){
                            cb(err)
                        }else{
                            console.log('rrrrrrrrrrrrrrrrr',result)

                            if(result.length){
                                deviceToken=result[0].deviceToken;
                            location=result[0].assignedLocations && result[0].assignedLocations.length && result[0].assignedLocations[0].pointName || ''
                            }
                        
                            cb()
                        }
                    })
            }else{
                cb()
            }
        }],
         sendNotification:['getLoaderData',function (cb) {
            if(deviceToken){
                var temp = {};
                temp.pushType=0;
                temp.bookingId = payloadData.bookingId;
                temp.title = "Driver "+ name + " is reached on dropoff location "+location+" .";
                var title =  'DRIVER ARRIVED';
                sendPushNotification('ANDROID', deviceToken, temp, title, function (err) {
                    if (err) {
                        console.log('ERRRRRRRRRRRRRRR',err);
                        cb(null)
                    } else {
                        cb(null)
                    }
                })
            }else{
                cb()
            }
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null,{})
        }
    })

};



var endRide = function(payloadData,callback)
{
    var driverId;
    var truckId;
    var contractId;
    var endCityId;
    var name;
    var freightAmount;
    var detentionCharges;
    var companyId;
    var bookingId;

    async.auto({

        getDriver: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var options = {
                lean: true
            };

            Service.DriverService.getDriver(criteria, projection, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                    if (result.length) {
                        driverId = result[0]._id;
                        name=result[0].name;
                        cb();

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        },
        checkOtp:['getDriver',function(cb){
            var criteria = {
                _id: payloadData.bookingId,
                OTP:payloadData.OTP
            };


            Service.BookingService.getBookingCount(criteria,function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    if(result){
                        cb(null)
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_OTP);
                    }
                    
                }
            })
        }],
        updateBooking: ['checkOtp', function (cb) {
            var criteria = {
                _id: payloadData.bookingId
            };

           
            var setQuery = {
                status:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.ENDED,
                endTime:parseInt(+new Date()+19800000),
                otpVerified:true,
                driverAmount:0,
                driverDate:parseInt(+new Date()+19800000),
                companyAmount:0,
                companyDate:parseInt(+new Date()+19800000),
            };


            var options = {
                new: true
            };

            Service.BookingService.updateBookings(criteria,setQuery,options,function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    if(result && Object.keys(result).length){
                        truckId=result.truckId;
                        contractId=result.contractId;
                        companyId=result.companyId;
                        bookingId=result.bookingId;
                        cb(null)
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_OPERATION);
                    }
                    
                }
            })
        }],
        updateDriver:['updateBooking',function(cb){
            var criteria={
                _id:driverId
            }

            var setQuery={
                isAvailable:true,
                currentBooking:false
            }

            var option={
                new:true,
                lean:true
            }

            Service.DriverService.updateDriver(criteria,setQuery,option,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result && Object.keys(result).length){
                        cb()
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        }],
        getContractDetails:['updateBooking',function(cb){
            var criteria={
                _id:contractId
            }

            var projection={
                endCityId:1,
                freightAmount:1,
                detentionCharges:1
            }

            var option={
                lean:true
            }

            Service.ContractsService.getContract(criteria,projection,option,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result && result.length){
                        console.log('===============',result[0])
                        endCityId=result[0].endCityId;
                        freightAmount=result[0].freightAmount;
                        detentionCharges=result[0].detentionCharges;
                        cb()
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CONTRACT_ID)
                    }
                }
            })
        }],
        updateVendorTruck:['getContractDetails',function(cb){
            var criteria={
                _id:truckId
            }

            var setQuery={
                status:Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.VACANT,
                currentLocation:endCityId || undefined
            }

            var option={
                new:true,
                lean:true
            }

            Service.VendorTruckService.updateVendorTruck(criteria,setQuery,option,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result && Object.keys(result).length){
                        cb()
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        }],
        addDriverEntryToAccount:['updateVendorTruck',function(cb){
            var object={
                bookingId:bookingId,
                type:'DRIVER',
                detentionCharges:detentionCharges,
                amount:0,
                date:+new Date(),
                receiverId:driverId
            }

            Service.AccountService.createAccount(object,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result && Object.keys(result).length){
                        cb();
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        }],
        addCompanyEntryToAccount:['updateVendorTruck',function(cb){
            var object={
                bookingId:bookingId,
                type:'COMPANY',
                freightAmount:freightAmount,
                amount:0,
                date:+new Date(),
                receiverId:companyId
            }

            Service.AccountService.createAccount(object,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result && Object.keys(result).length){
                        cb();
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null,{})
        }
    })

};



var startRide = function (payloadData, callback) {

    var driverId;
    var data;
    async.auto({

        getDriver: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var options = {
                lean: true
            };

            Service.DriverService.getDriver(criteria, projection, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                    if (result.length) {
                        driverId = result[0]._id;
                        cb();

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        },
        updateBooking: ['getDriver',function (cb) {

            var criteria = {
                bookingId: payloadData.bookingId
            };
            var dataToUpdate = {
                status: Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.TOWARDS_UNLOADING_LOCATION,
                startTime: parseInt(+new Date()+19800000)
            };
            var options = {
                new: true
            };
            Service.BookingService.updateBookings(criteria, dataToUpdate, options, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    cb(null)
                }
            })
        }],
        getBooking: ['getDriver', function (cb) {
           var criteria = {
                bookingId: payloadData.bookingId
            };
            var projection = {
                _id:1,
                pickupLocationId:1,
                dropOffLocationId:1,
                contractId:1
            };
            var options = {
                lean: true
            };

            var populate = [
                 {
                    path: 'contractId',
                    match: {
                        
                    },
                    select: {
                        truckType:1,
                        freightAmount:1,
                        detentionCharges:1,
                        billingPeriod:1,
                        startCityId:1,
                        endCityId:1,
                        truckId:1
                    },
                    options: {}
                },
                 {
                    path: 'pickupLocationId',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1
                    },
                    options: {
                        lean:true
                    }
                },
                 {
                    path: 'dropOffLocationId',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1
                    },
                    options: {
                        lean:true
                    }
                }
            ];

            Service.BookingService.getBookingsPopulateDriverUpdateRide(criteria, projection, options, populate, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    data = result[0];

                    cb(null)
                }
            })
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {bookingData:data})
        }
    })
};



var forgotPassword = function (payloadData, callback) {

    var driverData;
    var newPassword;
    async.auto({
        checkForPhoneNumber: function (cb) {
            if (payloadData.phoneNumber) {
                var criteria = {
                    countryCode: payloadData.countryCode,
                    phoneNumber: payloadData.phoneNumber,
                    email: {$ne: null}
                };
            }
            else {
                var criteria = {
                    email: payloadData.email
                };
            }

            var projection = {};
            var option = {
                lean: true
            };

            Service.DriverService.getDriver(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    if (result.length) {

                        driverData = result[0];
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACCOUNT_NOT_REGISTERED);
                    }
                }

            });

        },
        setNewPassword: ['checkForPhoneNumber', function (cb) {
            newPassword = UniversalFunctions.generatePassword();
            var encryptedPassword = UniversalFunctions.CryptData(newPassword);
            var criteria = {
                _id: driverData._id
            };
            var setQuery = {
                password: encryptedPassword
            };
            var option = {
                new: true
            };
            Service.DriverService.updateDriver(criteria, setQuery, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });

        }],
        sendMail: ['setNewPassword', function (cb) {
            var subject = "RNA";
            var content = "It seems that you have forgotten your password.<br>";
            content += "Your password is : " + newPassword + " \n <br>";
            content += "Thank You <br>\n";
            content += "\n\n<br>";
            content += "Team RNA \n";
            emailPassword.sendEmail(driverData.email, subject, content);
            cb();
        }]

    }, function (err, result) {

        if (err) {
            callback(err)
        }
        else {
            callback(null, {"newPassword": newPassword})
        }

    })


};


var changePassword = function (payloadData, callback) {
    var driverData;

    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.DriverService.getDriver(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        driverData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },
        checkPassword: ['checkForAccessToken', function (cb) {
            var criteria = {
                _id: driverData._id,
                password: UniversalFunctions.CryptData(payloadData.oldPassword)
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.DriverService.getDriver(criteria, projection, option, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    if (dataAry.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_PASSWORD);
                    }
                }
            });
        }],
        resetPassword: ['checkPassword', function (cb) {
            var criteria = {
                _id: driverData._id
            };
            var setQuery = {
                password: UniversalFunctions.CryptData(payloadData.newPassword)
            };
            Service.DriverService.updateDriver(criteria, setQuery, {new: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {});
        }

    })

};


var getProfileData = function (payloadData, callback) {
    var driverData;
    async.auto({
        getDriver: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
               name: 1,
                countryCode: 1,
                phoneNumber:1,
                email:1,
                profilePicURL:1,
                documents:1,
                notification:1
            };
            var options = {
                lean: true
            };

            Service.DriverService.getDriver(criteria, projection, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                    if (result.length) {
                        driverData = result[0];
                        cb();

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        }
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {profileData: driverData});
        }
    })


};


var updateProfile = function (payloadData, callback) {
    var driverData;
    var newData = {};
    var profilePicURL;
    var thumbUrl;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.DriverService.getDriver(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        driverData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        uploadImage: ['checkForAccessToken', function (cb) {
            if (payloadData.pic && payloadData.pic.filename) {
                UploadManager.uploadFile(payloadData.pic, driverData._id, "hello", function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        console.log("uploaded info", uploadedInfo);
                        profilePicURL = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                        thumbUrl = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.thumbnail;
                        cb();
                    }
                })
            } else {
                profilePicURL = driverData.profilePicURL.original;
                thumbUrl= driverData.profilePicURL.thumbnail;
                cb();
            }
        }],
        updateDriver: ['uploadImage', function (cb) {
            var criteria = {
                _id: driverData._id
            };
            var setQuery = {
                name: payloadData.name,
                profilePicURL: {
                    original: profilePicURL,
                    thumbnail: thumbUrl
                }
            };

            var options = {
                new: true,
                upsert: true
            };

            'notification' in payloadData && (setQuery.notification=payloadData.notification)

            Service.DriverService.updateDriver(criteria, setQuery, options, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    newData.name = payloadData.name;
                    newData.phoneNumber = driverData.phoneNumber;
                    newData.email = driverData.email;
                    newData.countryCode = dataAry.countryCode;
                    newData.profilePicURL = {
                        original: profilePicURL,
                        thumbnail: thumbUrl
                    };
                    cb();
                }
            });

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {profileData: newData});
        }
    })

};


var getEarnings = function(payloadData,callback)
{
    var driverId;
    var totalHours = 0;
    var totalPayment = 0;
    var totalRides = 0;
    async.auto({
        getDriver: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id:1
            };
            var options = {
                lean: true
            };

            Service.DriverService.getDriver(criteria, projection, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                    if (result.length) {
                        driverId = result[0]._id;
                        cb();

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        },
        getRideData:['getDriver',function(cb)
        {
            var startIndex = -1;
            var endIndex = -1;

            var criteria = {
                _id: driverId
            };
            var projection = {
                currentDate:1,
                todayHours:1,
                todayRides:1,
                todayPayment:1,
                rideData:1
            };
            var options = {
                lean: true
            };

            Service.DriverService.getDriver(criteria, projection, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                    if((new Date(payloadData.startDate).setHours(0,0,0,0) <= new Date(result[0].currentDate).setHours(0,0,0,0)) && new Date(payloadData.endDate).setHours(0,0,0,0) >= new Date(result[0].currentDate).setHours(0,0,0,0))
                    {
                        totalHours += result[0].todayHours;
                        totalPayment += result[0].todayPayment;
                        totalRides += result[0].todayRides;
                    }
                    if(result[0].rideData.length)
                    {
                        result[0].rideData.forEach(function(earnings)
                        {
                            console.log(new Date(payloadData.startDate).setHours(0,0,0,0));
                            console.log(new Date(earnings.rideDate).setHours(0,0,0,0));
                            console.log(new Date(payloadData.endDate).setHours(0,0,0,0));
                            if((new Date(payloadData.startDate).setHours(0,0,0,0) <= new Date(earnings.rideDate).setHours(0,0,0,0)) && (new Date(payloadData.endDate).setHours(0,0,0,0) >= new Date(earnings.rideDate).setHours(0,0,0,0)))
                            {
                                console.log("here");
                                totalHours += earnings.hours;
                                totalPayment += earnings.payment;
                                totalRides += earnings.rides;
                            }

                        })

                    }
                    cb();

                }
            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {totalHours: totalHours, totalPayment:totalPayment,totalRides: totalRides});
        }
    })

};


var getRatings = function(payloadData,callback)
{
    var driverId;
    var totalRides;
    var fiveStarRides;
    var ratedRides;
    var feedbacks = [];
    async.auto({
        getDriver: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id:1,
                totalRides: 1,
                fiveStarRides: 1,
                ratedRides: 1
            };
            var options = {
                lean: true
            };

            Service.DriverService.getDriver(criteria, projection, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                    if (result.length) {
                        driverId = result[0]._id;
                        totalRides = result[0].totalRides;
                        fiveStarRides = result[0].fiveStarRides;
                        ratedRides = result[0].ratedRides;
                        cb();

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        },
        getFeedbacks:['getDriver',function(cb)
        {

            var criteria = {
                driverId: driverId
            };
            var projection = {
                remarks:1
            };
            var options = {
                lean: true
            };

            Service.RideService.getRide(criteria, projection, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {

                    if(result.length)
                    {
                        result.forEach(function(ride)
                        {
                            if(ride.remarks != null || ride.remarks)
                            {
                                feedbacks.push(ride.remarks);
                            }
                        })

                    }

                    cb();

                }
            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {"feedbacks":feedbacks,"totalRides":totalRides,"fiveStarRides":fiveStarRides,"ratedRides":ratedRides});
        }
    })

};


var updateNavigationType = function(payloadData,callback)
{
    var driverId;
    async.auto({
        getDriver: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id:1
            };
            var options = {
                lean: true
            };

            Service.DriverService.getDriver(criteria, projection, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                    if (result.length) {
                        driverId = result[0]._id;
                        cb();

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        },
        updateNavigation:['getDriver',function(cb)
        {

            var criteria = {
                _id: driverId
            };
            var setQuery = {
                navigationType : payloadData.navigationType
            };

            var options = {
                new: true,
                upsert: true
            };
            Service.DriverService.updateDriver(criteria, setQuery, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                    cb();

                }
            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {"navigationType": payloadData.navigationType});
        }
    })

};


var getRequestPush = function(payloadData,callback)
{
    var driverId;
    async.auto({
        getDriver: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id:1
            };
            var options = {
                lean: true
            };

            Service.DriverService.getDriver(criteria, projection, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                    if (result.length) {
                        driverId = result[0]._id;
                        cb();

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        },
        requestPush:['getDriver',function(cb)
        {

            var criteria = {

            };
            var projection = {

            };
            var options = {
                lean: true
            };
            var populate = [
                {
                    path: 'companyId',
                    match: {},
                    select: "firstName lastName countryCode phoneNumber",
                    options: {}
                }
            ];

            Service.BookingService.getBookingsPopulate(criteria, projection, options, populate, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {

                    var temp = {};
                    temp.bookingId = result[0]._id;
                    temp.title =  result[0].companyId.firstName +" "+ result[0].companyId.lastName + " sends you new booking request";
                    temp.pushType = 0;
                    var title =  temp.title;
                    sendPushNotification('ANDROID', payloadData.deviceToken, temp, title, function (err) {
                        if (err) {
                            cb(null)
                        } else {
                            cb(null)
                        }
                    })
                }
            });



        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {});
        }
    })

};


var sendPushNotification = function (deviceType, deviceToken, data, title, cb) {
    if (deviceType == "IOS" && deviceToken.length) {
        var temp = {};
        temp = data;

        NotificationManager.sendAndroidPushNotificationForDriver(deviceToken, temp, title, function (err, result) {
            if (err) {
                cb(err)
            } else {
                cb(null)
            }
        })
    } else if (deviceType == "ANDROID") {
        var temp = {};
        temp = data;

        NotificationManager.sendAndroidPushNotificationForDriver(deviceToken, temp, title, function (err, result) {
            if (err) {
                cb(err)
            } else {
                cb(null)
            }
        })
    }
};



var getBookingDetails = function(payloadData,callback)
{
    var driverId;
    var bookingDetails;
    async.auto({
        getDriver: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id:1
            };
            var options = {
                lean: true
            };

            Service.DriverService.getDriver(criteria, projection, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                    if (result.length) {
                        driverId = result[0]._id;
                        cb();

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        },
        bookingDetails:['getDriver',function(cb)
        {
            var criteria = {
            };

            payloadData.option  && (payloadData.option=='Current' && (criteria.$nor=[{status:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.NO_DRIVER_FOUND},{status:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.PENDING},{status:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.ENDED},{status:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.REJECTED}]) || (criteria.$or=[{status:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.ENDED},{status:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.REJECTED}]))

            payloadData.bookingId && (criteria.bookingId=parseInt(payloadData.bookingId))
            var projection = {
                _id:1,
                companyId:1,
                contractId:1,
                pickupLocationId:1,
                dropOffLocationId:1,
                materials:1,
                bookingDate:1,
                status:1,
                driverId:1,
                driverAmount:1,
                loadingInfo:1,
                driverDate:1,
                bookingId:1

            };
            var options = {
                lean: true
            };

            var populate = [
                {
                    path: 'materials',
                    model:'Materials',
                    match: {},
                    select: {
                        materialName:1,
                        length:1,
                        breadth:1,
                        height:1,
                        accessionNo:1,
                        capacity:1
                    },
                    options: {
                        lean:true
                    }
                },
                {
                    path: 'companyId',
                    match: {},
                    select: {
                        name:1,
                        address:1,
                        pincode:1,
                        city:1,
                        state:1
                    },
                    options: {
                        lean:true
                    }
                },
                 {
                    path: 'contractId',
                    match: {},
                    select: {},
                    options: {
                        lean:true
                    },
                    model: 'Contract'
                },
                {
                    path: 'pickupLocationId',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1,
                        authPerson: 1,
                        phoneNumber: 1,
                    },
                    options: {
                        lean:true
                    }
                },
                 {
                    path: 'dropOffLocationId',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1,
                        authPerson: 1,
                        phoneNumber: 1,
                    },
                    options: {
                        lean:true
                    }
                },
                 {
                    path: 'driverId',
                    match: {},
                    select: {
                        VendorTruck:1
                    },
                    options: {
                        lean:true
                    }
                }
            ];

            Service.BookingService.getBookingsPopulateDriver(criteria, projection, options,populate, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                        bookingDetails = result;
                        if(bookingDetails.length){
                           for(let i=0;i<bookingDetails.length;i++){
                               if(bookingDetails[i].materials && bookingDetails[i].materials.length){
                                   for(let j=0;j<bookingDetails[i].materials.length;j++){
                                       bookingDetails[i].materials[j].quantity=bookingDetails[i].loadingInfo && bookingDetails[i].loadingInfo.length && bookingDetails[i].loadingInfo[j].materialQuantity || '0';
                                   }
                               }
                           }
                        }

                        
                        cb();
                }
            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {bookingDetails:payloadData.option  && payloadData.option=='Current' && ( bookingDetails && bookingDetails.length && bookingDetails[0] || {}) || bookingDetails});
        }
    })
};



var getLoadingDetails = function(payloadData,callback)
{
    var driverId;
    var loadingDetails;
    async.auto({
        getDriver: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id:1
            };
            var options = {
                lean: true
            };

            Service.DriverService.getDriver(criteria, projection, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                    if (result.length) {
                        driverId = result[0]._id;
                        cb();

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        },
        bookingDetails:['getDriver',function(cb)
        {
            var criteria = {
                bookingId: payloadData.bookingId
            };
            var projection = {
                _id:1,
                loadingLocations:1,
                companyId:1
            };
            var options = {
                lean: true
            };

            var populate = [
                {
                    path: 'loadingLocations.materials.materialId',
                    match: {},
                    select: "materialName",
                    options: {}
                },
                {
                    path: 'companyId',
                    match: {},
                    select: "firstName lastName address",
                    options: {}
                }
            ];

            Service.BookingService.getBookingsPopulate(criteria, projection, options,populate, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {

                    console.log(result)
                    loadingDetails = {};
                    loadingDetails.materials =[];
                    loadingDetails.company = {};

                     var loadingLocations = result[0].loadingLocations;
                     var companyDetails = result[0].companyId;
                     loadingLocations.forEach(function(loadings)
                     {
                         if(loadings.materials)
                         {
                             loadings.materials.forEach(function(material){

                                 loadingDetails.materials.push({
                                     "materialName":material.materialId.materialName,
                                     "materialQuantity":material.materialQuantity
                                 })
                             })
                         }


                     });
                     loadingDetails.company = {
                         "companyName": companyDetails.firstName + " " +companyDetails.lastName,
                         "address":companyDetails.address
                     };
                     loadingDetails.invoiceNumber = "BS-12-334347";
                     loadingDetails.permitNumber = "34fdf45f";
                     cb();

                }
            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {loadingDetails:loadingDetails});
        }
    })
};


var reachedUnloadingLocation = function(payloadData,callback)
{
    var driverId;
    var name;
    var deviceToken;
    var location;
    let authPerNo;
    let startLocation;
    let endLocation;
    let truckNo;
    let truckType;
    async.auto({

        getDriver: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var options = {
                lean: true
            };

            Service.DriverService.getDriver(criteria, projection, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                    if (result.length) {
                        driverId = result[0]._id;
                        name  =result[0].name;
                        cb();

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        },
        updateBooking: ['getDriver', function (cb) {

            var criteria = {
                bookingId: payloadData.bookingId,
                dropOffLocationId: payloadData.unloadingLocationId
            };
            var setQuery = {
                status:Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.UNLOADING_LOCATION,
                unloadLocationReachTime:parseInt(+new Date()+19800000)
            };
            var options = {
                new: true
            };

            Service.BookingService.updateBookings(criteria,setQuery,options,function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    startLocation=result.pickupAddress;
                    endLocation=result.dropOffAddress;
                    truckNo=result.truckNo;
                    truckType=result.truckType;
                    cb(null)
                }
            })
        }],
         getLoaderData:['updateBooking',function(cb){
            var criteria={
                isDeleted:false,
                isBlocked:false,
                assignedLocations:{$elemMatch:{$eq:payloadData.unloadingLocationId}}
            }

            var projection={
                deviceToken:1,
                assignedLocations:{$elemMatch:{$eq:payloadData.unloadingLocationId}}
            }

            var options={
                lean:true
            }

            var populate=[
                {
                    path: 'assignedLocations',
                    model:'Location',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1
                    },
                    options: {
                        lean:true
                    }
                },
            ]

            Service.LoaderService.getLoaderPopulate(criteria,projection,options,populate,function(err,result){
                if(err){
                    cb(err)
                }else{
                    console.log('rrrrrrrrrrrrrrrrr',result)

                    if(result.length){
                         deviceToken=result[0].deviceToken;
                    location=result[0].assignedLocations && result[0].assignedLocations.length && result[0].assignedLocations[0].pointName || ''
                    }
                   
                    cb()
                }
            })
        }],
          getAuthPerNo:['getLoaderData',function(cb){
            var criteria={
                _id:payloadData.unloadingLocationId
            }

            var projection={
                phoneNumber:1
            }

            var options={
                lean:true
            }
            Service.LocationService.getLocation(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    console.log('rrrrrrrrrrrrrrrrr',result)

                    if(result.length){
                         authPerNo=result && result.length && result[0].phoneNumber;
                         cb()
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        }],
         sendNotification:['getLoaderData',function (cb) {
            if(deviceToken){
                var temp = {};
                temp.pushType=0;
                temp.bookingId = payloadData.bookingId;
                temp.title = "Driver "+ name + " is reached on dropoff location "+location+" .";
                var title =  'DRIVER ARRIVED';
                NotificationManager.sendAndroidPushNotificationForLoader(deviceToken, temp, title, function (err) {
                    if (err) {
                        console.log('ERRRRRRRRRRRRRRR',err);
                        cb(null)
                    } else {
                        cb(null)
                    }
                })
            }else{
                cb()
            }
        }],
        sendSMS:['getAuthPerNo',function(cb){
           let message= "Driver "+ name + " is reached on dropoff location "+location+" for booking no "+payloadData.bookingId+" from "+startLocation+" to "+endLocation+" on truck "+truckType+" ("+truckNo+")." 
            NotificationManager.sendSMSToLoader(message,null, authPerNo,function(err,result)
            {
                cb(err)
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null,{})
        }
    })

};


var fillMaterialForm = function (payloadData, callback) {
console.log('pppppppppppppppppppppppppp',payloadData.materials)
    var driverId;
    let OTP;
    let unloadingLocation;
    let authPerNo;
    let materials=' ';
    let startLocation;
    let endLocation;
    let bookingId;
    async.auto({

        getDriver: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var options = {
                lean: true
            };

            Service.DriverService.getDriver(criteria, projection, options, function (err, result) {
                if (err) {
                    callback(err)
                }
                else {
                    if (result.length) {
                        driverId = result[0]._id;
                        cb();

                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })
        },
        updateMaterials: ['getDriver',function (cb) {
            OTP=UniversalFunctions.generateOTP();
            var criteria = {
                _id: payloadData.bookingId
            };

            var setQuery = {
                unloadInfo: payloadData.materials,
                OTP:OTP,
                otpVerified:false
            };

            var options = {
                new: true
            };

            Service.BookingService.updateBookings(criteria, setQuery, options, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if(result && Object.keys(result).length){
                        unloadingLocation=result && result.dropOffLocationId && result.dropOffLocationId || null;
                        startLocation=result.pickupAddress;
                        endLocation = result.dropOffAddress;
                        bookingId= result.bookingId;
                        cb()
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        }],
          getBookingData:['updateMaterials',function(cb)
        {
            var criteria = {
                _id:payloadData.bookingId
            };

            var projection = {
                unloadInfo:1
            };

            var options = {
                new: true
            };
            var populate = [
                {
                    path: 'unloadInfo.materialId',
                    match: {},
                    select: {},
                    options: {}
                }
            ];                               

            Service.BookingService.getBookingsPopulate(criteria, projection, options, populate,function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    for(let i=0;i<result[0].unloadInfo.length;i++){
                        materials+=' ('+result[0].unloadInfo[i].materialId.materialName +' totalQty:'+result[0].unloadInfo[i].totalQty +', ok: '+result[0].unloadInfo[i].ok +', missing: '+result[0].unloadInfo[i].missing+', damaged: '+result[0].unloadInfo[i].damaged +', other: '+result[0].unloadInfo[i].other +' ),'; 
                    }

                    materials=materials.replace(materials.lastIndexOf(','),'');
                    cb()
                }
            })

        }],
         getAuthPerNo:['updateMaterials',function(cb){
             if(unloadingLocation){
                var criteria={
                    _id:unloadingLocation
                }

                var projection={
                    phoneNumber:1
                }

                var options={
                    lean:true
                }
                Service.LocationService.getLocation(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        console.log('rrrrrrrrrrrrrrrrr',result)

                        if(result.length){
                            authPerNo=result && result.length && result[0].phoneNumber;
                            cb()
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }
                    }
                })
             }else{
                 cb()
             }
        }],
         sendSMS:['getAuthPerNo','getBookingData',function(cb){
             if(unloadingLocation && authPerNo){
                let message='Booking id '+bookingId+' from '+startLocation+' to '+endLocation+' is updated with following details : '+materials +' . Your OTP is '+OTP;
                NotificationManager.sendSMSToLoader(message, null, authPerNo,function(err,result)
                {
                    cb(err)
                })
             }else{
                 cb()
             }
        }]
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {
                OTP:OTP
            });
        }

    })

};



module.exports = {

    driverLogin: driverLogin,
    OTPLogin:OTPLogin,
    sendOTP:sendOTP,
    accessTokenLogin: accessTokenLogin,
    driverLogout: driverLogout,
    updateDriverLocation:updateDriverLocation,
    getProfileData:getProfileData,
    updateProfile:updateProfile,
    getRequestPush:getRequestPush,
    getBookingDetails:getBookingDetails,
    acceptRide: acceptRide,
    rejectRide: rejectRide,
    forgotPassword: forgotPassword,
    startRide: startRide,
    reachedLoadingLocation: reachedLoadingLocation,
    sendPushNotification:sendPushNotification,
    jegetLoadingDetails:getLoadingDetails,
    reachedUnloadingLocation:reachedUnloadingLocation,
    fillMaterialForm:fillMaterialForm,
    updateBookingStatus:updateBookingStatus,
    endRide:endRide,
    sendMessage:sendMessage
};
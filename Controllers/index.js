

module.exports  = {
    AdminController : require('./AdminController'),
    AppVersionController : require('./AppVersionController'),
    VendorController: require('./VendorController'),
    DriverController: require('./DriverController'),
    CompanyController: require('./CompanyController'),
    LoaderController: require('./LoaderController'),
    CityController:require('./CityController'),
    SettingsController:require('./SettingsController')
};
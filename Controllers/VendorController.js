/**
 * Created by parasgambhir on 23/04/17.
 */


'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');
var UploadManager = require('../Lib/UploadManager');
var TokenManager = require('../Lib/TokenManager');
var Config = require('../Config');
var NotificationManager = require('../Lib/NotificationManager');
var distance = require('google-distance');
var emailPassword = require('../Lib/email');
var appConstants = require('../Config/appConstants');
var mongoose  =require('mongoose')
var Joi   =require('joi')



var vendorSignup = function(payloadData,callback)
{

    var vendorId;
    var accessToken;
    var OTP;
    var insert=true;
    let obj={};
    var name = Joi.string().regex(/^[a-zA-Z ]+$/).required();
    var pan  = Joi.string().regex(/^[a-zA-Z0-9]+$/)

    if((Joi.validate(payloadData.name,name)).error){
        return callback('Name can only contain alphabets.')
    }
 

    if((Joi.validate(payloadData.panNumber,pan)).error){
        return callback('PanNumber must be alphanumeric.')
    }

    async.auto({
        checkInCompany:function(cb){
            var criteria = {
                countryCode: payloadData.countryCode,
                phoneNumber: payloadData.phoneNumber
            };

            Service.CompanyService.getCompaniesCount(criteria,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result){
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.COMPANY_NO_EXISTS)
                    }else{
                        cb()
                    }
                }
            })


        },
        checkInCompanyEmail:function(cb){
            var criteria = {
                email:new RegExp('^'+payloadData.email+'$','i')
            };

            Service.CompanyService.getCompaniesCount(criteria,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result){
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_ALREADY_EXIST)
                    }else{
                        cb()
                    }
                }
            })


        },
        checkForUserExists:[ 'checkInCompany','checkInCompanyEmail',function (cb) {
            let pattern

            var criteria = {
                countryCode: payloadData.countryCode,
                contactNo: payloadData.phoneNumber,
                email :{$nin: [new RegExp('^'+payloadData.email+'$','i')] }
            };
            var projection = {};
            var option = {
                lean: true
            };

            Service.VendorService.getVendor(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                }
                else {
                    if (result.length) {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.VENDOR_NO_EXISTS);
                    }
                    else {

                        var criteria = {
                          email : { $regex: new RegExp("^" +payloadData.email+"$",'i')}
                        };
                        var projection = {};
                        var option = {
                            lean: true
                        };
                        Service.VendorService.getVendor(criteria, projection, option, function (err, result) {
                            if (err) {
                                cb(err)
                            }
                            else {
                                

                               if (result.length) {
                                    if(result[0].step1 && result[0].step2 && result[0].step3 && result[0].emailVerify && result[0].approvedByAdmin){
                                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.COMPANY_EMAIL_EXISTS);
                                    }else if(result[0].step1 && result[0].step2 && result[0].step3 && !result[0].emailVerify){
                                        callback('Sorry email verification is pending , visit to your email to verification.')
                                    }else if(result[0].step1 && result[0].step2 && result[0].step3 && !result[0].approvedByAdmin){
                                        callback('Sorry your account is not approved  by Admin.')
                                    }
                                    else{
                                        insert=false;
                                        obj=result[0];
                                        cb();
                                    }
                                    
                                }
                                else{
                                    cb();
                                }

                            }
                        });
                    }
                }

            });

        }],

        insertData: ['checkForUserExists',function(cb){

            OTP = UniversalFunctions.generateOTP();

            var cryptedPassword = UniversalFunctions.CryptData(payloadData.password);
            var criteria={
                email:new RegExp("^" +payloadData.email+"$",'i')
            }

             var dataToBeInserted = {
                $set:{
                    countryCode: payloadData.countryCode,
                    contactNo: payloadData.phoneNumber,
                    email : payloadData.email,
                    name: payloadData.name,
                    password: cryptedPassword,
                    step1:obj.step1 || true,
                    step2:obj.step2 || false,
                    step3:obj.step3 || false,
                    OTP:OTP
                },
                $setOnInsert:{
                    registrationDate: new Date(),
                    isBlocked:false,
                    profilePicURL: {
                        original:null,
                        thumbnail: null
                    },
                    OTPVerified:false,
                    profileCompleted: false,
                    approvedByAdmin:false,
                    address: null,
                    emailVerify:false,
                    emailNotifications:true,
                    smsNotifications:true,
                    pushNotifications:true,
                    isDeleted:false
                }
                
            };

            var options={
                upsert:true,
                new:true
            }

            console.log(dataToBeInserted);
            Service.VendorService.updateVendor(criteria,dataToBeInserted,options,function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    obj=result;
                    if(obj.step1 && !obj.step2){
                            NotificationManager.sendSMSToUser(OTP, payloadData.countryCode, payloadData.phoneNumber,function(err,result)
                        {
                            // NotificationManager.sendEmailToUser('OTP' ,{'OTP':OTP},payloadData.email,function(err,result)
                            // {
                            //     cb();
                            // })
                            cb()

                        })
                    }else{
                        OTP='';
                        cb()
                    }
                    
                }
            });

        }],

        setToken: ['insertData', function (cb) {
            var tokenData = {
                id: obj._id,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.VENDOR
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                          cb()
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })

        }]

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null,{accessToken: accessToken, OTP : OTP,details:obj});
        }

    })

};



var verifyOTP = function (payloadData,callback) {

    var vendorId;
    var OTPSaved;

    async.auto({

        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.VendorService.getVendor(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        vendorId = result[0]._id;
                        OTPSaved = result[0].OTP;
                        if(OTPSaved == payloadData.OTP)
                        {
                            cb();
                        }
                        else{
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_OTP);
                        }
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })

        },
        updateVendor: ['checkForUserExists', function (cb) {
            var criteria = {
                _id: vendorId
            };
            var setQuery = {
                OTPVerified : true,
                step2:true
            };
            Service.VendorService.updateVendor(criteria, setQuery, {
                new: true,
                upsert: true
            }, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {});
        }

    })

};



var completeProfile = function(payloadData,callback)
{
    var vendorId;
    var adharURL;
    var PanURL;
    var vendorData;
    var email;
    var accessToken;
    var userName;
console.log('paylllllllllllllll',payloadData)

    async.auto({

        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.VendorService.getVendor(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        vendorId = result[0]._id;
                        email=result[0].email;
                        userName=result[0].name;
                         cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })

        },
        uploadAdhar: ['checkForUserExists', function (cb) {
            if(payloadData.adharPic && payloadData.adharPic!='undefined'){
                    UploadManager.uploadFile(payloadData.adharPic && payloadData.adharPic.filename && payloadData.adharPic || payloadData.adharPic[1], vendorId, "hello", function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        console.log("uploaded info", uploadedInfo);
                        adharURL = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                        cb();
                    }
                })
            }else{
                cb('Adhar pic required.')
            }
           
        }],
        uploadPan:['checkForUserExists',function(cb)
        {
            if(payloadData.panPic && payloadData.panPic!='undefined'){
                 UploadManager.uploadFile(payloadData.panPic && payloadData.panPic.filename && payloadData.panPic || payloadData.panPic[1], Math.random()*1000, "hello", function (err, uploadedInfo) {
                if (err) {
                    cb(err)
                } else {
                    console.log("uploaded info", uploadedInfo);
                    PanURL = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                    cb();
                }
            })

            }else{
                cb('Pan card pic required.')
            }

           
        }],
        setToken:['uploadAdhar','uploadPan',function(cb){
             var tokenData = {
                id: vendorId,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.VENDOR
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })
        }],
        updateVendor:['setToken',function(cb)
        {
            var criteria = {
                _id: vendorId
            };
            var setQuery = {
                aadharNumber: payloadData.adharNumber,
                panNumber: payloadData.panNumber,
                aadharCardImage: adharURL,
                panCardImage: PanURL,
                city:payloadData.city,
                address : payloadData.address,
                state:payloadData.state,
                pincode:payloadData.pincode,
                profileCompleted: true,
                accessToken:accessToken,
                step3:true
            };
            Service.VendorService.updateVendor(criteria, setQuery, {
                new: true,
                upsert: true
            }, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    vendorData = dataAry;
                    cb(null);
                }
            });
        }],
        sendEmail:['updateVendor',function(cb){
            let emailVariables={
                user_name:userName,
                accessToken:accessToken,
                userType:'VENDOR'
            }

            NotificationManager.sendEmailToUser('REGISTRATION_MAIL' ,emailVariables,email,function(err,result){
                console.log('Error in email sending..................',err,'......RRRRRRRRr...',result)
                cb(err);
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {});
        }
    })

};


var editProfile = function(payloadData,callback)
{
    var vendorId;
    var adharURL;
    var PanURL;
    var vendorData;
    async.auto({
        uploadAdhar: ['checkForUserExists', function (cb) {
            if(payloadData.adharPic && (payloadData.adharPic.filename || payloadData.adharPic[1].filename)){
                 UploadManager.uploadFile(payloadData.adharPic && payloadData.adharPic.filename && payloadData.adharPic || payloadData.adharPic[1], vendorId, "hello", function (err, uploadedInfo) {
                if (err) {
                    cb(err)
                } else {
                    console.log("uploaded info", uploadedInfo);
                    adharURL = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                    cb();
                }
            })
            }else{
                cb()
            }

           
        }],
        uploadPan:['checkForUserExists',function(cb)
        {

            if(payloadData.panPic && (payloadData.panPic.filename || payloadData.panPic[1].filename)){
                UploadManager.uploadFile(payloadData.panPic && payloadData.panPic.filename && payloadData.panPic || payloadData.panPic[1], Math.random()*1000, "hello", function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        console.log("uploaded info", uploadedInfo);
                        PanURL = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                        cb();
                    }
                })
            }else{
                cb()
            }

        }],
        updateVendor:['uploadAdhar','uploadPan',function(cb)
        {
            var criteria = {
                _id: payloadData.vendorId
            };
            var setQuery = {
                countryCode:payloadData.countryCode,
                contactNo:payloadData.phoneNumber,
                email:payloadData.email,
                name:payloadData.name,
                aadharNumber: payloadData.adharNumber,
                panNumber: payloadData.panNumber,
                city:payloadData.city,
                address : payloadData.address,
                state:payloadData.state,
                pincode:payloadData.pincode,
                profileCompleted: true
            };

            if(adharURL){
                setQuery.aadharCardImage=adharURL
            }

            if(PanURL){
                setQuery.panCardImage=PanURL
            }

            Service.VendorService.updateVendor(criteria, setQuery, {
                new: true,
                upsert: true
            }, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    vendorData = dataAry;
                    cb(null);
                }
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {
                details:vendorData
            });
        }
    })

};

var vendorLogin = function(payloadData,callback)
{

    var vendorData;
    var cryptedPassword = UniversalFunctions.CryptData(payloadData.password);
    async.auto({
        checkForUserExists: function (cb) {
            var criteria = {
                email: { $regex: new RegExp("^" +payloadData.email+"$",'i')}
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.VendorService.getVendor(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {

                        if(result[0].password == cryptedPassword)
                        {
                            vendorData = result[0];
                            if(result[0].OTPVerified == true)
                            {
                                if(result[0].profileCompleted == true)
                                {
                                    if(result[0].emailVerify){
                                        if(result[0].approvedByAdmin == true)
                                        {
                                            if(!result[0].isBlocked){
                                                 cb();
                                            }else{
                                                cb('Sorry your account is blocked by Admin.')
                                            }
                                           
                                        }
                                        else{
                                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_APPROVED);
                                        }
                                    }else{
                                        cb('Sorry email verification is pending , visit to your email to verification.')
                                    }
                                }
                                else
                                {
                                   cb('Sorry your profile is nor completed yet.');
                                }
                            }
                            else {
                                cb('Sorry OTP verification pending.');
                            }
                        }
                        else
                        {
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_USER_PASS);
                        }
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACCOUNT_NOT_REGISTERED);
                    }
                }
            })

        }

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {vendorData: vendorData});
        }

    })

};




const addDriver = function(payloadData,callback)
{
    var adharURL;
    var licenseURL;
    var vendorId;
    var status1=false;
    var name = Joi.string().regex(/^[a-zA-Z ]+$/).required();

    if((Joi.validate(payloadData.name,name)).error){
        return callback('Name can only contain alphabets.')
    }

    async.auto({

        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.VendorService.getVendor(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        vendorId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })

        },
        checkEmailOfDriver: ['checkForUserExists',function (cb) {
            if(payloadData.email){
                var criteria={
                    email:payloadData.email
                }
                var projection={}
                var options={}

                Service.DriverService.getDriver(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result.length){
                             callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DRIVER_EMAIL_EXISTS);
                        }else{
                            cb(null)
                        }
                    }
                })
            }else{
                cb(null)
            }

        }],
        checkDriver:['checkEmailOfDriver',function(cb){
            var criteria = {
                    countryCode: payloadData.countryCode,
                    phoneNumber: payloadData.phoneNumber
                };
                var projection = {};
                var option = {
                    lean: true
                };
                Service.DriverService.getDriver(criteria, projection, option, function (err, result) {

                    if(err){
                        cb(err)
                    }
                    else {
                        if (result.length) {
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_CONTACT);
                        }
                        else {
                            cb();
                        }
                    }

                })
        }],
        uploadAdhar: ['checkDriver', function (cb) {
            console.log('pay================',typeof(payloadData.adharPic))
            UploadManager.uploadFile(payloadData.adharPic && payloadData.adharPic.filename?payloadData.adharPic: payloadData.adharPic[1],Math.random()*1000, "hello", function (err, uploadedInfo) {
                if (err) {
                    cb(err)
                } else {
                    console.log("uploaded info", uploadedInfo);
                    adharURL = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                    cb();
                }
            })
        }],
        uploadLicense:['checkDriver',function(cb)
        {
            UploadManager.uploadFile(payloadData.licensePic && payloadData.licensePic.filename?payloadData.licensePic : payloadData.licensePic[1], Math.random()*1000, "hello", function (err, uploadedInfo) {
                if (err) {
                    cb(err)
                } else { 
                    console.log("uploaded info", uploadedInfo);
                    licenseURL = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                    cb();
                }
            })

        }],
        insertDriver:['uploadAdhar','uploadLicense',function(cb)
        {

            var documents = [];

            documents.push({
                "documentType":"ADHAR",
                "documentImage":adharURL
            });
            documents.push({
                "documentType":"DRIVING_LICENSE",
                "documentImage":licenseURL
            });


            var dataToBeInserted = {
                name: payloadData.name,
                dob: payloadData.dob,
                countryCode : payloadData.countryCode,
                address:payloadData.address,
                pincode:payloadData.pincode,
                city:payloadData.city,
                state:payloadData.state,
                country:payloadData.country,
                phoneNumber: payloadData.phoneNumber,
                email:payloadData.email,
                password: UniversalFunctions.CryptData(payloadData.password),
                vendorId: vendorId,
                documents: documents
            };
            Service.DriverService.createDriver(dataToBeInserted, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb(null);
                }
            });


        }]

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null,{});
        }

    })

};


const updateDriver = function(payloadData,callback)
{
    var adharURL;
    var licenseURL;
    var vendorId;
    var status1=false;
    var name = Joi.string().regex(/^[a-zA-Z ]+$/).required();

    name.validate(payloadData.name,function(err){
        if(err){
            status1=true;
        }
    })


    if(status1){
        return callback('Name can only contain alphabets.')
    }

    async.auto({

        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.VendorService.getVendor(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        vendorId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })

        },
        checkForDriverExists:[ 'checkForUserExists',function (cb) {
            var criteria = {
                _id: payloadData.driverId
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.DriverService.getDriver(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_DRIVER_ID);
                    }
                }
            })
        }],
        checkEmailOfDriver: ['checkForDriverExists',function (cb) {
            if(payloadData.email){
                var criteria={
                    _id: {$ne:payloadData.driverId},
                    email:payloadData.email
                }
                var projection={}
                var options={}

                Service.DriverService.getDriver(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result.length){
                             callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DRIVER_EMAIL_EXISTS);
                        }else{
                            cb(null)
                        }
                    }
                })
            }else{
                cb(null)
            }

        }],
        checkDriverContact:['checkEmailOfDriver',function(cb){
            var criteria = {
                    countryCode: payloadData.countryCode,
                    phoneNumber: payloadData.phoneNumber,
                     _id: {$ne:payloadData.driverId},
                };
                var projection = {};
                var option = {
                    lean: true
                };
                Service.DriverService.getDriver(criteria, projection, option, function (err, result) {

                    if(err){
                        cb(err)
                    }
                    else {
                        if (result.length) {
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_CONTACT);
                        }
                        else {
                            cb();
                        }
                    }

                })
        }],
        updateDriver:['checkDriverContact',function(cb)
        {

            var criteria={
                _id:payloadData.driverId
            }

            var dataToBeInserted = {
                $set:{
                    name: payloadData.name,
                    dob: payloadData.dob,
                    countryCode : payloadData.countryCode,
                    address:payloadData.address,
                    pincode:payloadData.pincode,
                    city:payloadData.city,
                    state:payloadData.state,
                    country:payloadData.country,
                    phoneNumber: payloadData.phoneNumber,
                    email:payloadData.email,
                    vendorId: vendorId
                }
            };


            if(payloadData.password){
                dataToBeInserted.password=UniversalFunctions.CryptData(payloadData.password)
            }

            var options={
                new:true
            }

            Service.DriverService.updateDriver(criteria,dataToBeInserted,options, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if(result && Object.keys(result).length)
                        cb(null);
                    else
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)    
                }
            });


        }]

    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null,{});
        }

    })

};

// const updateDriverDocs=function(payloadData,callback){

//     var adharURL;
//     var licenseURL;
//     var vendorId;

//     async.auto({
//         checkForUserExists: function (cb) {
//             var criteria = {
//                 accessToken: payloadData.accessToken
//             };
//             var projection = {};
//             var option = {
//                 lean: true
//             };
//             Service.VendorService.getVendor(criteria, projection, option, function (err, result) {
//                 if (err) {
//                     cb(err);
//                 } else {
//                     if (result.length) {
//                         vendorId = result[0]._id;
//                         cb();
//                     }
//                     else {
//                         callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
//                     }
//                 }
//             })

//         },
//         checkForDriverExists:[ 'checkForUserExists',function (cb) {
//             var criteria = {
//                 _id: payloadData.driverId
//             };
//             var projection = {};
//             var option = {
//                 lean: true
//             };
//             Service.DriverService.getDriver(criteria, projection, option, function (err, result) {
//                 if (err) {
//                     cb(err);
//                 } else {
//                     if (result.length) {
//                         cb();
//                     }
//                     else {
//                         callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_DRIVER_ID);
//                     }
//                 }
//             })
//         }],
//         uploadAdhar: ['checkForDriverExists', function (cb) {
//             if(payloadData.adharPic && payloadData.adharPic.filename){
//                     UploadManager.uploadFile(payloadData.adharPic && payloadData.adharPic.filename?payloadData.adharPic: payloadData.adharPic[1],Math.random()*1000, "hello", function (err, uploadedInfo) {
//                     if (err) {
//                         cb(err)
//                     } else {
//                         console.log("uploaded info", uploadedInfo);
//                         adharURL = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
//                         cb();
//                     }
//                 })
//             }else{
//                 cb()
//             }
            
//         }],
//         uploadLicense:['checkForDriverExists',function(cb)
//         {
//             if(payloadData.licensePic && payloadData.licensePic.filename){
//                 UploadManager.uploadFile(payloadData.licensePic && payloadData.licensePic.filename?payloadData.licensePic : payloadData.licensePic[1], Math.random()*1000, "hello", function (err, uploadedInfo) {
//                     if (err) {
//                         cb(err)
//                     } else { 
//                         console.log("uploaded info", uploadedInfo);
//                         licenseURL = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
//                         cb();
//                     }
//                 })
//             }else{
//                 cb()
//             }

//         }],
//         updateDriver:['checkDriver',function(cb)
//         {

//             if(!(adharURL && licenseURL && 'notification' in payloadData )){
//                 return cb('Sorry Nothing to update.')
//             }

//             var criteria={
//                 _id:payloadData.driverId
//             }

//             var dataToBeInserted = {
//                 $set:{
//                     name: payloadData.name,
//                     dob: payloadData.dob,
//                     countryCode : payloadData.countryCode,
//                     address:payloadData.address,
//                     pincode:payloadData.pincode,
//                     city:payloadData.city,
//                     state:payloadData.state,
//                     country:payloadData.country,
//                     phoneNumber: payloadData.phoneNumber,
//                     email:payloadData.email,
//                     vendorId: vendorId
//                 }
//             };


//             if(payloadData.password){
//                 dataToBeInserted.password=UniversalFunctions.CryptData(payloadData.password)
//             }

//             var options={
//                 new:true
//             }

//             Service.DriverService.updateDriver(criteria,dataToBeInserted,options, function (err, result) {
//                 if (err) {
//                     cb(err)
//                 } else {
//                     if(result && Object.keys(result).length)
//                         cb(null);
//                     else
//                         cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)    
//                 }
//             });


//         }]

//     }, function (err, result) {
//         if (err) {
//             callback(err)
//         }
//         else {
//             callback(null,{});
//         }

//     })
// }


var listDrivers = function(payloadData,callback)
{

    var vendorId;
    var driverData;
    async.auto({

        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.VendorService.getVendor(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        vendorId = result[0]._id;
                        console.log("vendor id", vendorId);
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })

        },
        listDrivers: ['checkForUserExists', function (cb) {
            var criteria = {
                vendorId: vendorId
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.DriverService.getDriver(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    driverData = result;
                    cb();
                }
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {driverData:driverData});
        }

    })
};


const addVendorTruck = function(payloadData,callback){
    let vendorId= null;
    let  truckId;
    let rcPic;
    let fitnessCertificate;
    let pollutionPic;
    let insurancePic;
    if(payloadData.startCityId == payloadData.endCityId){
        return callback('Start end End Cities should be different. ')
    }

    async.auto({
        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.VendorService.getVendor(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        vendorId = result[0]._id;
                         console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP',payloadData);
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })

        },
        checkTruckId:['checkForUserExists',function(cb){
            var criteria={
                _id:payloadData.truckId
            }

            var projection={}

            var options={}

            Service.TruckService.getTruck(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        cb(null)
                    }else{
                        cb('Invalid Truck Id.')
                    }
                }
            })
        }],
        getStartCityLat:['checkTruckId',function(cb){
            var criteria={
                _id:payloadData.startCityId,
                isDeleted:false
            }

            var projection={
                cityName:1,
                location:1
            }

            var options={}

            Service.CityService.getCity(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        payloadData.startLocationAddress=result[0].cityName;
                        payloadData.startLocation=result[0].location;
                        cb(null)
                    }else{
                        cb('Invalid Start City Id.')
                    }
                }
            })
        }],
         getEndCityLat:['checkTruckId',function(cb){
            var criteria={
                _id:payloadData.endCityId,
                isDeleted:false
            }

            var projection={
                cityName:1,
                location:1
            }

            var options={}

            Service.CityService.getCity(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        payloadData.endLocationAddress=result[0].cityName;
                        payloadData.endLocation=result[0].location;
                        cb(null)
                    }else{
                        cb('Invalid End City Id.')
                    }
                }
            })
        }],
        addTruck:['getStartCityLat','getEndCityLat',function(cb){
            console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP',payloadData);
            let obj = {
                truckId:payloadData.truckId,
                vendorId:vendorId,
                truckNo:payloadData.truckNo,
                startLocation:payloadData.startLocation,
                startCityId:payloadData.startCityId,
                endLocation:payloadData.endLocation,
                endCityId:payloadData.endCityId,
                startAddress:payloadData.startLocationAddress,
                endAddress:payloadData.endLocationAddress
            }
            Service.VendorTruckService.createVendorTruck(obj,function(err,result){
                if(err){
                    cb(err)
                }else{
                    truckId = result._id;
                    cb(null)
                }
            })
        }],
        rcPicUpload:['checkForUserExists',function(cb)
        {
            UploadManager.uploadFile(payloadData.rcPic && payloadData.rcPic.filename ? payloadData.rcPic :payloadData.rcPic[1], Math.random()*1000, "hello", function (err, uploadedInfo) {
                if (err) {
                    cb(err)
                } else {
                    console.log("uploaded info", uploadedInfo);
                    rcPic = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                    cb();
                }
            })
        }],
        pollutionPic:['checkForUserExists',function(cb)
        {
            UploadManager.uploadFile(payloadData.pollutionPic && payloadData.pollutionPic.filename ? payloadData.pollutionPic :payloadData.pollutionPic[1], Math.random()*1000, "hello", function (err, uploadedInfo) {
                if (err) {
                    cb(err)
                } else {
                    console.log("uploaded info", uploadedInfo);
                    pollutionPic = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                    cb();
                }
            })
        }],
        insurancePic:['checkForUserExists',function(cb)
        {
            UploadManager.uploadFile(payloadData.insurancePic && payloadData.insurancePic.filename ?payloadData.insurancePic :payloadData.insurancePic[1], Math.random()*1000, "hello", function (err, uploadedInfo) {
                if (err) {
                    cb(err)
                } else {
                    console.log("uploaded info", uploadedInfo);
                    insurancePic = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                    cb();
                }
            })
        }],
        fitnessCertificate:['checkForUserExists',function(cb)
        {
            UploadManager.uploadFile(payloadData.fitnessCertificate && payloadData.fitnessCertificate.filename ?payloadData.fitnessCertificate:payloadData.fitnessCertificate[1] , Math.random()*1000, "hello", function (err, uploadedInfo) {
                if (err) {
                    cb(err)
                } else {
                    console.log("uploaded info", uploadedInfo);
                    fitnessCertificate = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                    cb();
                }
            })
        }],
        uploadImage:['addTruck','rcPicUpload','pollutionPic','insurancePic','fitnessCertificate',function(cb){
            var documents = [];
            documents.push({
                "documentType":"FITNESS",
                "documentImage":fitnessCertificate
            });
            documents.push({
                "documentType":"INSURANCE",
                "documentImage":insurancePic
            });
            documents.push({
                "documentType":"POLLUTION",
                "documentImage":pollutionPic
            });
            documents.push({
                "documentType":"REGISTRATION",
                "documentImage":rcPic
            });
            
            let query = {_id:truckId}
            console.log(".......truckId............",truckId);
            console.log(".......documents............",documents);


            Service.VendorTruckService.updateVendorTruck(query,{documents:documents},{lean:true},function(err,result){
                if(err){
                    cb(err)
                }else{
                    cb(null)
                }
            })
        }]

    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null)
        }
    })
}




const editVendorTruck = function(payloadData,callback){
    let vendorId= null;
    let  truckId;
    let rcPic;
    let fitnessCertificate;
    let pollutionPic;
    let insurancePic;
    let driverId;
    if(payloadData.startCityId == payloadData.endCityId){
        return callback('Start end End Cities should be different. ')
    }

    async.auto({
        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.VendorService.getVendor(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        vendorId = result[0]._id;
                         console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP',payloadData);
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })

        },
        checkTruckId:['checkForUserExists',function(cb){
            var criteria={
                _id:payloadData.truckId
            }

            var projection={}

            var options={}

            Service.TruckService.getTruck(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP=====checkTruckId');
                        cb(null)
                    }else{
                        cb('Invalid Truck Id.')
                    }
                }
            })
        }],
        checkVendorTruckId:['checkForUserExists',function(cb){
             var criteria={
                _id:payloadData.id,
                vendorId:vendorId
            }

            var projection={}

            var options={}

            Service.VendorTruckService.getVendorTruck(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                         console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP=====checkVendorTruckId');
                         driverId=result[0].driverId;
                        cb(null)
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_ID)
                    }
                }
            })
        }],
         checkDriverId:['checkVendorTruckId',function(cb){
             if(payloadData.driverId && (!driverId && payloadData.driverId!=driverId.toString())){
                  var criteria={
                    _id:payloadData.driverId
                }

                var projection={}

                var options={}

                Service.DriverService.getDriver(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result.length){
                            if(!result[0].truckAssign){
                                 console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP=====checkDriverId');
                                cb(null)
                            }else{
                                cb('Driver has already truck assigned.')
                            }
                           
                        }else{
                            cb('Invalid driverId.')
                        }
                    }
                })
             }else{
                 cb(null)
             }
            
        }],
         getEndCityLat:['checkForUserExists',function(cb){
            var criteria={
                _id:payloadData.endCityId,
                isDeleted:false
            }

            var projection={
                cityName:1,
                location:1
            }

            var options={}

            Service.CityService.getCity(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        payloadData.endLocationAddress=result[0].cityName;
                        payloadData.endLocation=result[0].location;
                         console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP=====getEndCityLat');
                        cb(null)
                    }else{
                        cb('Invalid End City Id.')
                    }
                }
            })    
        }],
        getStartCityLat:['checkForUserExists',function(cb){
            var criteria={
                _id:payloadData.startCityId,
                isDeleted:false
            }

            var projection={
                cityName:1,
                location:1
            }

            var options={}

            Service.CityService.getCity(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        payloadData.startLocationAddress=result[0].cityName;
                        payloadData.startLocation=result[0].location;
                         console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP=====getStartCityLat');
                        cb(null)
                    }else{
                        cb('Invalid Start City Id.')
                    }
                }
            })
        }],
        addTruck:['getStartCityLat','getEndCityLat','checkTruckId','checkDriverId',function(cb){
            console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP',payloadData);

            var criteria={
                _id:payloadData.id
            }

            let obj = {
                truckId:payloadData.truckId,
                vendorId:vendorId,
                truckNo:payloadData.truckNo,
                startLocation:payloadData.startLocation,
                startCityId:payloadData.startCityId,
                endLocation:payloadData.endLocation,
                endCityId:payloadData.endCityId,
                startAddress:payloadData.startLocationAddress,
                endAddress:payloadData.endLocationAddress,
                driverId:payloadData.driverId || null
            }


            var setQuery={
                $set:obj
            }

            var options={
                new:true
            }
            Service.VendorTruckService.updateVendorTruck(criteria,setQuery,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                   if(result && Object.keys(result).length){
                        console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP=====addTruck');
                       cb(null)
                   }else{
                       cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                   }
                }
            })
        }],
        rcPicUpload:['checkForUserExists',function(cb)
        {
            if(payloadData.rcPic &&(payloadData.rcPic.filename || payloadData.rcPic[1].filename)){
                UploadManager.uploadFile(payloadData.rcPic && payloadData.rcPic.filename ? payloadData.rcPic :payloadData.rcPic[1], Math.random()*1000, "hello", function (err, uploadedInfo) {
                    if (err) {
                        console.log("uploaded info rcPicUpload",err);
                        cb(err)
                    } else {
                        console.log("uploaded info", uploadedInfo);
                        rcPic = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                         console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP=====rcPicUpload');
                        cb(null);
                    }
                })
            }else{
                cb(null)
            }
        }],
        pollutionPic:['checkForUserExists',function(cb)
        {

            if(payloadData.pollutionPic && (payloadData.pollutionPic.filename || payloadData.pollutionPic[1].filename)){
                UploadManager.uploadFile(payloadData.pollutionPic && payloadData.pollutionPic.filename ? payloadData.pollutionPic :payloadData.pollutionPic[1], Math.random()*1000, "hello", function (err, uploadedInfo) {
                    if (err) {
                        console.log("uploaded info pollutionPic",err);
                        cb(err)
                    } else {
                        console.log("uploaded info", uploadedInfo);
                        pollutionPic = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                         console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP=====pollutionPic');
                        cb(null);
                    }
                })
            }else{
                cb(null)
            }
        }],
        insurancePic:['checkForUserExists',function(cb)
        {
            if(payloadData.insurancePic && (payloadData.insurancePic.filename || payloadData.insurancePic[1].filename)){
                 UploadManager.uploadFile(payloadData.insurancePic && payloadData.insurancePic.filename ?payloadData.insurancePic :payloadData.insurancePic[1], Math.random()*1000, "hello", function (err, uploadedInfo) {
                if (err) {
                    console.log("uploaded info insurancePic",err);
                    cb(err)
                } else {
                    console.log("uploaded info", uploadedInfo);
                    insurancePic = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                     console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP=====insurancePic');
                    cb(null);
                }
            })
            }else{
                cb(null)
            }
             
        }],
        fitnessCertificate:['checkForUserExists',function(cb)
        {
            if(payloadData.fitnessCertificate &&(payloadData.fitnessCertificate.filename || payloadData.fitnessCertificate[1].filename)){
                UploadManager.uploadFile(payloadData.fitnessCertificate && payloadData.fitnessCertificate.filename ?payloadData.fitnessCertificate : payloadData.fitnessCertificate[1], Math.random()*1000, "hello", function (err, uploadedInfo) {
                    if (err) {
                        console.log("uploaded info fitnessCertificate",err);
                        cb(err)
                    } else {
                        console.log("uploaded info fitnessCertificate");
                        fitnessCertificate = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                         console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP=====fitnessCertificate');
                        cb(null);
                    }
                })
            }else{
                cb(null)
            }
        }],
        uploadImage:['addTruck','rcPicUpload','pollutionPic','insurancePic','fitnessCertificate',function(cb){
            console.log('=====================================================')
            var documents = [];

            if(fitnessCertificate){
                documents.push({
                    "documentType":"FITNESS",
                    "documentImage":fitnessCertificate
                });
            }

            if(insurancePic){
                documents.push({
                    "documentType":"INSURANCE",
                    "documentImage":insurancePic
                });
            }

            if(pollutionPic){
                documents.push({
                    "documentType":"POLLUTION",
                    "documentImage":pollutionPic
                });
            }

            if(rcPic){
                documents.push({
                    "documentType":"REGISTRATION",
                    "documentImage":rcPic
                });
            }
            
            let query = {
                _id:payloadData.id
            }
            console.log(".......truckId............",truckId);
            console.log(".......documents............",documents);

            var options={
                new:true
            }

            var setQuery={
                $set:{
                    documents:documents
                }
            }

            Service.VendorTruckService.updateVendorTruck(query,setQuery,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                     console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP=====uploadImage');

                     if(result && Object.keys(result).length){
                          cb(null)
                     }else{
                         cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                     }
                   
                }
            })
        }]

    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null)
        }
    })
}




const listGlobalTruck = function(callback){
    let query = {};
    let options = {};
    let projections = {};
    Service.TruckService.getTruck(query,projections,options,function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,result)
        }
    })
}

const myTruck = function(queryData,callback){
    let vendorId;
    let data = []; 
    async.auto({
        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: queryData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.VendorService.getVendor(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        vendorId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })},
        listTruck:['checkForUserExists',function(cb){
            let query = {
                vendorId:vendorId
            };
            let options = {lean:true};
            let projections = {};
                var populate = [
                {
                    path: 'driverId',
                    match: {},
                    select: {},
                    options: {}
                },
                 {
                    path: 'truckId',
                    match: {},
                    select: "",
                    options: {}
                },
                {
                    path: 'startCityId',
                    match: {},
                    select: "",
                    options: {}
                },
                {
                    path: 'endCityId',
                    match: {},
                    select: "",
                    options: {}
                }]
            Service.VendorTruckService.getVendorTruckPopulate(query,projections,options,populate,function(err,result){
                if(err){
                    cb(err)
                }else{
                    data = result;
                    cb(null)
                }
            })
            
        }]
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,data)
        }
    })
}

const createPassword = function(queryData,callback){
    let vendorId;
    async.auto({
        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: queryData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.VendorService.getVendor(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        vendorId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })},
        checkDriverForExists: function (cb) {
            var criteria = {
                _id: queryData.driverId
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.DriverService.getDriver(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DRIVER_NOT_FOUND);
                    }
                }
            })},
        changePassword:['checkForUserExists','checkDriverForExists',function(cb){
            var criteria = {
                _id: queryData.driverId,
                vendorId:vendorId
            };
            var setData = {
                password :UniversalFunctions.CryptData(queryData.password)
            };
            var option = {
                lean: true
            };
            Service.DriverService.updateDriver(criteria,setData,option,function(err,result){
                if(err){
                    cb(err)
                }else{
                    cb(null)
                }
            })
        }]


    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null)
        }
    })
}


const assignDriverToTruck = function(payloadData,callback){
    let vendorId;
    let globalTruck;
    async.auto({
        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.VendorService.getVendor(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        vendorId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })},
            checkTruckId:['checkForUserExists',function(cb){
                var criteria={
                    _id:payloadData.truckId
                }
                var projection={
                    truckId:1
                }
                var options={}

                Service.VendorTruckService.getVendorTruck(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err);
                    }else{
                        if(result.length){
                            globalTruck=result[0].truckId;
                            cb(null);
                        }else{
                            cb("Invalid truck Id.")
                        }
                    }
                })
            }],
        assignTruckDriver:['checkTruckId',function(cb){
            let query = {_id:payloadData.truckId};
            let setData ={
                driverId:payloadData.driverId
            };
            let option ={lean:true}

            Service.VendorTruckService.updateVendorTruck(query,setData,option,function(err,result){
                if(err){
                    cb(err)
                }else{
                    cb(null)
                }
            })
        }],
        assignDriverTruck:['checkTruckId',function(cb){
            let query = {_id:payloadData.driverId};
            let setData ={
                VendorTruck:payloadData.truckId,
                truckAssign:true,
                truckId:globalTruck
            };
            let option ={lean:true}

            Service.DriverService.updateDriver(query,setData,option,function(err,result){
                if(err){
                    cb(err)
                }else{
                    cb(null)
                }
            })
        }],


    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null)
        }
    })
}

const notAssignTruckDriver = function(payloadData,callback){
   let  vendorId;
   let data = [];
    async.auto({
        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.VendorService.getVendor(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        vendorId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })},
        findFreeDriver:['checkForUserExists',function(cb){
            let query = {
                vendorId:vendorId,
                truckAssign:false,
                VendorTruck:null,

            }
            let options = {lean:true};
            let projections = {
                name:1,
                _id:1,
                address:1,
                phoneNumber:1
            }
            Service.DriverService.getDriver(query,projections,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    data = result;
                    cb(null)
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,data)
        }
    })
}

const driverLocation = function(payloadData,callback){

    console.log("hello");
    let vendorId;
    let data = [];
    let objects=[];
    async.auto({
        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.VendorService.getVendor(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        vendorId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })},
        getDriver:['checkForUserExists',function(cb){

            let query = {
                vendorId:vendorId,
                truckAssign:true
            };
            let truck={}

             if(payloadData.truckStatus){
                truck.status=payloadData.truckStatus
            }

            if('status' in payloadData){
                query.currentBooking=payloadData.status;
            }

            if(payloadData.driverId){
                query._id=payloadData.driverId
            }


            let options = {lean:true};
            let projections = {
                currentLocation :1,
                currentBooking :1,
                name:1,
                VendorTruck:1
            };

            var populate = [
                {
                    path: 'VendorTruck',
                    match: truck,
                    select: "status",
                    options: {}
                }
            ];

            Service.DriverService.getDriverPopulate(query,projections,options,populate,function(err,result){
                if(err){
                    cb(err)
                }else{
                     async.each(result,function(obj,cl){

                          if(obj.VendorTruck){
                            obj.currentLocation.push({
                                currentBooking:obj.currentBooking,
                                name:obj.name,
                                truckStatus:obj.VendorTruck  && obj.VendorTruck.status
                            })
                            data.push(obj.currentLocation);
                        }
                        cl(null);
                    },function(err,res){
                          cb(err)
                    })
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,{locations:data,details:objects})
        }
    })
}




const bookingList = function(payloadData,callback){
    let data = [];
    let vendorId;
    let truckIds=[];
    var isDate=false;
   if(payloadData.startDate && payloadData.endDate){
       isDate=true;
   }
    async.auto({
        checkForUserExists: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.VendorService.getVendor(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        vendorId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            })},
        getVendorTruckIds:['checkForUserExists',function(cb){
            var criteria={
                vendorId:vendorId
            }

            var projection={
                _id:1
            }
            var options=-{}

            Service.VendorTruckService.getVendorTruck(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        truckIds=result.map(function(id){
                           return mongoose.Types.ObjectId(id._id);
                        })
                        cb();
                    }else{
                        return callback(null,data)
                    }
                }
            })

        }],
        bookingLisitng:['checkForUserExists','getVendorTruckIds',function(cb){
            let query = {};
            let date;
           
           
            if(payloadData.status == 'ALL'){
                query = {};
            } else if(payloadData.status == 'ON_THE_WAY'){
                query = {
                    status:
                        {
                            $in: ['TOWARDS_UNLOADING_LOCATION','LOADING_LOCATION','UNLOADING_LOCATION']
                        }
                };
            } else {
                query = {
                    status:payloadData.status
                };
            }

            if(payloadData.bookingId){
                query._id=payloadData.bookingId;
            }

            console.log('TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT',truckIds)
            if(isDate) 
            {
                query.bookingDate={
                    $gt:payloadData.startDate,
                    $lt:payloadData.endDate
                }
            }
            if(payloadData.driverId){
                query.driverId=payloadData.driverId;
            }
            if(payloadData.truckId){
                console.log('lllllllllllllll')
                query.truckId=payloadData.truckId;
            }else{
                query.truckId={$in:truckIds}
            }
            let projection = {
                 materials:1,
                companyId:1,
                contractId:1,
                pickupLocationId:1,
                dropOffLocationId:1,
                driverId:1,
                bookingDate:1,
                status:1,
                rejection:1,
                locationLogs:1,
                unloadInfo:1,
                bookingId:1,
                acceptTime:1,
                loadLocationReachTime:1,
                startTime:1,
                unloadLocationReachTime:1,
                endTime:1,
                loadingInfo:1
            };

            let options = {
                lean:true
            }

           
                var populate = [
               {
                    path: 'materials',
                    model:'Materials',
                    match: {},
                    select: {
                        materialName:1,
                        length:1,
                        breadth:1,
                        height:1,
                         accessionNo:1,
                        capacity:1
                    },
                    options: {
                        lean:true
                    }
                },
                   {
                    path: 'unloadInfo.materialId',
                    model:'Materials',
                    match: {},
                    select: {
                        materialName:1,
                        length:1,
                        breadth:1,
                        height:1,
                         accessionNo:1,
                        capacity:1
                    },
                    options: {
                        lean:true
                    }
                },
                 {
                    path: 'loadingInfo.materialId',
                    model:'Materials',
                    match: {},
                    select: {
                        materialName:1,
                        length:1,
                        breadth:1,
                        height:1,
                         accessionNo:1,
                        capacity:1
                    },
                    options: {
                        lean:true
                    }
                },
                {
                    path: 'companyId',
                    match: {},
                    select: {
                        name:1
                    },
                    options: {
                        lean:true
                    }
                },
               {
                    path: 'contractId',
                    match: {
                        
                    },
                    select: {
                        truckType:1,
                        freightAmount:1,
                        detentionCharges:1,
                        billingPeriod:1,
                        startCityId:1,
                        endCityId:1,
                        truckId:1
                    },
                    options: {}
                },
                 {
                    path: 'pickupLocationId',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1,
                        authPerson: 1,
                        phoneNumber: 1,
                    },
                    options: {
                        lean:true
                    }
                },
                 {
                    path: 'dropOffLocationId',
                    match: {},
                    select: {
                        pointName:1,
                        location:1,
                        address:1,
                        authPerson: 1,
                        phoneNumber: 1,
                    },
                    options: {
                        lean:true
                    }
                },
                   {
                    path: 'driverId',
                    match: {},
                    select: {
                        name:1,
                        dob:1,
                        countryCode:1,
                        address:1,
                        pincode:1,
                        city:1,
                        state:1,
                        phoneNumber:1,
                        email:1,
                        currentLocation:1,
                        profilePicURL:1,
                        truckId:1,
                        vendorId:1,
                        VendorTruck:1,
                        currentBooking:1,
                        truckAssign:1

                    },
                    options: {}
                }
                ]  

            Service.BookingService.getBookingsPopulate(query,projection,options,populate,function(err,result){
                if(err){
                    cb(err)
                } else {
                    if(result && result.length){
                        data = result;
                        cb(null)
                    } else {
                        return callback(null,data)
                       
                    }
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null,data)
        }
    })
}


var getProfileData=function(queryData,callback){
    var criteria={
        _id:queryData.id
    }

    var projection={
        name:1,
        email:1,
        countryCode:1,
        contactNo:1,
        address:1,
        aadharNumber:1,
        aadharCardImage:1,
        panNumber:1,
        panCardImage:1,
        city:1,
        state:1,
        pincode:1,
        emailNotifications:1,
        smsNotifications:1,
        pushNotifications
    }

    var options={
        lean:true
    }

    Service.VendorService.getVendor(criteria,projection,options,function(err,result){
        if(err){
            callback(err)
        }else{
            if(result.length){
                callback(null,result[0])
            }else{
                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }
    })
}






var verifyVendorEmail=function(payloadData,callback){

    if(payloadData.userType=='VENDOR'){
        let vendorId;
        async.auto({
            chckUser:function(cb){
                var criteria={
                    accessToken:payloadData.accessToken
                }

                var projection={
                    _id:1
                }

                var options={
                    lean:true
                }

                Service.VendorService.getVendor(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result.length){
                            vendorId=result[0]._id;
                            cb(null)
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN)
                        }
                    }
                })
            },
            updateVendorDb:['chckUser',function(cb){
                 var criteria={
                    _id:vendorId
                }

                var setQuery={
                    emailVerify:true
                }

                var options={
                    new:true
                }

                Service.VendorService.updateVendor(criteria,setQuery,options,function(err,result){
                    cb(err);
                })

            }]
        },function(err,result){
            callback(err,{});
        })
    }else if(payloadData.userType=='COMPANY'){
        let companyId;
        async.auto({
            chckUser:function(cb){
                var criteria={
                    accessToken:payloadData.accessToken
                }

                var projection={
                    _id:1
                }

                var options={
                    lean:true
                }

                Service.CompanyService.getCompany(criteria,projection,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        if(result.length){
                            companyId=result[0]._id;
                            cb(null)
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN)
                        }
                    }
                })
            },
            updateCompanyInDb:['chckUser',function(cb){
                 var criteria={
                    _id:companyId
                }

                var setQuery={
                    emailVerify:true
                }

                var options={
                    new:true
                }

                Service.CompanyService.updateCompany(criteria,setQuery,options,function(err,result){
                    cb(err);
                })

            }]
        },function(err,result){
            callback(err,{});
        })
    }else{
        // async.auto({

        // },function(err,result){
        //     callback(err,{});
        // })
        callback(null);
    }
   
}


var updateVendorBlockStatus=function(payloadData,callback){
    let oldStatus;

    async.auto({
        checkVendorId:function(cb){
            var criteria={
                _id:payloadData.vendorId,
                isDeleted:false
            }

            var projection={
                isBlocked:1
            }

            var options={}

            Service.VendorService.getVendor(criteria,projection,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        oldStatus=result[0].isBlocked || false;
                        if(oldStatus==payloadData.block){
                            cb('Sorry Vendor already '+(payloadData.block?'BLOCKED':'UNBLOCKED')+' .')
                        }else{
                             cb(null);
                        }
                       
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_ID)
                    }
                }
            })
        },
        updateStatus:['checkVendorId',function(cb){
            var criteria={
                _id:payloadData.vendorId,
                isDeleted:false
            }

            var setQuery={
                isBlocked:payloadData.block
            }

            var options={
                new:true
            }

            Service.VendorService.updateVendor(criteria,setQuery,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result && Object.keys(result).length){
                        cb(null)
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                    }
                }
            })
        }]
    },function(err,result){
        callback(err,{blockStatus:payloadData.block});
    })
}


module.exports = {
    vendorSignup : vendorSignup,
    verifyOTP:verifyOTP,
    completeProfile:completeProfile,
    vendorLogin:vendorLogin,
    addDriver:addDriver,
    listDrivers:listDrivers,
    addVendorTruck:addVendorTruck,
    listGlobalTruck:listGlobalTruck,
    myTruck:myTruck,
    createPassword:createPassword,
    assignDriverToTruck:assignDriverToTruck,
    notAssignTruckDriver:notAssignTruckDriver,
    driverLocation:driverLocation,
    bookingList:bookingList,
    getProfileData:getProfileData,
    editVendorTruck:editVendorTruck,
    editProfile:editProfile,
    verifyVendorEmail:verifyVendorEmail,
    updateVendorBlockStatus:updateVendorBlockStatus,
    updateDriver:updateDriver
};
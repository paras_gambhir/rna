'use strict';

var SERVER = {
    APP_NAME: 'RNA',
    PORTS: {
        HAPI: 8000
    },
    TOKEN_EXPIRATION_IN_MINUTES: 600,
    JWT_SECRET_KEY: 'hello',
    GOOGLE_API_KEY : '',
    COUNTRY_CODE : '+91',
    MAX_TIME:120,
    MAX_TIME_TO_SEND_REQUEST:20,
    MAX_DISTANCE_RADIUS_TO_SEARCH : 0.0007839251445558, //in radians   5km
    THUMB_WIDTH : 50,
    THUMB_HEIGHT : 50,
    BASE_DELIVERY_FEE : 25,
    COST_PER_KM: 9, // In USD
    DOMAIN_NAME : 'http://localhost:8000/',
    SUPPORT_EMAIL : 'paras5217@gmail.com',
    DEFAULT_IMAGE: 'http://gmldnigeria.org/images/person_default_page.jpg',
    FCM_KEY_FOR_CUSTOMER : 'AIzaSyBMEFdviolFu6Ooxzu6N_sgauv8D-Q2Dyw',
    FCM_KEY_FOR_DRIVER:'AIzaSyAjPotzpyiq0v_S3H6d0Yy8QH_vZjzmVoY',
    TIME_PER_KM: 1
};

var DATABASE = {
    ADMIN_EMAIL:'prince@gmail.com',
    PROFILE_PIC_PREFIX : {
        ORIGINAL : 'profilePic_',
        THUMB : 'profileThumb_'
    },
    LOGO_PREFIX : {
        ORIGINAL : 'logo_',
        THUMB : 'logoThumb_'
    },
    PERSON_TYPE : {
        CUSTOMER : 'CUSTOMER',
        DRIVER : 'DRIVER'
    },
    DOCUMENT_PREFIX : 'document_',
    USER_ROLES: {
        ADMIN: 'ADMIN',
        COMPANY: 'COMPANY',
        DRIVER: 'DRIVER',
        VENDOR:'VENDOR',
        LOADER:'LOADER'
    },
    FILE_TYPES: {
        LOGO: 'LOGO',
        DOCUMENT: 'DOCUMENT',
        OTHERS: 'OTHERS'
    },
    VEHICLE_TYPE: {
        SUX: 'SUX',
        LUXURY: 'LUXURY',
        SHOFER: 'SHOFER',
        NONE:'NONE'
    },
    NAVIGATION_TYPE: {
        GOOGLE: 'GOOGLE',
        WAZE: 'WAZE'
    },
    DEVICE_TYPES: {
        IOS: 'IOS',
        ANDROID: 'ANDROID'
    },
    FAVORITE_LOCATION_TYPE: {
        HOME: 'HOME',
        WORK: 'WORK'
    },
    LANGUAGE: {
        EN: 'EN',
        ES_MX: 'ES_MX'
    },
    PAYMENT_OPTIONS : {
        CREDIT_DEBIT_CARD : 'CREDIT_DEBIT_CARD',
        PAYPAL : 'PAYPAL',
        BITCOIN : 'BITCOIN',
        GOOGLE_WALLET : 'GOOGLE_WALLET',
        APPLE_PAY : 'APPLE_PAY',
        EIYA_CASH : 'EIYA_CASH'
    },
    DOCUMENT_TYPE : {
        DRIVING_LICENSE : 'DRIVING_LICENSE',
        INSURANCE : 'INSURANCE',
        REGISTRATION : 'REGISTRATION',
        ADHAR:'ADHAR',
        PAN:'PAN',
        FITNESS:'FITNESS',
        POLLUTION:'POLLUTION'
    },
    RIDE_STATUS : {
        PENDING : 'PENDING',
        NO_DRIVER_FOUND : 'NO DRIVER FOUND',
        ACCEPTED : 'ACCEPTED',
        REJECTED : 'REJECTED',
        STARTED:'STARTED',
        LOADING_LOCATION:'LOADING_LOCATION',
        TOWARDS_UNLOADING_LOCATION:'TOWARDS UNLOADING LOCATION',
        UNLOADING_LOCATION:'UNLOADING_LOCATION',
        ENDED:'ENDED',
        CANCELLED_BY_USER:'CANCELLED_BY_USER',
        TIMEOUT:'TIMEOUT',
        FILLED_BY_LOADER:'FILLED_BY_LOADER',
        UNLOADING_FORM_FILLED:'UNLOADING_FORM_FILLED',
        OTP_CONFIRMED:'OTP_CONFIRMED',
        ON_LOADING_LOCATION:'ON LOADING LOCATION',
        ON_UNLOADING_LOCATION:'ON UNLOADING LOCATION'

    },
    PROMO_TYPE : {
        NO_EXPIRY:'NO_EXPIRY',
        EXPIRY:'EXPIRY'
    },
    OFFER_TYPE : {
        PERCENTAGE:'PERCENTAGE',
        FIXED:'FIXED',
        RIDES:'RIDES'
    },
    DRIVER_STATUS : {
        ONLINE:'ONLINE',
        OFFLINE:'OFFLINE'
    }
};

var STATUS_MSG = {
    ERROR: {
        VERIFY_OTP: {
            statusCode:400,
            customMessage : 'OTP is not verified. Please verify OTP first',
            type : 'VERIFY_OTP'
        },
        COMPLETE_PROFILE: {
            statusCode:400,
            customMessage : 'Please complete your profile first',
            type : 'COMPLETE_PROFILE'
        },
        INVALID_OTP: {
            statusCode:400,
            type: 'INVALID_OTP',
            customMessage : 'Invalid OTP'
        },
        INVALID_OPERATION: {
            statusCode:401,
            type: 'INVALID_OPERATION',
            customMessage : 'Invalid operation please verify.'
        },
        USER_EXISTS: {
            statusCode:401,
            type: 'USER_EXISTS',
            customMessage : 'User Exists. Please Enter the Password'
        },
        VENDOR_EMAIL_EXISTS: {
            statusCode:401,
            type: 'VENDOR_EMAIL_EXISTS',
            customMessage : 'This Account is already registered. Please Choose a different Email'
        },
        COMPANY_EMAIL_EXISTS: {
            statusCode:401,
            type: 'COMPANY_EMAIL_EXISTS',
            customMessage : 'This Account is already registered. Please Choose a different Email'
        },
        DRIVER_EMAIL_EXISTS: {
            statusCode:401,
            type: 'DRIVER_EMAIL_EXISTS',
            customMessage : 'This Account is already registered. Please Choose a different Email'
        },
         LOADER_EMAIL_EXISTS: {
            statusCode:401,
            type: 'LOADER_EMAIL_EXISTS',
            customMessage : 'This Account is already registered. Please Choose a different Email'
        },
        DRIVER_NO_EXISTS: {
            statusCode:401,
            type: 'DRIVER_NO_EXISTS',
            customMessage : 'Invalid Driver Id'
        },
         LOADER_PHONE_NO_EXISTS: {
            statusCode:401,
            type: 'LOADER_PHONE_NO_EXISTS',
            customMessage : 'Contact No already exist for another loader.'
        },
        VENDOR_NO_EXISTS: {
            statusCode:401,
            type: 'VENDOR_NO_EXISTS',
            customMessage : 'This Account is already registered. Please Choose a different Phone Number'
        },
        COMPANY_NO_EXISTS: {
            statusCode:401,
            type: 'COMPANY_NO_EXISTS',
            customMessage : 'This Account is already registered. Please Choose a different Phone Number'
        },
        DRIVER_USER_EXISTS: {
            statusCode:401,
            type: 'DRIVER_EXISTS',
            customMessage : 'This Account is registered as Driver. Please Choose a different Phone Number'
        },
        FAULTY_CARD: {
            statusCode:400,
            customMessage : 'Some error with the credit card',
            type : 'ERROR_WITH_CREDIT_CARD'
        },
        INVALID_USER_PASS: {
            statusCode:401,
            type: 'INVALID_USER_PASS',
            customMessage : 'Invalid username or password'
        },
        TOKEN_EXPIRED: {
            statusCode:402,
            customMessage : 'Token Expired',
            type : 'TOKEN_EXPIRED'
        },
        DB_ERROR: {
            statusCode:400,
            customMessage : 'DB Error : ',
            type : 'DB_ERROR'
        },
        ADD_CARD: {
            statusCode:400,
            customMessage : 'Please add your credit card details',
            type : 'ADD_CREDIT_CARD'
        },
        INVALID_ID: {
            statusCode:400,
            customMessage : 'Invalid Id Provided. ',
            type : 'INVALID_ID'
        },

         INVALID_RECEIVER_ID: {
            statusCode:400,
            customMessage : 'Invalid receiverId provided. ',
            type : 'INVALID_ID'
        },
        APP_ERROR: {
            statusCode:400,
            customMessage : 'Application Error',
            type : 'APP_ERROR'
        },
        ADDRESS_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Address not found',
            type : 'ADDRESS_NOT_FOUND'
        },
        SAME_ADDRESS_ID: {
            statusCode:400,
            customMessage : 'Pickup and Delivery Address Cannot Be Same',
            type : 'SAME_ADDRESS_ID'
        },
        PICKUP_ADDRESS_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Pickup Address not found',
            type : 'PICKUP_ADDRESS_NOT_FOUND'
        },
        DELIVERY_ADDRESS_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Delivery Address not found',
            type : 'DELIVERY_ADDRESS_NOT_FOUND'
        },
        IMP_ERROR: {
            statusCode:500,
            customMessage : 'Implementation Error',
            type : 'IMP_ERROR'
        },
        APP_VERSION_ERROR: {
            statusCode:400,
            customMessage : 'One of the latest version or updated version value must be present',
            type : 'APP_VERSION_ERROR'
        },
        INVALID_TOKEN: {
            statusCode:400,
            customMessage : 'Invalid token provided',
            type : 'INVALID_TOKEN'
        },
        INVALID_TRUCK_NO: {
            statusCode:402,
            customMessage : 'Invalid truck no provided',
            type : 'INVALID_TRUCK'
        },
         INVALID_TRUCK_ID: {
            statusCode:402,
            customMessage : 'Invalid truck id provided',
            type : 'INVALID_TRUCK_ID'
        },
        INVALID_BOOKING_ID_OR_LOCATION: {
            statusCode:402,
            customMessage : 'Invalid Booking id or loading location provided',
            type : 'INVALID_BOOKING_ID_OR_LOCATION'
        },
        INVALID_BOOKING_ID: {
            statusCode:400,
            customMessage : 'Invalid Booking id provided.',
            type : 'INVALID_BOOKING_ID'
        },
         INVALID_DRIVER_ID: {
            statusCode:400,
            customMessage : 'Invalid driver id provided.',
            type : 'INVALID_DRIVER_ID'
        },
         INVALID_DROP_LOCATION: {
            statusCode:400,
            customMessage : 'Invalid drop location provided.',
            type : 'INVALID_DROP_LOCATION'
        },
         INVALID_CITY_ID: {
            statusCode:402,
            customMessage : 'Invalid City Id',
            type : 'INVALID_CITY_ID'
        },
        
          INVALID_LOCATION_ID: {
            statusCode:402,
            customMessage : 'Invalid location Id.',
            type : 'INVALID_LOCATION_ID'
        },
         INVALID_LOADER_ID: {
            statusCode:402,
            customMessage : 'Invalid Loader Id',
            type : 'INVALID_LOADER_ID'
        },
        INVALID_CODE: {
            statusCode:400,
            customMessage : 'Invalid Verification Code',
            type : 'INVALID_CODE'
        },
        INVALID_CONTRACT_ID: {
            statusCode:400,
            customMessage : 'Invalid Contract Id',
            type : 'INVALID_CONTRACT_ID'
        },
        DEFAULT: {
            statusCode:400,
            customMessage : 'Error',
            type : 'DEFAULT'
        },
        PHONE_NO_EXIST: {
            statusCode:400,
            customMessage : 'Phone No Already Exist',
            type : 'PHONE_NO_EXIST'
        },
        EMAIL_EXIST: {
            statusCode:400,
            customMessage : 'Email Already Exist',
            type : 'EMAIL_EXIST'
        },
        NO_DRIVER_FOUND: {
            statusCode:400,
            customMessage : 'No driver found.',
            type : 'NO_DRIVER_FOUND'
        },
        DUPLICATE: {
            statusCode:400,
            customMessage : 'Duplicate Entry',
            type : 'DUPLICATE'
        },
        DUPLICATE_ADDRESS: {
            statusCode:400,
            customMessage : 'Address Already Exist',
            type : 'DUPLICATE_ADDRESS'
        },
          DUPLICATE_CONTACT: {
            statusCode:400,
            customMessage : 'Contact no is already exist.',
            type : 'DUPLICATE_CONTACT'
        },
        UNIQUE_CODE_LIMIT_REACHED: {
            statusCode:400,
            customMessage : 'Cannot Generate Unique Code, All combinations are used',
            type : 'UNIQUE_CODE_LIMIT_REACHED'
        },
        INVALID_REFERRAL_CODE: {
            statusCode:400,
            customMessage : 'Invalid Referral Code',
            type : 'INVALID_REFERRAL_CODE'
        },
        FACEBOOK_ID_PASSWORD_ERROR: {
            statusCode:400,
            customMessage : 'Only one field should be filled at a time, either facebookId or password',
            type : 'FACEBOOK_ID_PASSWORD_ERROR'
        },
        INVALID_EMAIL: {
            statusCode:400,
            customMessage : 'Invalid Email Address',
            type : 'INVALID_EMAIL'
        },
        PASSWORD_REQUIRED: {
            statusCode:400,
            customMessage : 'Password is required',
            type : 'PASSWORD_REQUIRED'
        },
        INVALID_COUNTRY_CODE: {
            statusCode:400,
            customMessage : 'Invalid Country Code, Should be in the format +52',
            type : 'INVALID_COUNTRY_CODE'
        },
         INVALID_COMPANY_ID: {
            statusCode:400,
            customMessage : 'Invalid CountryId Provided.',
            type : 'INVALID_COMPANY_ID'
        },
        INVALID_PHONE_NO_FORMAT: {
            statusCode:400,
            customMessage : 'Phone no. cannot start with 0',
            type : 'INVALID_PHONE_NO_FORMAT'
        },
        COUNTRY_CODE_MISSING: {
            statusCode:400,
            customMessage : 'You forgot to enter the country code',
            type : 'COUNTRY_CODE_MISSING'
        },
        INVALID_PHONE_NO: {
            statusCode:400,
            customMessage : 'Phone No. & Country Code does not match to which the OTP was sent',
            type : 'INVALID_PHONE_NO'
        },
        PHONE_NO_MISSING: {
            statusCode:400,
            customMessage : 'You forgot to enter the phone no.',
            type : 'PHONE_NO_MISSING'
        },
        NOTHING_TO_UPDATE: {
            statusCode:400,
            customMessage : 'Nothing to update',
            type : 'NOTHING_TO_UPDATE'
        },
        NOT_FOUND: {
            statusCode:400,
            customMessage : 'User Not Found',
            type : 'NOT_FOUND'
        },
        INVALID_RESET_PASSWORD_TOKEN: {
            statusCode:400,
            customMessage : 'Invalid Reset Password Token',
            type : 'INVALID_RESET_PASSWORD_TOKEN'
        },
        INCORRECT_PASSWORD: {
            statusCode:401,
            customMessage : 'Incorrect Password',
            type : 'INCORRECT_PASSWORD'
        },
        INCORRECT_OTP: {
            statusCode:401,
            customMessage : 'Incorrect OTP',
            type : 'INCORRECT_OTP'
        },
        NOT_APPROVED: {
            statusCode:401,
            customMessage : 'Account is not approved yet, Please wait for confirmation from client',
            type : 'NOT_APPROVED'
        },
        EMPTY_VALUE: {
            statusCode:400,
            customMessage : 'Empty String Not Allowed',
            type : 'EMPTY_VALUE'
        },
        PHONE_NOT_MATCH: {
            statusCode:400,
            customMessage : "Phone No. Doesn't Match",
            type : 'PHONE_NOT_MATCH'
        },
        SAME_PASSWORD: {
            statusCode:400,
            customMessage : 'Old password and new password are same',
            type : 'SAME_PASSWORD'
        },
        ACTIVE_PREVIOUS_SESSIONS: {
            statusCode:400,
            customMessage : 'You already have previous active sessions, confirm for flush',
            type : 'ACTIVE_PREVIOUS_SESSIONS'
        },
        EMAIL_ALREADY_EXIST: {
            statusCode:400,
            customMessage : 'Email Address Already Exists',
            type : 'EMAIL_ALREADY_EXIST'
        },
        ERROR_PROFILE_PIC_UPLOAD: {
            statusCode:400,
            customMessage : 'Profile pic is not a valid file',
            type : 'ERROR_PROFILE_PIC_UPLOAD'
        },
        PHONE_ALREADY_EXIST: {
            statusCode:400,
            customMessage : 'Phone No. Already Exists',
            type : 'PHONE_ALREADY_EXIST'
        },
        EMAIL_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Email Not Found',
            type : 'EMAIL_NOT_FOUND'
        },
        FACEBOOK_ID_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Facebook Id Not Found',
            type : 'FACEBOOK_ID_NOT_FOUND'
        },
        PHONE_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Phone No. Not Found',
            type : 'PHONE_NOT_FOUND'
        },
        INCORRECT_OLD_PASS: {
            statusCode:400,
            customMessage : 'Incorrect Old Password',
            type : 'INCORRECT_OLD_PASS'
        },
        UNAUTHORIZED: {
            statusCode:401,
            customMessage : 'You are not authorized to perform this action',
            type : 'UNAUTHORIZED'
        },
        NO_POST_FOUND: {
            statusCode:401,
            customMessage : 'no post found',
            type : 'NO_POST_FOUND'
        },
        ACCOUNT_EXISTS: {
            statusCode:401,
            type: 'ACCOUNT_EXISTS',
            customMessage : 'Account already Exists'
        },
        USERNAME_EXISTS: {
            statusCode:401,
            type: 'USERNAM_EXISTS',
            customMessage : 'UserName already Exists'
        },
        PHONE_EXISTS: {
            statusCode:401,
            type: 'PHONE_EXISTS',
            customMessage : 'Phone Number already Exists'
        },
        ACCOUNT_NOT_REGISTERED: {
            statusCode:401,
            type: 'ACCOUNT_NOT_REGISTERED',
            customMessage : 'Your Account is not registered. Please sign up first!'
        },
        DRIVER_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Driver Not Found',
            type : 'DRIVER_NOT_FOUND'
        },
        DRIVER_ALREADY_APPROVE: {
            statusCode:400,
            customMessage : 'Driver Already Approved',
            type : 'DRIVER_ALREADY_APPROVE'
        },
    },
    SUCCESS: {
        OTP_SENT: {
            statusCode:201,
            customMessage : 'OTP Sent Successfully',
            type : 'OTP_SENT'
        },
        CREATED: {
            statusCode:201,
            customMessage : 'Created Successfully',
            type : 'CREATED'
        },
        CORRECT_OTP: {
            statusCode:201,
            customMessage : 'Correct OTP',
            type : 'Correct OTP'
        },
        MAIL_SENT: {
            statusCode:201,
            customMessage : 'New Password Sent Successfully',
            type : 'SENT'
        },
        DEFAULT: {
            statusCode:200,
            customMessage : 'Success',
            type : 'DEFAULT'
        },
        UPDATED: {
            statusCode:200,
            customMessage : 'Updated Successfully',
            type : 'UPDATED'
        },
        LOGOUT: {
            statusCode:200,
            customMessage : 'Logged Out Successfully',
            type : 'LOGOUT'
        },
        DELETED: {
            statusCode:200,
            customMessage : 'Deleted Successfully',
            type : 'DELETED'
        }
    }
};


var swaggerDefaultResponseMessages = [
    {code: 200, message: 'OK'},
    {code: 400, message: 'Bad Request'},
    {code: 401, message: 'Unauthorized'},
    {code: 404, message: 'Data Not Found'},
    {code: 500, message: 'Internal Server Error'}
];

var SCREEN_TO_SHOW = {
    HOMEPAGE : 'HOMEPAGE',
    TRACKING : 'TRACKING',
    FEEDBACK : 'FEEDBACK'
};

var notificationMessages = {
    verificationCodeMsg: 'Your 4 digit verification code for RNA is {{four_digit_verification_code}}',
    registrationEmail: {
        emailMessageVendor : "<img src='https://s3-us-west-2.amazonaws.com/vippy/193.56380636059444.jpg' height='100px' width='100px'/><br><br>Hello {{user_name}}, <br><br> Please  <a href='http://35.162.144.231/rna_vendor_panel/vendorpanel/#/login?token={{accessToken}}'>click here</a> to verify your email address.",
        emailMessageCompany : "<img src='https://s3-us-west-2.amazonaws.com/vippy/193.56380636059444.jpg' height='100px' width='100px'/><br><br>Hello {{user_name}}, <br><br> Please  <a href='http://35.162.144.231/rna_company/#/content/home?token={{accessToken}}'>click here</a> to verify your email address.",
        emailSubject: "Welcome to RNA"
    },
    contactDriverForm: {
        emailMessage : "A new driver has showed interest <br><br> Details : <br><br> Name : {{fullName}} <br><br> Email : {{email}} <br><br> Phone No : {{phoneNo}} <br><br> Vehicle Type : {{vehicleType}} <br><br> Bank Account : {{bankAccountBoolean}} <br><br> Heard From : {{heardFrom}}",
        emailSubject: "New Driver Contact Request"
    },
    contactBusinessForm: {
        emailMessage : "A new business has showed interest <br><br> Details : <br><br> Name : {{fullName}} <br><br> Email : {{email}} <br><br> Phone No : {{phoneNo}} <br><br> Business Name: {{businessName}} <br><br> Business Address: {{businessAddress}}  <br><br> Delivery Service : {{ownDeliveryService}} <br><br> Heard From : {{heardFrom}}",
        emailSubject: "New Business Contact Request"
    },
    forgotPassword: {
        emailMessage : "<img src='https://s3-us-west-2.amazonaws.com/vippy/193.56380636059444.jpg' height='100px' width='100px'/>  <br/> <br/> Dear {{user_name}}, <br><br>  Please  <a href='http://35.162.144.231/rna_company/#/content/home?token={{accessToken}}'>click here</a> to reset your password.",
        emailSubject: "Password Reset  For RNA"
    },
    noDriverFound: {
        emailMessage : "<img src='https://s3-us-west-2.amazonaws.com/vippy/193.56380636059444.jpg' height='100px' width='100px'/>  <br/> <br/> Dear Admin, <br><br>  No drivers are found for booking Id <b>{{bookingId}}</b> from <b> {{startAddress}} </b> to <b> {{endAddress}} </b>.",
        emailSubject: "No driver found"
    },
    postQuery: {
        emailMessage : "<img src='https://s3-us-west-2.amazonaws.com/vippy/193.56380636059444.jpg' height='100px' width='100px'/>  <br/> <br/> Dear Admin, <br><br>  <b>  {{companyName}} </b> company posted query for add new contract from <b> {{source}} </b> to <b> {{destination}} </b> of truck type <b> {{truckType}} </b>.",
        emailSubject: "Company query for add contract"
    }
};


var PAGES_LINK={
    EMAIL_VERIFICATION:'http://35.162.144.231/rna_vendor_panel/#/login?token={{accessToken}}',
    FORGOT_PASSWORD:'http://35.162.144.231/rna_vendor/#/newPassword?token={{accessToken}}'
}



var VENDOR_TRUCK={
    STATUS:{
        BOOKED:'BOOKED',
        VACANT:'VACANT',
        BROKE: 'BROKE'
    }
}

var languageSpecificMessages = {
    verificationCodeMsg : {
        EN : 'Your 4 digit verification code for Seed Project is {{four_digit_verification_code}}',
        ES_MX : 'Your 4 digit verification code for Seed Project is {{four_digit_verification_code}}'
    }
};

var APP_CONSTANTS = {
    SERVER: SERVER,
    DATABASE: DATABASE,
    SCREEN_TO_SHOW : SCREEN_TO_SHOW,
    STATUS_MSG: STATUS_MSG,
    notificationMessages: notificationMessages,
    languageSpecificMessages: languageSpecificMessages,
    swaggerDefaultResponseMessages: swaggerDefaultResponseMessages,
    VENDOR_TRUCK:VENDOR_TRUCK,
    PAGES_LINK:PAGES_LINK
};

module.exports = APP_CONSTANTS;

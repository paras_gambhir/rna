/**
 * Created by paras
 */

var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../Config');

module.exports = [
     {
        method: 'POST',
        path: '/api/loader/passwordLogin',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.LoaderController.loaderLogin(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'loader Login with password',
        tags: ['api', 'loader', 'login','with password'],
        validate: {
            payload: {
                countryCode: Joi.string().required().trim(),
                phoneNumber: Joi.string().required().trim(),
                password: Joi.string().required().trim(),
                deviceToken: Joi.string().optional(),
                appVersion: Joi.string().required(),
                deviceType: Joi.string().required().valid(
                    [UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID])
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},{
        method: 'POST',
        path: '/api/loader/OTPLogin',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.LoaderController.OTPLogin(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Loader Login with OTP',
        tags: ['api', 'loader', 'login','with otp'],
        validate: {
            payload: {
                countryCode: Joi.string().required().trim(),
                phoneNumber: Joi.string().required().trim(),
                OTP: Joi.number().required(),
                deviceToken: Joi.string().optional(),
                appVersion: Joi.string().required(),
                deviceType: Joi.string().required().valid(
                    [UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID])
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
 {
        method: 'POST',
        path: '/api/loader/sendOTP',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.LoaderController.sendOTP(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Send OTP on mobile',
        tags: ['api', 'loader', 'send otp'],
        validate: {
            payload: {
                countryCode: Joi.string().required().trim(),
                phoneNumber: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/loader/enterTruckNumber',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.LoaderController.enterTruckNumber(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Enter truck number',
        tags: ['api', 'loader', 'enter truck number'],
        validate: {
            payload: {
              truckNumber:Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/loader/selectLoadingLocation',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.LoaderController.selectLoadingLocation(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Select loading location',
        tags: ['api', 'loader', 'select loading location'],
        validate: {
            payload: {
                loadingLocationId:Joi.string().required(),
                bookingId: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/loader/fillForm',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.LoaderController.fillForm(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });
        }, config: {
        description: 'Fill form',
        tags: ['api', 'loader', 'fill form'],
        validate: {
            payload: {
                loadingLocationId:Joi.string().required(),
                dropOffLocationId:Joi.string().required(),
                bookingId: Joi.string().required(),
                materials:Joi.array().items(Joi.object().keys({
                    materialId: Joi.string().required(),
                    materialQuantity: Joi.string().required()
                }))

            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/loader/confirmOTP',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.LoaderController.confirmOTP(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Confirm OTP',
        tags: ['api', 'loader', 'confirm otp'],
        validate: {
            payload: {
                loadingLocationId:Joi.string().required(),
                bookingId: Joi.string().required(),
                OTP: Joi.number().required()

            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
  {
        method: 'GET',
        path: '/api/loader/bookingListing',
        handler: function (request, reply) {
            var payloadData = request.query;
              var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData.id) {
                payloadData.userId=userData.id;
                payloadData.assignedLocations=userData.assignedLocations || [];
                Controller.LoaderController.bookingListing(payloadData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        if(data.customMessage)
                        {
                            reply(UniversalFunctions.sendSuccess(data,{}))
                        }
                        else{
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }

                    }
                });
            }else{
                reply(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }, config: {

        auth:'LoaderAuth',
        description: 'get Booking listing.',
        tags: ['api'],
        validate: {
            query:{
                bookingId:Joi.string().trim().optional(),
                skip:Joi.number().optional().min(0),
                limit:Joi.number().optional().min(0)
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
  {
        method: 'GET',
        path: '/api/loader/materialListing',
        handler: function (request, reply) {
            var payloadData = request.query;
              var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData.id) {
                Controller.LoaderController.listMaterial(payloadData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        if(data.customMessage)
                        {
                            reply(UniversalFunctions.sendSuccess(data,{}))
                        }
                        else{
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }

                    }
                });
            }else{
                reply(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }, config: {

        auth:'LoaderAuth',
        description: 'get Booking listing.',
        tags: ['api'],
        validate: {
            query:{
                bookingId:Joi.string().trim().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'GET',
        path: '/api/loader/getLocations',
        handler: function (request, reply) {
            var payloadData = request.query;
              var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData.id) {
                Controller.LoaderController.getLocations(payloadData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        if(data.customMessage)
                        {
                            reply(UniversalFunctions.sendSuccess(data,{}))
                        }
                        else{
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }

                    }
                });
            }else{
                reply(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }, config: {

        auth:'LoaderAuth',
        description: 'get Booking listing.',
        tags: ['api'],
        validate: {
            query:{
                contractId:Joi.string().trim().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'PUT',
        path: '/api/loader/updateBookingDetails',
        handler: function (request, reply) {
            var payloadData = request.payload;
              var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData.id) {
                payloadData.userId=userData.id;
                payloadData.name=userData.name;
                payloadData.assignedLocations=userData.assignedLocations || [];
                Controller.LoaderController.updateBookingDetails(payloadData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        if(data.customMessage)
                        {
                            reply(UniversalFunctions.sendSuccess(data,{}))
                        }
                        else{
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }

                    }
                });
            }else{
                reply(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }, config: {

        auth:'LoaderAuth',
        description: 'get Booking listing.',
        tags: ['api'],
        validate: {
            payload:{
                bookingId:Joi.string().trim().required(),
                dropOffLocationId:Joi.string().optional(),
                materialIds:Joi.array().items(Joi.string()).optional()
                // authPersonName:Joi.string().trim().required(),
                // city:Joi.string().trim().required(),
                // state:Joi.string().trim().required(),
                // pincode:Joi.string().optional(),
                // countryCode:Joi.string().trim().optional(),
                // phoneNumber:Joi.string().min(10).required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'POST',
        path: '/api/loader/editImage',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData.id){
                payloadData.id=userData._id;
                Controller.LoaderController.editProfilePic(payloadData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }
                });
            }else{
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

        }


        }, config: {
        description: 'edit image',
        tags: ['api'],
         auth: 'LoaderAuth',
        payload: {
            maxBytes: 10485760,
            parse: true,
            output: 'file'
        },
        validate: {
            payload: {
                image: Joi.any()
                    .meta({swaggerType: 'file'})
                    .required()
                    .description('image file')
            },
             headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'POST'
        , path: '/api/loader/logout'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id){
            if([Config.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN,Config.APP_CONSTANTS.DATABASE.USER_ROLES.LOADER,Config.APP_CONSTANTS.DATABASE.USER_ROLES.COMPANY,Config.APP_CONSTANTS.DATABASE.USER_ROLES.VENDOR,Config.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER].indexOf(userData.type)<0 ){
                return reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED))
            }

            var queryData = {};
            queryData.type=userData.type;
            queryData.id=userData.id;
            Controller.AdminController.logoutUser(queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

        }
        }, config: {
        description: 'get Accounts Details.',
        tags: ['api', 'loader'],
        auth: 'CommonAuth',
        validate: {
            payload: {},
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
}

];
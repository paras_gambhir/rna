/**
 * Created by paras
 */

var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');

var Config = require('../Config');



module.exports = [
    {
        method: 'POST',
        path: '/api/vendor/vendorSignup',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.VendorController.vendorSignup(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Vendor Sign Up ',
        tags: ['api', 'vendor', 'Registration'],
        validate: {
            payload: {
                countryCode: Joi.string().required().trim(),
                phoneNumber: Joi.string().required().trim(),
                email:Joi.string().required().trim(),
                name: Joi.string().required().trim(),
                password: Joi.string().required().min(5).max(15).trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/vendor/verifyOTP',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.VendorController.verifyOTP(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Vendor Verify OTP ',
        tags: ['api', 'vendor', 'verify otp'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                OTP: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/vendor/completeProfile',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.VendorController.completeProfile(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Vendor Complete Profile ',
        tags: ['api', 'vendor', 'complete profile'],
        payload: {
            maxBytes: 10485760,
            parse: true,
            output: 'file'
        },
        validate: {
             payload: {
                 accessToken: Joi.string().required().trim(),
                 address: Joi.string().required().trim(),
                 city:Joi.string().required().trim(),
                 state:Joi.string().required().trim(),
                 pincode:Joi.number().required(),
                 adharNumber:Joi.string().length(12).required().trim(),
                 panNumber:Joi.string().required().length(10).trim(),
                 adharPic: Joi.any()
                     .meta({swaggerType: 'file'})
                     .required()
                     .description('image file'),
                 panPic: Joi.any()
                     .meta({swaggerType: 'file'})
                     .required()
                     .description('image file')
             },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
 {
        method: 'PUT',
        path: '/api/vendor/editProfile',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            if(userData && userData._id){
                payloadData.vendorId=userData._id;
                 Controller.VendorController.editProfile(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });
            }else{
                reply(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }

        }, config: {

        auth: 'VendorAuth',
        description: 'Vendor edit Profile ',
        tags: ['api', 'vendor', 'edit profile'],
        payload: {
            maxBytes: 10485760,
            parse: true,
            output: 'file'
        },
        validate: {
             payload: {
                 accessToken: Joi.string().required().trim(),
                 countryCode: Joi.string().required().trim(),
                 phoneNumber: Joi.string().required().trim(),
                 email:Joi.string().required().trim(),
                 name: Joi.string().required().trim(),
                 address: Joi.string().required().trim(),
                 city:Joi.string().required().trim(),
                 state:Joi.string().required().trim(),
                 pincode:Joi.number().min(111111).max(999999).required(),
                 adharNumber:Joi.string().length(12).required().trim(),
                 panNumber:Joi.string().required().length(10).trim(),
                 adharPic: Joi.any()
                     .meta({swaggerType: 'file'})
                     .optional()
                     .description('image file'),
                 panPic: Joi.any()
                     .meta({swaggerType: 'file'})
                     .optional()
                     .description('image file')
             },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'GET',
        path: '/api/vendor/getCities',
        handler: function (request, reply) {
            var queryData = request.query;
            queryData.status='ALL';
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            if(userData && userData.id) {
                Controller.CityController.getCities(queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });
            }else{
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            } 

        }, config: {
        auth: 'VendorAuth',
        description: 'Get UnAssigned Cities',
        tags: ['api', 'vendor', 'get Cities'],
        validate: {
            query:{},
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'PUT',
        path: '/api/vendor/emailVerify',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.VendorController.verifyVendorEmail(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Verify Email email.',
        tags: ['api', 'vendor', 'get Cities'],
        validate: {
            payload:{
                accessToken:Joi.string().required(),
                userType:Joi.string().valid('VENDOR','USER','DRIVER','COMPANY').required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
    {
        method: 'POST',
        path: '/api/vendor/login',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.VendorController.vendorLogin(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });

        }, config: {
        description: 'Vendor Login ',
        tags: ['api', 'vendor', 'login'],
        validate: {
            payload: {
                email: Joi.string().required().trim(),
                password: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/vendor/addDriver',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.VendorController.addDriver(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Vendor Add Driver ',
        tags: ['api', 'vendor', 'add driver'],
        payload: {
            maxBytes: 10485760,
            parse: true,
            output: 'file'
        },
        validate: {
            payload: {
                accessToken:Joi.string().required().trim(),
                name: Joi.string().required().trim(),
                dob : Joi.string().required().trim(),
                pincode:Joi.number().required(),
                address: Joi.string().required().trim(),
                city: Joi.string().required().trim(),
                state: Joi.string().required().trim(),
                adharPic: Joi.any()
                    .meta({swaggerType: 'file'})
                    .required()
                    .description('image file'),
                licensePic: Joi.any()
                    .meta({swaggerType: 'file'})
                    .required()
                    .description('image file'),
                email: Joi.string().optional().trim(),
                password: Joi.string().required().trim(),
                countryCode: Joi.string().required().trim(),
                phoneNumber: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
}, {
        method: 'POST',
        path: '/api/vendor/updateDriver',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.VendorController.updateDriver(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Vendor Add Driver ',
        tags: ['api', 'vendor', 'add driver'],
        payload: {
            maxBytes: 10485760,
            parse: true,
            output: 'file'
        },
        validate: {
            payload: {
                accessToken:Joi.string().required().trim(),
                driverId:Joi.string().trim().required(),
                name: Joi.string().required().trim(),
                dob : Joi.string().required().trim(),
                pincode:Joi.number().required(),
                address: Joi.string().required().trim(),
                city: Joi.string().required().trim(),
                state: Joi.string().required().trim(),
                email: Joi.string().optional().trim(),
                password: Joi.string().optional().trim(),
                countryCode: Joi.string().required().trim(),
                phoneNumber: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
}, {
        method: 'GET',
        path: '/api/vendor/listDrivers',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.VendorController.listDrivers(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Vendor List Driver ',
        tags: ['api', 'vendor', 'list driver'],
        validate: {
            query: {
                accessToken:Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/vendor/addTruck',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.VendorController.addVendorTruck(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });

        }, config: {
        description: 'Add new Truck ',
        tags: ['api', 'vendor', 'add truck'],
        payload: {
            maxBytes: 10485760,
            parse: true,
            output: 'file'
        },
        validate: {
            payload: {
                accessToken:Joi.string().required().trim(),
                truckId:Joi.string().required().trim(),
                truckNo:Joi.string().required().trim(),
                rcPic: Joi.any()
                    .meta({swaggerType: 'file'})
                    .required()
                    .description('image file'),
                pollutionPic: Joi.any()
                    .meta({swaggerType: 'file'})
                    .required()
                    .description('image file'),
                insurancePic:Joi.any()
                    .meta({swaggerType: 'file'})
                    .required()
                    .description('image file'),
                fitnessCertificate:Joi.any()
                    .meta({swaggerType: 'file'})
                    .required()
                    .description('image file'),
                startCityId:Joi.string().required(),
                endCityId:Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
 {
        method: 'PUT',
        path: '/api/vendor/editVendorTruck',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.VendorController.editVendorTruck(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });

        }, config: {
        description: 'Edit Vendor Truck ',
        tags: ['api', 'vendor', 'edit truck'],
        payload: {
            maxBytes: 10485760,
            parse: true,
            output: 'file'
        },
        validate: {
            payload: {
                accessToken:Joi.string().required().trim(),
                id:Joi.string().required().trim(),
                truckId:Joi.string().required().trim(),
                truckNo:Joi.string().required().trim(),
                rcPic: Joi.any()
                    .meta({swaggerType: 'file'})
                    .optional()
                    .description('image file'),
                pollutionPic: Joi.any()
                    .meta({swaggerType: 'file'})
                    .optional()
                    .description('image file'),
                insurancePic:Joi.any()
                    .meta({swaggerType: 'file'})
                    .optional()
                    .description('image file'),
                fitnessCertificate:Joi.any()
                    .meta({swaggerType: 'file'})
                    .optional()
                    .description('image file'),
                startCityId:Joi.string().required(),
                endCityId:Joi.string().required(),
                driverId:Joi.string().optional()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'GET',
        path: '/api/vendor/listGlobalTruck',
        handler: function (request, reply) {
            Controller.VendorController.listGlobalTruck(function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });

        }, config: {
        description: 'list Global Truck',
        tags: ['api', 'vendor', 'listGlobalTruck'],
        validate: {
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'GET',
        path: '/api/vendor/myTruck',
        handler: function (request, reply) {
            Controller.VendorController.myTruck(request.query,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });

        }, config: {
        description: 'myTruck',
        tags: ['api', 'vendor', 'myTruck'],
        validate: {
            query:{
                accessToken:Joi.string().required().trim(),
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'PUT',
        path: '/api/vendor/createPassword',
        handler: function (request, reply) {
            Controller.VendorController.createPassword(request.query,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });

        }, config: {
        description: 'create Password',
        tags: ['api', 'vendor', 'myTruck'],
        validate: {
            query:{
                accessToken:Joi.string().required().trim(),
                driverId:Joi.string().required().trim(),
                password:Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/vendor/assignDriverToTruck',
        handler: function (request, reply) {
            Controller.VendorController.assignDriverToTruck(request.payload,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });

        }, config: {
        description: 'assign truck',
        tags: ['api', 'vendor', 'myTruck'],
        validate: {
            payload:{
                accessToken:Joi.string().required().trim(),
                driverId:Joi.string().required().trim(),
                truckId:Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'GET',
        path: '/api/vendor/notAssignTruckDriver',
        handler: function (request, reply) {
            Controller.VendorController.notAssignTruckDriver(request.query,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });

        }, config: {
        description: 'Not assign truck',
        tags: ['api', 'vendor', 'myTruck'],
        validate: {
            query:{
                accessToken:Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'GET',
        path: '/api/vendor/driverLocation',
        handler: function (request, reply) {
            Controller.VendorController.driverLocation(request.query,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });

        }, config: {
        description: 'get driver current location',
        tags: ['api', 'vendor', 'driveLocation'],
        validate: {
            query:{
                accessToken:Joi.string().required().trim(),
                status:Joi.boolean().optional(),
                driverId:Joi.string().trim().optional(),
                truckStatus:Joi.string().valid([
                    Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.BOOKED,
                    Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.BROKE,
                    Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.VACANT
                ]).optional()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
 {
        method: 'POST'
        , path: '/api/vendor/bookingList'
        , handler: function (request, reply) {
        Controller.VendorController.bookingList(request.query,function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
            } else {
                reply(UniversalFunctions.sendSuccess(null, data))

            }
        });
    }, config: {
        description: 'booking list according to status or driver or truck',
        tags: ['api', 'vendor'],
        validate: {
            query:{
                accessToken:Joi.string().required().trim(),
                driverId:Joi.string().optional().trim(),
                bookingId:Joi.string().trim().optional().description('_id of the booking.'),    
                truckId:Joi.string().optional().trim(),
                startDate:Joi.number().optional(),
                endDate:Joi.number().min(Joi.ref('startDate')).optional(),
                status:Joi.string().required().valid('PENDING','ACCEPTED','REJECTED','ON_THE_WAY','ENDED','ALL'),
                skip:Joi.string().required(),
                limit:Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
     {
        method: 'PUT'
        , path: '/api/vendor/updateVendorSetting'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            var payloadData=request.payload;
            payloadData.id=userData._id;
            payloadData.emailNotifications=userData.emailNotifications;
            payloadData.smsNotifications=userData.smsNotifications;
            payloadData.pushNotifications=userData.pushNotifications;
            payloadData.userType=Config.APP_CONSTANTS.DATABASE.USER_ROLES.VENDOR;
            Controller.SettingsController.updateSetting(payloadData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'update vendor setting',
        tags: ['api', 'admin'],
        auth: 'VendorAuth',
        validate: {
            payload:{
                sms:Joi.boolean().required(),
                email:Joi.boolean().required(),
                push:Joi.boolean().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
   {
        method: 'GET'
        , path: '/api/vendor/getProfileData'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            var payloadData=request.payload;
            payloadData.id=userData._id;
            Controller.VendorController.getProfileData(payloadData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'get profile data.',
        tags: ['api', 'admin'],
        auth: 'VendorAuth',
        validate: {
            query:{},
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'PUT'
        , path: '/api/vendor/updateVendorTruckStatus'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {

            request.payload.vendorId=userData.id;
            Controller.AdminController.updateVendorTruckStatus(request.payload,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'update vendor contract status',
        tags: ['api', 'vendor'],
        auth: 'VendorAuth',
        validate: {
            payload:{
                truckId:Joi.string().required(),
                status:Joi.string().valid([
                    Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.BOOKED,
                    Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.BROKE,
                    Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.VACANT
                ])
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'POST'
        , path: '/api/vendor/sendSMS'
        , handler: function (request, reply) {
        Controller.AdminController.sendSMS(request.payload,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
    }, config: {
        description: 'update vendor contract status',
        tags: ['api', 'vendor'],
        validate: {
            payload:{
                countryCode:Joi.string().required(),
                phoneNumber:Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    }

];
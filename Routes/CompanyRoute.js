/**
 * Created by paras
 */

var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');

module.exports = [
    {
        method: 'POST',
        path: '/api/company/register',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CompanyController.companySignup(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Company Sign Up ',
        tags: ['api', 'company', 'signup'],
        validate: {
            payload: {
                countryCode: Joi.string().required().trim(),
                phoneNumber: Joi.string().required().trim(),
                email:Joi.string().required().trim(),
                name: Joi.string().required().trim(),
                password: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'GET',
        path: '/api/company/getCities',
        handler: function (request, reply) {
            var queryData = request.query;
            queryData.status='ALL';
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData.id) {
                Controller.CityController.getCities(queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });
            }else{
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            } 

        }, config: {
        auth: 'CompanyAuth',
        description: 'Get UnAssigned Cities',
        tags: ['api', 'company', 'get Cities'],
        validate: {
            query:{},
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
    {
        method: 'POST',
        path: '/api/company/verifyOTP',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CompanyController.verifyOTP(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Company Verify OTP ',
        tags: ['api', 'company', 'verify otp'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                OTP: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/company/completeProfile',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CompanyController.completeProfile(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Company Complete Profile ',
        tags: ['api', 'company', 'complete profile'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                address: Joi.string().required().trim(),
                pincode:Joi.number().required(),
                city:Joi.string().required().trim(),
                state:Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/company/emailLogin',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CompanyController.emailLogin(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Company Email Login',
        tags: ['api', 'company', 'email login'],
        validate: {
            payload: {
                email: Joi.string().required().trim(),
                password: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/company/addMaterial',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CompanyController.addMaterial(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Company Add material',
        tags: ['api', 'company', 'add material'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                materialName: Joi.string().required().trim(),
                breadth: Joi.number().required().min(1),
                length:Joi.number().required().min(1),
                height:Joi.number().required().min(1),
                accessionNo:Joi.string().trim().required(),
                capacity:Joi.number().required().min(1)
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'POST',
        path: '/api/company/editMaterial',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CompanyController.editMaterial(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Company Add material',
        tags: ['api', 'company', 'add material'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                id:Joi.string().trim().required(),
                materialName: Joi.string().required().trim(),
                breadth: Joi.number().required().min(1),
                length:Joi.number().required().min(1),
                height:Joi.number().required().min(1),
                 accessionNo:Joi.string().trim().required(),
                capacity:Joi.number().required().min(1)
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'GET',
        path: '/api/company/listMaterials',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.CompanyController.listMaterial(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Company List materials',
        tags: ['api', 'company', 'list material'],
        validate: {
            query: {
                accessToken: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/company/addContract',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CompanyController.addContract(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Company Add contract',
        tags: ['api', 'company', 'add contract'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                startLocation: Joi.array().required(),
                startAddress: Joi.string().required(),
                endLocation: Joi.array().required(),
                endAddress: Joi.string().required(),
                truckId:Joi.string().required()


            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'GET',
        path: '/api/company/listContracts',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.CompanyController.listContracts(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Company List contracts',
        tags: ['api', 'company', 'list contracts'],
        validate: {
            query: {
                accessToken: Joi.string().required().trim(),
                skip:Joi.number().min(0).required(),
                limit:Joi.number().min(0).required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/company/addBooking',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CompanyController.addBooking(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Company Add Booking',
        tags: ['api', 'company', 'add booking'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                contractId: Joi.string().required(),
                bookingDate:Joi.string().required().description('Date should be in the format of mm/dd/yyyy.'),
                pickupLocationId: Joi.string().required(),
                dropOffLocationId:Joi.string().optional(),
                materialIds:Joi.array().items(Joi.string()).optional()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'GET',
        path: '/api/company/globalTruck',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.CompanyController.globalTruck(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });
        }, config: {
        description: 'global truck list',
        tags: ['api', 'company', 'add booking'],
        validate: {
            query:{
                accessToken: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'GET',
        path: '/api/company/bookingListing',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.CompanyController.bookingListing(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });
        }, config: {
        description: 'current and history booking lisitng ',
        tags: ['api', 'company', 'add booking'],
        validate: {
            query:{
                accessToken: Joi.string().required(),
                startDate:Joi.number().optional(),
                endDate:Joi.number().min(Joi.ref('startDate')).optional(),
                bookingId:Joi.string().trim().optional().description('_id of the booking.'),    
                status:Joi.string().required().valid('HISTORY','CURRENT')
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},

{
        method: 'POST',
        path: '/api/company/addLocation',
        handler: function (request, reply) {
            var queryData = request.payload;
            queryData.status='ALL';
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData.id) {
                Controller.CityController.getCities(queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });
            }else{
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            } 

        }, config: {
        auth: 'CompanyAuth',
        description: 'Get UnAssigned Cities',
        tags: ['api', 'vendor', 'get Cities'],
        validate: {
            payload:{
                cityId:Joi.string().required(),
                
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'POST'
        , path: '/api/company/addNewLocation'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id){
            var payloadData = request.payload;
            payloadData.userId=userData._id;
            Controller.CityController.addLocation(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

        }
        }, config: {
        description: 'add New Location',
        tags: ['api', 'company'],
        auth: 'CompanyAuth',
        validate: {
            payload: {
                pointName:Joi.string().trim().required(),
                address:Joi.string().trim().required(),
                authPerson:Joi.string().required(),
                phoneNo:Joi.string().required(),
                lat:Joi.number().min(-180).max(180).required(),
                long:Joi.number().min(-180).max(180).required(),
                cityId:Joi.string().trim().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'POST'
        , path: '/api/company/postQuery'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id){
            var payloadData = request.payload;
            payloadData.userId=userData._id;
            payloadData.name=userData.name;
            Controller.AdminController.postQuery(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

        }
        }, config: {
        description: 'post query for add new contract.',
        tags: ['api', 'company'],
        auth: 'CompanyAuth',
        validate: {
            payload: {
                source:Joi.string().trim().required(),
                destination:Joi.string().trim().required(),
                truckType:Joi.string().trim().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'POST'
        , path: '/api/company/editLocation'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id){
            var payloadData = request.payload;
            payloadData.userId=userData._id;
            Controller.CityController.editLocation(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

        }
        }, config: {
        description: 'edit Location',
        tags: ['api', 'company'],
        auth: 'CompanyAuth',
        validate: {
            payload: {
                id:Joi.string().trim().required(),
                pointName:Joi.string().trim().required(),
                address:Joi.string().trim().required(),
                city:Joi.string().trim().required(),
                state:Joi.string().trim().required(),
                pincode:Joi.number().required(),
                authPerson:Joi.string().required(),
                phoneNo:Joi.string().required(),
                lat:Joi.number().min(-180).max(180).required(),
                long:Joi.number().min(-180).max(180).required(),
                cityId:Joi.string().trim().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
  {
        method: 'GET',
        path: '/api/company/getLocations',
        handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id){
            var payloadData = request.query;
            payloadData.userId=userData._id;
            Controller.CityController.getLocations(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

        }
        }, 
        config: {
        description: 'current and history booking lisitng ',
        tags: ['api', 'company', 'add booking'],
         auth: 'CompanyAuth',
        validate: {
            query:{
                contractId: Joi.string().optional(),
                skip:Joi.number().min(0).optional(),
                limit:Joi.number().min(0).optional()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'POST',
        path: '/api/company/editImage',
        handler: function (request, reply) {
            var payloadData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData.id){
                payloadData.companyId=userData._id;
                Controller.CompanyController.editProfilePic(payloadData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }
                });
            }else{
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

        }


        }, config: {
        description: 'edit image',
        tags: ['api'],
         auth: 'CompanyAuth',
        payload: {
            maxBytes: 10485760,
            parse: true,
            output: 'file'
        },
        validate: {
            payload: {
                image: Joi.any()
                    .meta({swaggerType: 'file'})
                    .required()
                    .description('image file')
            },
             headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
}
];
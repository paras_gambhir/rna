/**
 * Created by paras
 */

var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../Config');

module.exports = [

    {
        method: 'POST',
        path: '/api/driver/passwordLogin',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.driverLogin(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Driver Login with password',
        tags: ['api', 'driver', 'login','with password'],
        validate: {
            payload: {
                countryCode: Joi.string().required().trim(),
                phoneNumber: Joi.string().required().trim(),
                password: Joi.string().required().trim(),
                deviceToken: Joi.string().optional(),
                appVersion: Joi.string().required(),
                deviceType: Joi.string().required().valid(
                    [UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID])
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },


    {
        method: 'POST',
        path: '/api/driver/OTPLogin',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.OTPLogin(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Driver Login with OTP',
        tags: ['api', 'driver', 'login','with otp'],
        validate: {
            payload: {
                countryCode: Joi.string().required().trim(),
                phoneNumber: Joi.string().required().trim(),
                OTP: Joi.number().required(),
                deviceToken: Joi.string().optional(),
                appVersion: Joi.string().required(),
                deviceType: Joi.string().required().valid(
                    [UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID])
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },


    {
        method: 'POST',
        path: '/api/driver/sendOTP',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.sendOTP(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Send OTP on mobile',
        tags: ['api', 'driver', 'send otp'],
        validate: {
            payload: {
                countryCode: Joi.string().required().trim(),
                phoneNumber: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },


    {
        method: 'POST',
        path: '/api/driver/forgotPassword',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.forgotPassword(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if (data.customMessage) {
                        reply(UniversalFunctions.sendSuccess(data, {}))
                    }
                    else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }

                }
            });

        }, config: {
        description: 'Forgot password customer',
        tags: ['api', 'driver', 'forgot password hai beta'],
        validate: {
            payload: {
                email: Joi.string().optional(),
                countryCode: Joi.string().optional(),
                phoneNumber: Joi.string().optional()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },


    {
        method: 'POST',
        path: '/api/driver/accessTokenLogin',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.accessTokenLogin(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Driver access token Login',
        tags: ['api', 'driver', 'access token login'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                latitude: Joi.string().required(),
                longitude: Joi.string().required(),
                deviceToken: Joi.string().required(),
                appVersion: Joi.string().required(),
                deviceType: Joi.string().optional().valid(
                    [UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID])
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },


    {
        method: 'POST',
        path: '/api/driver/driverLogout',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.driverLogout(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Driver Logout',
        tags: ['api', 'driver', 'logout'],
        validate: {
            payload: {
                accessToken: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },


    {
        method: 'PUT',
        path: '/api/driver/updateDriverLocation',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.updateDriverLocation(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Update driver location',
        tags: ['api', 'driver', 'update driver location'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                location: Joi.array().required(),
                bookingId: Joi.string().optional()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },


    {
        method: 'GET',
        path: '/api/driver/profileData',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.DriverController.getProfileData(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if (data.customMessage) {
                        reply(UniversalFunctions.sendSuccess(data, {}))
                    }
                    else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }

                }
            });

        }, config: {
        description: 'Get profile data',
        tags: ['api', 'driver', 'profile data'],
        validate: {
            query: {
                accessToken:Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'PUT',
        path: '/api/driver/updateProfile',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.updateProfile(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if (data.customMessage) {
                        reply(UniversalFunctions.sendSuccess(data, {}))
                    }
                    else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }

                }
            });

        }, config: {
        description: 'Driver Edit Profile',
        tags: ['api', 'driver', 'edit profile'],
        payload: {
            maxBytes: 10485760,
            parse: true,
            output: 'file'
        },
        validate: {
            payload: {
                accessToken: Joi.string().trim().required(),
                name: Joi.string().trim().required(),
                notification: Joi.string().optional().valid('1','0'),
                pic: Joi.any()
                    .meta({swaggerType: 'file'})
                    .optional()
                    .description('image file')

            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'GET',
        path: '/api/driver/getRequestPush',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.DriverController.getRequestPush(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if (data.customMessage) {
                        reply(UniversalFunctions.sendSuccess(data, {}))
                    }
                    else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }

                }
            });

        }, config: {
        description: 'Get New Request Push',
        tags: ['api', 'driver', 'request push'],
        validate: {
            query: {
                accessToken:Joi.string().required(),
                deviceToken: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'GET',
        path: '/api/driver/getBookingDetails',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.DriverController.getBookingDetails(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if (data.customMessage) {
                        reply(UniversalFunctions.sendSuccess(data, {}))
                    }
                    else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }

                }
            });

        }, config: {
        description: 'Booking details',
        tags: ['api', 'driver', 'booking details'],
        validate: {
            query: {
                accessToken:Joi.string().required(),
                bookingId: Joi.string().optional().trim(),
                option:Joi.string().optional().valid(
                    'Current',
                    'History'
                )
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/driver/acceptRide',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.acceptRide(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Accept Ride',
        tags: ['api', 'driver', 'accept ride'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                bookingId: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
 {
        method: 'POST',
        path: '/api/driver/sendMessage',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.sendMessage(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Accept Ride',
        tags: ['api', 'driver'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                message: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/driver/rejectRide',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.rejectRide(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Reject Ride',
        tags: ['api', 'driver', 'reject ride'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                bookingId: Joi.string().required(),
                rejectionReason: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/driver/startRide',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.startRide(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Start Ride',
        tags: ['api', 'driver', 'start ride'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                bookingId: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/driver/reachedLoadingLocation',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.reachedLoadingLocation(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Reached loading location',
        tags: ['api', 'driver', 'reached loading location'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                bookingId: Joi.string().required(),
                loadingLocationId: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'POST',
        path: '/api/driver/updateBookingStatus',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.updateBookingStatus(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'update booking status',
        tags: ['api', 'driver'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                bookingId: Joi.string().required(),
                status:Joi.string().optional().valid(
                    Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.UNLOADING_LOCATION,
                    Config.APP_CONSTANTS.DATABASE.RIDE_STATUS.ENDED
                    
                )
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'POST',
        path: '/api/driver/endRide',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.endRide(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'update booking status',
        tags: ['api', 'driver'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                OTP:Joi.string().required(),
                bookingId: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/driver/getloadingDetails',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.getLoadingDetails(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Reached loading location',
        tags: ['api', 'driver', 'reached loading location'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                bookingId: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/driver/reachedUnloadingLocation',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.reachedUnloadingLocation(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Reached unloading location',
        tags: ['api', 'driver', 'reached unloading location'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                bookingId: Joi.string().required(),
                unloadingLocationId: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/driver/fillUnloadingForm',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.fillMaterialForm(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        description: 'Fill unloading material form',
        tags: ['api', 'driver', 'fill unloading material form'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                bookingId: Joi.string().required(),
                materials:Joi.array().items(Joi.object().keys({
                    materialId: Joi.string().required(),
                    totalQty: Joi.number().required().min(0),
                    ok:Joi.number().required().min(0),
                    missing:Joi.number().required().min(0),
                    damaged:Joi.number().required().min(0),
                    other:Joi.number().required().min(0)
                })).required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    }

];
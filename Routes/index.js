
'use strict';
var AdminRoute = require('./AdminRoute');
var DriverRoute = require('./DriverRoute');
var VendorRoute = require('./VendorRoute');
var CompanyRoute = require('./CompanyRoute');
var LoaderRoute = require('./LoaderRoute');

var all = [].concat(AdminRoute,DriverRoute,VendorRoute,CompanyRoute,LoaderRoute);

module.exports = all;


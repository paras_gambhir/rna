'use strict';
/**
 * Created by paras
 */

var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../Config');

var non_auth_routes = [];

var adminLogin = [
    {
        method: 'POST'
        , path: '/api/admin/login'
        , handler: function (request, reply) {
        var payloadData = request.payload;
        Controller.AdminController.adminLogin(payloadData, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
            } else {
                if (data.customMessage) {
                    reply(UniversalFunctions.sendSuccess(data, {}))
                }
                else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            }
        });
    }, config: {
        description: 'Admin Login',
        tags: ['api', 'admin', 'login'],

        validate: {
            payload: {
                email: Joi.string().trim().required(),
                password: Joi.string().trim().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST'
        , path: '/api/admin/addTruck'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id){
            var payloadData = request.payload;
            Controller.AdminController.addTruck(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

        }
        }, config: {
        description: 'add New Truck',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            payload: {
                truckName:Joi.string().required(),
                Capacity:Joi.number().min(1).max(50).required(),
                length:Joi.number().required(),
                breadth:Joi.number().required(),
                height:Joi.number().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'POST'
        , path: '/api/admin/addCity'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id){
            var payloadData = request.payload;
            Controller.CityController.addCity(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

        }
        }, config: {
        description: 'add New City',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            payload: {
                cityName:Joi.string().required(),
                lat:Joi.number().min(-180).max(180).required(),
                long:Joi.number().min(-180).max(180).required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'GET'
        , path: '/api/admin/listTruck'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            Controller.AdminController.listTruck(function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'add New Truck',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'GET'
        , path: '/api/admin/listVendor'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            Controller.AdminController.listVendor(request.query,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'list Vendor',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            query:{
                adminVerifiy:Joi.boolean().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
  {
        method: 'GET'
        , path: '/api/admin/getCompanies'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            Controller.CompanyController.getCompaniesList(request.query,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'list Companies',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            query:{
                isApproved:Joi.boolean().optional(),
                skip:Joi.number().required(),
                limit:Joi.number().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'PUT'
        , path: '/api/admin/verifiyVendor'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            Controller.AdminController.verifiyVendor(request.payload,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'list Vendor',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            payload:{
                _id:Joi.string().required(),

            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
 {
        method: 'PUT'
        , path: '/api/admin/verifyCompany'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            Controller.AdminController.verifyCompany(request.payload,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'list Vendor',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            payload:{
                _id:Joi.string().required(),

            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'GET'
        , path: '/api/admin/listVendorTruck'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            Controller.AdminController.listVendorTruck(request.query,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'list Vendor truck',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            query:{
                vendorId:Joi.string().optional(),
                driverId:Joi.string().optional(),
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'GET'
        , path: '/api/admin/listDriver'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            Controller.AdminController.listDriver(request.query,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'list Driver',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            query:{
                vendorId:Joi.string().optional(),
                truckId:Joi.string().optional(),
                adminVerifiy:Joi.boolean().required(),
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'PUT'
        , path: '/api/admin/approveDriver'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            Controller.AdminController.approveDriver(request.query,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'Approve Driver',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            query:{
                driverId:Joi.string().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
 {
        method: 'PUT'
        , path: '/api/admin/forgotPassword'
        , handler: function (request, reply) {

        var payload=request.payload;

        Controller.AdminController.forgotPassword(payload,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
    }, config: {
        description: 'forgot password',
        tags: ['api', 'admin'],
        validate: {
            payload:{
                email:Joi.string().required(),
                userType:Joi.string().required().valid('VENDOR','ADMIN','DRIVER','COMPANY')
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'PUT'
        , path: '/api/admin/updatePassword'
        , handler: function (request, reply) {

        var payload=request.payload;

        Controller.AdminController.updatePassword(payload,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
    }, config: {
        description: 'forgot password',
        tags: ['api', 'admin'],
        validate: {
            payload:{
                accessToken:Joi.string().required(),
                password:Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'PUT'
        , path: '/api/admin/blockUnblockContract'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            Controller.AdminController.updateContractStatus(request.payload,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'update contract status',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            payload:{
                contractId:Joi.string().required(),
                block:Joi.boolean().default(true).required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'PUT'
        , path: '/api/admin/blockUnblockVendor'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            Controller.VendorController.updateVendorBlockStatus(request.payload,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'update vendor status',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            payload:{
                vendorId:Joi.string().required(),
                block:Joi.boolean().default(true).required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'PUT'
        , path: '/api/admin/blockUnblockCompany'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            Controller.CompanyController.updateCompanyBlockStatus(request.payload,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'update company status',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            payload:{
                companyId:Joi.string().required(),
                block:Joi.boolean().default(true).required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'PUT'
        , path: '/api/admin/updateVendorTruckStatus'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            Controller.AdminController.updateVendorTruckStatus(request.payload,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'update vendor contract status',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            payload:{
                truckId:Joi.string().required(),
                status:Joi.string().valid([
                    Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.BOOKED,
                    Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.BROKE,
                    Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.VACANT
                ])
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'GET'
        , path: '/api/admin/driverLocations'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            Controller.AdminController.driverLocations(request.query,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'driver Locations',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            query:{
                driverId:Joi.string().optional(),
                vendorId:Joi.string().optional(),
                status:Joi.boolean().optional(),
                truckStatus:Joi.string().valid([
                    Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.BOOKED,
                    Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.BROKE,
                    Config.APP_CONSTANTS.VENDOR_TRUCK.STATUS.VACANT
                ]).optional()
                
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST'
        , path: '/api/admin/bookingList'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            Controller.AdminController.bookingList(request.query,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'booking list according to status',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            query:{
                status:Joi.string().required().valid(
                    'PENDING',
                    'ACCEPTED',
                    'REJECTED',
                    'ON_THE_WAY',
                    'ENDED',
                    'ALL'),
                bookingId:Joi.string().trim().optional().description('_id of the booking.'),    
                truckId:Joi.string().optional().trim(),
                vendorId:Joi.string().optional().trim(),
                startDate:Joi.number().optional(),
                endDate:Joi.number().min(Joi.ref('startDate')).optional(),
                skip:Joi.string().required(),
                limit:Joi.string().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'GET'
        , path: '/api/admin/trackBookingRoute'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            var query=request.query;
            Controller.AdminController.trackBookingRoute(query,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'track booking route.',
        tags: ['api', 'admin'],
        auth: 'CommonAuth',
        validate: {
            query:{
                bookingId:Joi.string().trim().required().description('_id of the booking.')
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'POST',
        path: '/api/admin/addLoader',
        handler: function (request, reply) {
            var payloadData = request.payload;
              var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData.id) {
                Controller.LoaderController.registerLoader(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });
            }else{
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            } 

        }, config: {
        auth: 'adminAuth',
        description: 'Admin Add Loader ',
        tags: ['api', 'admin', 'add loader'],
        validate: {
            payload: {
                name: Joi.string().required().trim(),
                dob : Joi.string().required().trim(),
                address: Joi.string().required().trim(),
                city:Joi.string().required().trim(),
                state:Joi.string().required().trim(),
                pincode:Joi.number().required(),
                email: Joi.string().required().trim(),
                password: Joi.string().required().trim(),
                countryCode: Joi.string().required().trim(),
                phoneNumber: Joi.string().required().trim()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'PUT',
        path: '/api/admin/editLoader',
        handler: function (request, reply) {
            var payloadData = request.payload;
              var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData.id) {
                Controller.LoaderController.editLoader(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });
            }else{
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            } 

        }, config: {
        auth: 'adminAuth',
        description: 'Admin edit Loader ',
        tags: ['api', 'admin', 'add loader'],
        validate: {
            payload: {
                id:Joi.string().required(),
                name: Joi.string().required().trim(),
                dob : Joi.string().required().trim(),
                address: Joi.string().required().trim(),
                city:Joi.string().required().trim(),
                state:Joi.string().required().trim(),
                pincode:Joi.number().required(),
                email: Joi.string().required().trim(),
                countryCode: Joi.string().required().trim(),
                phoneNumber: Joi.string().required().trim()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'GET',
        path: '/api/admin/getCities',
        handler: function (request, reply) {
            var queryData = request.query;
              var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData.id) {
                Controller.CityController.getCities(queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });
            }else{
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            } 

        }, config: {
        auth: 'adminAuth',
        description: 'Get UnAssigned Cities',
        tags: ['api', 'admin', 'get Cities'],
        validate: {
            query:{
                status:Joi.string().valid('ALL','NOT ASSIGNED').required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'GET',
        path: '/api/admin/getLocations',
        handler: function (request, reply) {
            var queryData = request.query;
              var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData.id) {
                Controller.CityController.getLocationsAdmin(queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });
            }else{
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            } 

        }, config: {
        auth: 'adminAuth',
        description: 'get locations added by company.',
        tags: ['api'],
        validate: {
            query:{
                cityId:Joi.string().trim().optional(),
                companyId:Joi.string().trim().optional(),
                loaderId:Joi.string().trim().optional(),
                assigned:Joi.boolean().optional(),
                skip:Joi.number().min(0).required(),
                limit:Joi.number().min(0).required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'GET',
        path: '/api/admin/getLoaders',
        handler: function (request, reply) {
            var queryData = request.query;
              var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData.id) {
                Controller.LoaderController.getLoaders(queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });
            }else{
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            } 

        }, config: {
        auth: 'adminAuth',
        description: 'Get Loaders List',
        tags: ['api', 'admin', 'get Loaders'],
        validate: {
            query:{
                id:Joi.string().optional().trim(),
                skip:Joi.number().min(0).required(),
                limit:Joi.number().min(0).required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'POST',
        path: '/api/admin/assignLocationToLoader',
        handler: function (request, reply) {
            var payloadData = request.payload;
              var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData.id) {
                Controller.CityController.assignLocationToLoader(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });
            }else{
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            } 

        }, config: {
        auth: 'adminAuth',
        description: 'Admin assign city to loader ',
        tags: ['api', 'admin', 'add City to loader'],
        validate: {
            payload: {
                locations:Joi.array().items(Joi.string()).required(),
                loaderId:Joi.string().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {

                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'DELETE',
        path: '/api/admin/removeLocationFromLoader',
        handler: function (request, reply) {
            var payloadData = request.payload;
              var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData.id) {
                Controller.CityController.removeLoaderLocation(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });
            }else{
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            } 

        }, config: {
        auth: 'adminAuth',
        description: 'Admin assign city to loader ',
        tags: ['api', 'admin', 'add City to loader'],
        validate: {
            payload: {
                locationId:Joi.string().required(),
                loaderId:Joi.string().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'POST',
        path: '/api/admin/addContract',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CompanyController.addContract(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        auth: 'adminAuth',
        description: 'Admin Add contract',
        tags: ['api', 'admin', 'add contract'],
        validate: {
            payload: {
                companyId:Joi.string().required(),
                truckType:Joi.string().required(),
                freightAmount:Joi.number().required(),
                detentionCharges:Joi.number().required(),
                billingPeriod:Joi.number().required(),
                startCityId:Joi.string().required(),
                endCityId:Joi.string().required(),
                truckId:Joi.string().required()


            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'PUT',
        path: '/api/admin/editContract',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CompanyController.editContract(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    if(data.customMessage)
                    {
                        reply(UniversalFunctions.sendSuccess(data,{}))
                    }
                    else{
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }

                }
            });

        }, config: {
        auth: 'adminAuth',
        description: 'Admin Edit contract',
        tags: ['api', 'admin', 'edit contract'],
        validate: {
            payload: {
                id:Joi.string().required(),
                companyId:Joi.string().required(),
                truckType:Joi.string().required(),
                freightAmount:Joi.number().required(),
                detentionCharges:Joi.number().required(),
                billingPeriod:Joi.number().required(),
                startCityId:Joi.string().required(),
                endCityId:Joi.string().required(),
                truckId:Joi.string().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'GET',
        path: '/api/admin/listContracts',
        handler: function (request, reply) {
            var payloadData = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData._id){
                Controller.CompanyController.listContractsAdmin(payloadData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        if(data.customMessage)
                        {
                            reply(UniversalFunctions.sendSuccess(data,{}))
                        }
                        else{
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }

                    }
                });
            }else{
                reply(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }, config: {
        auth: 'adminAuth',
        description: 'Company List contracts',
        tags: ['api', 'admin', 'list contracts'],
        validate: {
            query: {
                companyId:Joi.string().optional(),
                skip:Joi.number().min(0).required(),
                limit:Joi.number().min(0).required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'GET',
        path: '/api/admin/getProfileDetails',
        handler: function (request, reply) {
            var payloadData = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if(userData && userData._id){
                payloadData.id=userData._id;
                Controller.AdminController.getProfileData(payloadData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        if(data.customMessage)
                        {
                            reply(UniversalFunctions.sendSuccess(data,{}))
                        }
                        else{
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }

                    }
                });
            }else{
                reply(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }, config: {
        auth: 'adminAuth',
        description: 'Company List contracts',
        tags: ['api', 'admin', 'list contracts'],
        validate: {
            query: {},
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
 {
        method: 'PUT'
        , path: '/api/admin/updateAdminSetting'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            var payloadData=request.payload;
            payloadData.id=userData._id;
            payloadData.emailNotifications=userData.emailNotifications;
            payloadData.smsNotifications=userData.smsNotifications;
            payloadData.pushNotifications=userData.pushNotifications;
            payloadData.userType=Config.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN;
            Controller.SettingsController.updateSetting(payloadData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'update admin setting',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            payload:{
                sms:Joi.boolean().required(),
                email:Joi.boolean().required(),
                push:Joi.boolean().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
 {
        method: 'POST'
        , path: '/api/admin/bookAgain'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id) {
            var payloadData=request.payload;
            Controller.AdminController.bookingAgain(payloadData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
        }
    }, config: {
        description: 'update admin setting',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            payload:{
                bookingId:Joi.string().trim().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
 {
        method: 'POST'
        , path: '/api/admin/addAccountDetails'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id){
            var payloadData = request.payload;
            Controller.AdminController.addAccount(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

        }
        }, config: {
        description: 'add New Account',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            payload: {
                bookingId:Joi.string().trim().optional(),
                invoiceNo:Joi.string().trim().optional(),
                date:Joi.number().positive().required(),
                startDate:Joi.string().trim().optional(),
                endDate:Joi.string().trim().optional(),
                amount:Joi.number().min(1).required(),
                comment:Joi.string().trim().required(),
                receiverId:Joi.string().trim().optional(),
                type:Joi.string().required().valid(
                    'DRIVER',
                    'COMPANY',
                    'VENDOR',
                    'LOADER'
                )

            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
 {
        method: 'PUT'
        , path: '/api/admin/editAccountDetails'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id){
            var payloadData = request.payload;
            Controller.AdminController.editAccount(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

        }
        }, config: {
        description: 'edit uncleared  Account deatils.',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            payload: {
                id:Joi.string().trim().required(),
                bookingId:Joi.string().trim().optional(),
                invoiceNo:Joi.string().trim().optional(),
                date:Joi.number().positive().required(),
                amount:Joi.number().min(1).required(),
                comment:Joi.string().trim().required(),
                receiverId:Joi.string().trim().optional(),
                type:Joi.string().required().valid(
                    'DRIVER',
                    'COMPANY',
                    'VENDOR',
                    'LOADER'
                )

            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
 {
        method: 'PUT'
        , path: '/api/admin/clearAccountDetails'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id){
            var payloadData = request.payload;
            Controller.AdminController.clearAccount(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

        }
        }, config: {
        description: 'mark  Account closed.',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            payload: {
                id:Joi.string().trim().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
 {
        method: 'GET'
        , path: '/api/admin/getAccountDetails'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id){
             var queryData = request.query;
            queryData.receiverId=queryData.id || null
            queryData.id=userData._id;
            queryData.type=userData.type;
            Controller.AdminController.getAccounts(queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

        }
        }, config: {
        description: 'get Accounts Details.',
        tags: ['api', 'admin'],
        auth: 'CommonAuth',
        validate: {
            query: {
                id:Joi.string().trim().optional(),
                bookingId:Joi.string().trim().optional(),
                skip:Joi.number().required().min(0),
                limit:Joi.number().required().min(0),
                options:Joi.string().required().valid(
                    'DRIVER',
                    'COMPANY',
                    'VENDOR',
                    'LOADER'
                ),
                cleared:Joi.boolean().optional()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'GET'
        , path: '/api/admin/getMessages'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id){
            var queryData = request.query;
            Controller.AdminController.getMessages(queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

        }
        }, config: {
        description: 'get Accounts Details.',
        tags: ['api', 'admin'],
        auth: 'CommonAuth',
        validate: {
            query: {
                id:Joi.string().trim().optional(),
                skip:Joi.number().required().min(0),
                limit:Joi.number().required().min(0)
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'POST'
        , path: '/api/admin/logout'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id){
            if([Config.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN,Config.APP_CONSTANTS.DATABASE.USER_ROLES.LOADER,Config.APP_CONSTANTS.DATABASE.USER_ROLES.COMPANY,Config.APP_CONSTANTS.DATABASE.USER_ROLES.VENDOR,Config.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER].indexOf(userData.type)<0 ){
                return reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED))
            }

            var queryData = {};
            queryData.type=userData.type;
            queryData.id=userData.id;
            Controller.AdminController.logoutUser(queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

        }
        }, config: {
        description: 'get Accounts Details.',
        tags: ['api', 'admin'],
        auth: 'CommonAuth',
        validate: {
            payload: {},
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'POST'
        , path: '/api/admin/sendPush'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id){
            var payloadData = request.payload;
            Controller.AdminController.sendPush(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

        }
        }, config: {
        description: 'Send Push to all,vendors,company,driver,loaders.',
        tags: ['api', 'admin'],
        auth: 'adminAuth',
        validate: {
            payload: {
                ids:Joi.array().items(Joi.string()).optional(),
                option:Joi.string().required().valid(
                    Config.APP_CONSTANTS.DATABASE.USER_ROLES.COMPANY,
                    Config.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER,
                    Config.APP_CONSTANTS.DATABASE.USER_ROLES.VENDOR,
                    Config.APP_CONSTANTS.DATABASE.USER_ROLES.LOADER
                ),
                message:Joi.string().trim().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
},
{
        method: 'GET'
        , path: '/api/admin/getNotifications'
        , handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        if(userData && userData.id){
            if([Config.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN,Config.APP_CONSTANTS.DATABASE.USER_ROLES.COMPANY,Config.APP_CONSTANTS.DATABASE.USER_ROLES.VENDOR].indexOf(userData.type)<0 ){
                return reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED))
            }

            var queryData = request.query;
            queryData.id=userData.id;
            queryData.userType=userData.type;
            Controller.AdminController.getPushNotifications(queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))

                }
            });
        }else{
            reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

        }
        }, config: {
        description: 'get Notifications .',
        tags: ['api', 'admin'],
        auth: 'CommonAuth',
        validate: {
            query: {
                type:Joi.string().valid(
                    Config.APP_CONSTANTS.DATABASE.USER_ROLES.COMPANY,
                    Config.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER,
                    Config.APP_CONSTANTS.DATABASE.USER_ROLES.LOADER,
                    Config.APP_CONSTANTS.DATABASE.USER_ROLES.VENDOR
                    ).optional(),
                skip:Joi.number().required().min(0),
                limit:Joi.number().required().min(0)
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
}
];


var authRoutes = [].concat(adminLogin);

module.exports = authRoutes.concat(non_auth_routes);